<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\FarmaciaController;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\CarrinhoController;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\RelatorioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Produto
Route::get('/', [ProdutoController::class, 'index'])->name('index');

Route::get('/produto', [ProdutoController::class, 'index'])->name('produto');

Route::get('/produto/{nomeprod}', [ProdutoController::class, 'detalhes'])->name('nome_produto');

Route::get('/produto/{nomeprod}/{id_farmacia}', [ProdutoController::class, 'detalhes'])->name('nome_produto_selecionado');

Route::get('/categorias', [ProdutoController::class, 'categoria'])->name('categorias');

Route::get('/categorias/{nomecat}', [ProdutoController::class, 'categoria'])->name('nome_categoria');

Route::post('/pesquisa', [ProdutoController::class, 'pesquisa'])->name('pesquisa');

Route::post('/carrinho/adicionar', [ProdutoController::class, 'adicionaProdutoCarrinho'])->name('adicionar_carrinho');


//Carrinho
Route::post('/carrinho/remover', [CarrinhoController::class, 'removeProdutoCarrinho'])->name('remover_carrinho');

Route::post('/carrinho/atualizar', [CarrinhoController::class, 'alteraProdutoCarrinho'])->name('atualizar_carrinho');

Route::get('/carrinho', [CarrinhoController::class, 'carrinho'])->name('carrinho');

Route::get('/checkout', [CarrinhoController::class, 'checkout'])->name('checkout');

Route::post('/finalizar_pedido', [CarrinhoController::class, 'finalizaPedido'])->name('finalizar_pedido');


//Cliente
Route::get('/cadastro', [ClienteController::class, 'cadastro'])->name('cadastro_cliente');

Route::post('/cadastro/finalizar', [ClienteController::class, 'finalizaCadastro'])->name('finalizar_cadastro');

Route::get('/minha_conta', [ClienteController::class, 'minhaConta'])->name('minha_conta');

Route::post('/endereco/usar', [ClienteController::class, 'alteraEnderecoEmUso'])->name('alterar_endereco_em_uso');

Route::get('/endereco/dados/{id_endereco}', [ClienteController::class, 'getEndereco'])->name('dados_endereco');

Route::post('/endereco/alterar', [ClienteController::class, 'alteraEndereco'])->name('alterar_endereco');

Route::post('/endereco/cadastrar', [ClienteController::class, 'cadastraEndereco'])->name('cadastrar_endereco');

Route::get('/endereco', function() {
    return view('cliente/cadastroEndereco');
});



//Pedido
Route::get('/meus_pedidos', [PedidoController::class, 'meusPedidos'])->name('meus_pedidos');

Route::get('/pedido/detalhes/{id_pedido}', [PedidoController::class, 'detalhePedido'])->name('detalhe_pedido');

Route::get('/pedido/cancelar/{id_pedido}', [PedidoController::class, 'cancelaPedido'])->name('cancelar_pedido');

Route::get('/pedido/cancelar/{id_pedido}/{slug}', [PedidoController::class, 'cancelaProdutoPedido'])->name('cancelar_produto_pedido');

Route::get('/pedido/confirmar/{id_pedido}', [PedidoController::class, 'confirmaPedido'])->name('confirmar_pedido');

Route::get('/pedido/confirmar/{id_pedido}/{slug}', [PedidoController::class, 'confirmaProdutoPedido'])->name('confirmar_produto_pedido');

Route::get('/pedido/entregar/{id_pedido}', [PedidoController::class, 'entregaPedido'])->name('entregar_pedido');

Route::get('/pedido/concluir/{id_pedido}', [PedidoController::class, 'concluiPedido'])->name('concluir_pedido');





//Farmácia
Route::get('/farmacia/cadastro', [FarmaciaController::class, 'cadastro'])->name('cadastro_farmacia');

Route::post('/farmacia/cadastro/finalizar', [FarmaciaController::class, 'finalizaCadastro'])->name('finalizar_cadastro_farmacia');

Route::get('/farmacia/alterar_dados', [FarmaciaController::class, 'alteraDadosFarmacia'])->name('alterar_dados_farmacia');

Route::post('/farmacia/alterar_cadastro', [FarmaciaController::class, 'alteraCadastroFarmacia'])->name('alterar_cadastro_farmacia');

Route::get('/farmacia', [FarmaciaController::class, 'index'])->name('index_farmacia');

Route::get('/farmacia/abrir_fechar', [FarmaciaController::class, 'abreFecha'])->name('abrir_fechar');

Route::get('/farmacia/minha_conta', [FarmaciaController::class, 'minhaConta'])->name('minha_conta_farmacia');

Route::get('/farmacia/estoque', [FarmaciaController::class, 'estoque'])->name('estoque');

Route::get('/farmacia/estoque/{codbarras}', [FarmaciaController::class, 'getProduto'])->name('retornar_produto');

Route::get('/farmacia/meus_produtos', [FarmaciaController::class, 'getProdutoFarmacia'])->name('meus_produtos');

Route::post('/estoque/produto/alterar', [FarmaciaController::class, 'alteraEstoque'])->name('alterar_estoque');

Route::get('/farmacia/cadastro', [FarmaciaController::class, 'cadastro'])->name('cadastro_farmacia');

/*Route::get('/verificar_email', [ClienteController::class, 'verificaEmailUsuario'])->name('verificar_email');

Route::post('/enviar_email_recuperacao', [ClienteController::class, 'enviaRecuperacaoSenha'])->name('enviar_email_recuperacao');

Route::get('/recuperar_senha', [ClienteController::class, 'recuperaSenha'])->name('recuperar_senha');

Route::post('/redefinir_senha', [ClienteController::class, 'redefineSenha'])->name('redefinir_senha');*/


//Usuário
Route::get('/alterar_dados', [UsuarioController::class, 'alteraDados'])->name('alterar_dados');

Route::post('/alterar_cadastro', [UsuarioController::class, 'alteraCadastro'])->name('alterar_cadastro');

Route::post('/autenticar', [UsuarioController::class, 'autenticar'])->name('autenticar');

Route::get('/sair', [UsuarioController::class, 'sair'])->name('sair');

Route::get('/alterar_senha', [UsuarioController::class, 'alteracaoSenha'])->name('alterar_senha');

Route::post('/confirmar_alteracao_senha', [UsuarioController::class, 'confirmaAlteracaoSenha'])->name('confirmar_alteracao_senha');

Route::get('/login', [UsuarioController::class, 'telaLogin'])->name('login');

Route::get('/farmacia/login', [UsuarioController::class, 'telaLogin'])->name('login_farmacia');

Route::get('/esqueci_senha', [UsuarioController::class, 'esqueciSenha'])->name('esqueci_senha');

Route::get('/verificar_email', [UsuarioController::class, 'verificaEmailUsuario'])->name('verificar_email');

Route::post('/enviar_email_recuperacao', [UsuarioController::class, 'enviaRecuperacaoSenha'])->name('enviar_email_recuperacao');

Route::get('/recuperar_senha', [UsuarioController::class, 'recuperaSenha'])->name('recuperar_senha');

Route::post('/redefinir_senha', [UsuarioController::class, 'redefineSenha'])->name('redefinir_senha');

Route::get('/sucesso', function() {
    return view('cliente/telaSucesso');
})->name('tela_sucesso');

Route::get('/farmacia/sucesso', function() {
    return view('farmacia/telaSucesso');
})->name('tela_sucesso_farmacia');

Route::get('/erro', function() {
    return view('cliente/telaErro');
})->name('tela_erro');

Route::get('/farmacia/erro', function() {
    return view('farmacia/telaErro');
})->name('tela_erro_farmacia');

//Route::get('/sucesso', [UsuarioController::class, 'sucesso'])->name('tela_sucesso');

//Route::get('/erro', [UsuarioController::class, 'erro'])->name('tela_erro');

/*Route::get('/esqueci_senha', function() {
    return view('esqueciSenha');
})->name('esqueci_senha');*/



//Relatórios
Route::get('/farmacia/relatorios', [RelatorioController::class, 'index'])->name('relatorios');

Route::get('/farmacia/relatorios/estoque', [RelatorioController::class, 'relatorioEstoque'])->name('relatorio_estoque');

Route::get('/farmacia/relatorios/vendas_detalhado', [RelatorioController::class, 'relatorioVendasDetalhado'])->name('relatorio_vendas_detalhado');

Route::get('/farmacia/relatorios/vendas_canceladas_detalhado', [RelatorioController::class, 'relatorioVendasCanceladasDetalhado'])->name('relatorio_vendas_canceladas_detalhado');

Route::get('/farmacia/relatorios/vendas_por_periodo', [RelatorioController::class, 'relatorioVendasPorPeriodo'])->name('relatorio_vendas_por_periodo');

Route::get('/farmacia/relatorios/vendas_por_mes', [RelatorioController::class, 'relatorioVendasPorMes'])->name('relatorio_vendas_por_mes');

Route::get('/farmacia/relatorios/vendas_por_regiao', [RelatorioController::class, 'relatorioVendasPorRegiao'])->name('relatorio_vendas_por_regiao');

Route::get('/farmacia/relatorios/produtos_mais_vendidos', [RelatorioController::class, 'relatorioMaisVendidos'])->name('relatorio_produtos_mais_vendidos');

Route::get('/farmacia/relatorios/produtos_menos_vendidos', [RelatorioController::class, 'relatorioMenosVendidos'])->name('relatorio_produtos_menos_vendidos');