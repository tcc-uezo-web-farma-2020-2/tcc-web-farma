<?php

namespace Database\Factories;

use App\Models\ProdutoFarmacia;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProdutoFarmaciaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProdutoFarmacia::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
