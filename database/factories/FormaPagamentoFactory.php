<?php

namespace Database\Factories;

use App\Models\FormaPagamento;
use Illuminate\Database\Eloquent\Factories\Factory;

class FormaPagamentoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FormaPagamento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
