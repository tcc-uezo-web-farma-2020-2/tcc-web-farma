<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Perfil;
use App\Models\Usuario;
use App\Models\Endereco;
use App\Models\Farmacia;
use App\Models\ValorEntrega;
use App\Models\ProdutoFarmacia;
use Illuminate\Support\Facades\Hash;

class InsertUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //perfis
        $perfil1 = new Perfil(['perfil' => 'Cliente']);
        $perfil1->save();
        $perfil2 = new Perfil(['perfil' => 'Administrador']);
        $perfil2->save();

        //usuarios        
        //$usuario1 = new usuario(['nome' => '', 'email' => '', 'password' => '', 'cpf' => '', 'data_nascimento' => '', 'telefone' => '', 'celular' => '', 'genero' => '']);
        $usuario1 = new Usuario(['id_perfil' => $perfil1->id, 'nome' => 'Uezo da Silva', 'email' => 'uezo@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1997-04-05', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Masculino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuario1->save();
        $usuario2 = new Usuario(['id_perfil' => $perfil1->id, 'nome' => 'Lucas estudante', 'email' => 'lucas@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1996-02-15', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Masculino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuario2->save();
        $usuario3 = new Usuario(['id_perfil' => $perfil1->id, 'nome' => 'Marcos professor', 'email' => 'marcos@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1993-08-11', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Masculino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuario3->save();
        $usuario4 = new Usuario(['id_perfil' => $perfil1->id, 'nome' => 'Camila diretora', 'email' => 'camila@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1998-12-30', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Feminino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuario4->save();
        $usuario5 = new Usuario(['id_perfil' => $perfil1->id, 'nome' => 'Jose Formando', 'email' => 'jlvargem@hotmail.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1994-05-20', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Masculino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuario5->save();

        //usuarios        
        //$usuario1 = new usuario(['nome' => '', 'email' => '', 'password' => '', 'cpf' => '', 'data_nascimento' => '', 'telefone' => '', 'celular' => '', 'genero' => '']);
        $usuariof1 = new Usuario(['id_perfil' => $perfil2->id, 'nome' => 'Adm Uezo Farma', 'email' => 'uezofarma@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1997-04-05', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Feminino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuariof1->save();
        $usuariof2 = new Usuario(['id_perfil' => $perfil2->id, 'nome' => 'Adm Zona Oeste Farma', 'email' => 'zonaoeste@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1996-02-15', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Masculino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuariof2->save();
        $usuariof3 = new Usuario(['id_perfil' => $perfil2->id, 'nome' => 'Adm JL farma', 'email' => 'jl@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1993-08-11', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Feminino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuariof3->save();
        $usuariof4 = new Usuario(['id_perfil' => $perfil2->id, 'nome' => 'Adm TCC farma', 'email' => 'ballet_tati@hotmail.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1998-12-30', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Feminino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuariof4->save();
        $usuariof5 = new Usuario(['id_perfil' => $perfil2->id, 'nome' => 'Adm Web saúde', 'email' => 'websaude@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1994-05-20', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Masculino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuariof5->save();
        $usuariof6 = new Usuario(['id_perfil' => $perfil2->id, 'nome' => 'Adm Teste farma', 'email' => 'teste@email.com', 'password' => Hash::make('1234'), 'cpf' => '12345678910', 'data_nascimento' => '1994-05-20', 'telefone' => '2133333333', 'celular' => '21999999999', 'genero' => 'Masculino', 'codigo_verificacao' => null, 'email_verificado' => 1]);
        $usuariof6->save();

        //Endereços clientes
        //$endereco1 = new Endereco(['id_usuario' => '', 'descricao' => '', 'em_uso' => 1, 'logradouro' => '', 'numero' => , 'complemento' => null, 'bairro' => '','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '', 'latitude' => '', 'longitude' => '']);
        $endereco = new Endereco(['id_usuario' => $usuario1->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Avenida Manuel Caldeira de Alvarenga', 'numero' => 1203, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23070200', 'latitude' => -22.899155, 'longitude' => -43.578162]);
        $endereco->save();
        $endereco = new Endereco(['id_usuario' => $usuario2->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Avenida Manuel Caldeira de Alvarenga', 'numero' => 1203, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23070200', 'latitude' => -22.899155, 'longitude' => -43.578162]);
        $endereco->save();
        $endereco = new Endereco(['id_usuario' => $usuario3->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Avenida Manuel Caldeira de Alvarenga', 'numero' => 1203, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23070200', 'latitude' => -22.899155, 'longitude' => -43.578162]);
        $endereco->save();
        //Bangu
        $endereco = new Endereco(['id_usuario' => $usuario4->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Rua Oliveira Ribeiro', 'numero' => 1610, 'complemento' => null, 'bairro' => 'Bangu','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '21815115', 'latitude' => -22.882701, 'longitude' => -43.468097]);
        $endereco->save();
        $endereco = new Endereco(['id_usuario' => $usuario5->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Rua Cobé', 'numero' => 621, 'complemento' => null, 'bairro' => 'Bangu','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '21820200', 'latitude' => -22.884042, 'longitude' => -43.474351]); 
        $endereco->save();
        $endereco = new Endereco(['id_usuario' => $usuario5->id, 'descricao' => 'faculdade', 'em_uso' => 0, 'logradouro' => 'Avenida Manuel Caldeira de Alvarenga', 'numero' => 1203, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23070200', 'latitude' => -22.899155, 'longitude' => -43.578162]);
        $endereco->save();


        //Endereços farmácias
        /*$endereco = new Endereco(['id_usuario' => $usuariof1->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Rua campo grande', 'numero' => 1164, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.902137, 'longitude' => -43.560656]);
        $endereco->save();
        $endereco = new Endereco(['id_usuario' => $usuariof2->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Rua campo grande', 'numero' => 1164, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.902137, 'longitude' => -43.560656]);
        $endereco->save();
        $endereco = new Endereco(['id_usuario' => $usuariof3->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Rua campo grande', 'numero' => 1164, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.902137, 'longitude' => -43.560656]);
        $endereco->save();
        $endereco = new Endereco(['id_usuario' => $usuariof4->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Rua dos Limadores', 'numero' => 756, 'complemento' => null, 'bairro' => 'Bangu','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '21830005', 'latitude' => -22.886341, 'longitude' => -43.476127]);
        $endereco->save();
        //Bangu
        $endereco = new Endereco(['id_usuario' => $usuariof5->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Rua campo grande', 'numero' => 1164, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.902137, 'longitude' => -43.560656]);
        $endereco->save();
        $endereco = new Endereco(['id_usuario' => $usuariof6->id, 'descricao' => 'casa', 'em_uso' => 1, 'logradouro' => 'Rua Francisco Real', 'numero' => 1933, 'complemento' => null, 'bairro' => 'Bangu','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.879856, 'longitude' => -43.463680]); 
        $endereco->save();



        //Farmácias
        //$farmacia1 = new Farmacia(['nome' => '', 'email' => '', 'password' => '', 'aberto' => true, 'logradouro' => 'Rua campo grande', 'numero' => 132, 'complemento' => null, 'bairro' => '','cidade' => '', 'estado' => '', 'cep' => '', 'latitude' => '', 'longitude' => '']);
        $farmacia2 = new Farmacia(['id_usuario' => $usuariof1->id, 'aberto' => true, 'raio_entrega' => 5]);
        $farmacia1->save();
        $farmacia2 = new Farmacia(['id_usuario' => $usuariof2->id, 'aberto' => true, 'raio_entrega' => 5]);
        $farmacia2->save();
        $farmacia3 = new Farmacia(['id_usuario' => $usuariof3->id, 'aberto' => true, 'raio_entrega' => 5]);
        $farmacia3->save();
        //Bangu
        $farmacia4 = new Farmacia(['id_usuario' => $usuariof4->id, 'aberto' => true, 'raio_entrega' => 5]);
        $farmacia4->save();
        $farmacia5 = new Farmacia(['id_usuario' => $usuariof5->id, 'aberto' => true, 'raio_entrega' => 5]);
        $farmacia5->save();
        //Bangu
        $farmacia6 = new Farmacia(['id_usuario' => $usuariof6->id, 'aberto' => true, 'raio_entrega' => 5]);
        $farmacia6->save();*/

        $farmacia1 = new Farmacia(['id_usuario' => $usuariof1->id, 'nome' => 'Uezo Farma', /*'email' => 'uezo@email.com',*/ 'cnpj' => '12345678901234', 'aberto' => true, 'raio_entrega' => 5, 'logradouro' => 'Rua campo grande', 'numero' => 1164, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.902137, 'longitude' => -43.560656]);
        $farmacia1->save();
        $farmacia2 = new Farmacia(['id_usuario' => $usuariof2->id, 'nome' => 'Zona Oeste Farma', /*'email' => 'zonaoeste@email.com',*/ 'cnpj' => '12345678901234', 'aberto' => true, 'raio_entrega' => 5, 'logradouro' => 'Rua campo grande', 'numero' => 1164, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.902137, 'longitude' => -43.560656]);
        $farmacia2->save();
        $farmacia3 = new Farmacia(['id_usuario' => $usuariof3->id, 'nome' => 'JL farma', /*'email' => 'jl@email.com',*/ 'cnpj' => '12345678901234', 'aberto' => true, 'raio_entrega' => 5, 'logradouro' => 'Rua campo grande', 'numero' => 1164, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.902137, 'longitude' => -43.560656]);
        $farmacia3->save();
        //Bangu
        $farmacia4 = new Farmacia(['id_usuario' => $usuariof4->id, 'nome' => 'TCC farma', /*'email' => 'tcc@email.com',*/ 'cnpj' => '12345678901234', 'aberto' => true, 'raio_entrega' => 5, 'logradouro' => 'Rua dos Limadores', 'numero' => 756, 'complemento' => null, 'bairro' => 'Bangu','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '21830005', 'latitude' => -22.886341, 'longitude' => -43.476127]);
        $farmacia4->save();
        $farmacia5 = new Farmacia(['id_usuario' => $usuariof5->id, 'nome' => 'Web saúde', /*'email' => 'websaude@email.com',*/ 'cnpj' => '12345678901234', 'aberto' => true, 'raio_entrega' => 5, 'logradouro' => 'Rua campo grande', 'numero' => 1164, 'complemento' => null, 'bairro' => 'Campo Grande','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.902137, 'longitude' => -43.560656]);
        $farmacia5->save();
        //Bangu
        $farmacia6 = new Farmacia(['id_usuario' => $usuariof6->id, 'nome' => 'Teste farma', /*'email' => 'teste@email.com',*/ 'cnpj' => '12345678901234', 'aberto' => true, 'raio_entrega' => 5, 'logradouro' => 'Rua Francisco Real', 'numero' => 1933, 'complemento' => null, 'bairro' => 'Bangu','cidade' => 'Rio de Janeiro', 'estado' => 'RJ', 'cep' => '23050300', 'latitude' => -22.879856, 'longitude' => -43.463680]);
        $farmacia6->save();

        //$entrega = new ValorEntrega(['id_farmacia' => '', 'raio_km_de' => '', 'raio_km_ate' => '', 'valor' => '']);
        //farmacia1
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia1->id, 'raio_km_de' => 0, 'raio_km_ate' => 2, 'valor' => 2]);
        $entrega->save();
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia1->id, 'raio_km_de' => 2.1, 'raio_km_ate' => 4, 'valor' => 3]);
        $entrega->save();
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia1->id, 'raio_km_de' => 4.1, 'raio_km_ate' => 5, 'valor' => 4]);
        $entrega->save();

        //farmacia2
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia2->id, 'raio_km_de' => 0, 'raio_km_ate' => 3, 'valor' => 2]);
        $entrega->save();
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia2->id, 'raio_km_de' => 3.1, 'raio_km_ate' => 5, 'valor' => 4]);
        $entrega->save();

        //farmacia3
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia3->id, 'raio_km_de' => 0, 'raio_km_ate' => 3, 'valor' => 0]);
        $entrega->save();
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia3->id, 'raio_km_de' => 3.1, 'raio_km_ate' => 5, 'valor' => 2]);
        $entrega->save();

        //farmacia4
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia4->id, 'raio_km_de' => 0, 'raio_km_ate' => 5, 'valor' => 0]);
        $entrega->save();

        //farmacia5
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia5->id, 'raio_km_de' => 0, 'raio_km_ate' => 5, 'valor' => 1.99]);
        $entrega->save();

        //farmacia6
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia6->id, 'raio_km_de' => 0, 'raio_km_ate' => 4, 'valor' => 0]);
        $entrega->save();
        $entrega = new ValorEntrega(['id_farmacia' => $farmacia6->id, 'raio_km_de' => 4.1, 'raio_km_ate' => 5, 'valor' => 3]);
        $entrega->save();

        //Produto_Farmacia = new \App\Model\Produto(['id_Farmacia' => $Farmacia, 'produto' => '', 'cod_barras' => '', 'estoque' => '', 'variacao' => '', 'composicao' => '', 'detalhes' => '', 'imagens' => 'asset{{ imagens/. }}']);
        //Advil
        /*$pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 1, 'valor' => 8.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 1, 'valor' => 9.35, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 1, 'valor' => 9.24, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 1, 'valor' => 10.05, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 1, 'valor' => 9.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 1, 'valor' => 8.99, 'estoque' => 33]);
        $pf->save();

        //Cimegripe
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 2, 'valor' => 8.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 2, 'valor' => 9.35, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 2, 'valor' => 9.99, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 2, 'valor' => 10.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 2, 'valor' => 9.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 2, 'valor' => 9.44, 'estoque' => 33]);
        $pf->save();

        //Florent
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 3, 'valor' => 35.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 3, 'valor' => 38.35, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 3, 'valor' => 32.90, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 3, 'valor' => 39.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 3, 'valor' => 37.50, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 3, 'valor' => 33.99, 'estoque' => 33]);
        $pf->save();

        //Acetona
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 4, 'valor' => 4.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 4, 'valor' => 5.40, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 4, 'valor' => 4.99, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 4, 'valor' => 5.15, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 4, 'valor' => 4.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 4, 'valor' => 4.99, 'estoque' => 33]);
        $pf->save();

        //Hidratante
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 5, 'valor' => 12.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 5, 'valor' => 11.99, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 5, 'valor' => 12.50, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 5, 'valor' => 12.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 5, 'valor' => 10.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 5, 'valor' => 11.79, 'estoque' => 33]);
        $pf->save();

        //Esmalte
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 6, 'valor' => 6.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 6, 'valor' => 6.79, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 6, 'valor' => 6.29, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 6, 'valor' => 7.19, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 6, 'valor' => 5.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 6, 'valor' => 6.69, 'estoque' => 33]);
        $pf->save();

        //Desodorante
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 7, 'valor' => 11.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 7, 'valor' => 10.99, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 7, 'valor' => 12.69, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 7, 'valor' => 10.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 7, 'valor' => 10.59, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 7, 'valor' => 11.69, 'estoque' => 33]);
        $pf->save();

        //Cotonete
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 8, 'valor' => 4.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 8, 'valor' => 5.19, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 8, 'valor' => 4.79, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 8, 'valor' => 4.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 8, 'valor' => 4.39, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 8, 'valor' => 4.19, 'estoque' => 33]);
        $pf->save();

        //Shampoo
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 9, 'valor' => 9.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 9, 'valor' => 10.59, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 9, 'valor' => 9.67, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 9, 'valor' => 9.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 9, 'valor' => 10.19, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 9, 'valor' => 9.39, 'estoque' => 33]);
        $pf->save();

        //Esparadrapo
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia1->id, 'id_produto' => 10, 'valor' => 9.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia2->id, 'id_produto' => 10, 'valor' => 10.99, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia3->id, 'id_produto' => 10, 'valor' => 10.29, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia4->id, 'id_produto' => 10, 'valor' => 10.79, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia5->id, 'id_produto' => 10, 'valor' => 11.29, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => $farmacia6->id, 'id_produto' => 10, 'valor' => 9.99, 'estoque' => 33]);
        $pf->save();*/
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}