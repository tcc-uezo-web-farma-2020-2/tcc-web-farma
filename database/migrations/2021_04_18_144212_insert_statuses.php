<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Status;

class InsertStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $formaPag = new Status(['status' => 'Aguardando confirmação', 'classe_estilo' => 'text-primary']);
        $formaPag->save();
        $formaPag = new Status(['status' => 'Confirmado', 'classe_estilo' => 'text-success']);
        $formaPag->save();
        $formaPag = new Status(['status' => 'Parcialmente confirmado', 'classe_estilo' => 'text-info']);
        $formaPag->save();
        $formaPag = new Status(['status' => 'Em entrega', 'classe_estilo' => 'text-primary']);
        $formaPag->save();
        $formaPag = new Status(['status' => 'Concluído', 'classe_estilo' => 'text-success']);
        $formaPag->save();
        $formaPag = new Status(['status' => 'Parcialmente concluído', 'classe_estilo' => 'text-info']);
        $formaPag->save();
        $formaPag = new Status(['status' => 'Cancelado', 'classe_estilo' => 'text-danger']);
        $formaPag->save();
        $formaPag = new Status(['status' => 'Parcialmente cancelado', 'classe_estilo' => 'text-warning']);
        $formaPag->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
