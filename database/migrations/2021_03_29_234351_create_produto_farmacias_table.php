<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutoFarmaciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto_farmacias', function (Blueprint $table) {
            $table->id();
            $table->string('id_farmacia')->constrained('farmacias');
            $table->string('id_produto')->constrained('produtos');
            $table->float('valor', 8, 2);            
            $table->integer('estoque');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto_farmacias');
    }
}
