<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValorEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valor_entregas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_farmacia')->constrained('farmacias');
            $table->float('raio_km_de', 4, 1);
            $table->float('raio_km_ate', 4, 1);
            $table->float('valor', 4, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valor_entregas');
    }
}
