<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarmaciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farmacias', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_usuario')->constrained('usuarios');
            $table->string('nome', 100);
            //$table->string('email', 100)->unique();            
            $table->string('cnpj', 14);                        
            $table->boolean('aberto');
            $table->float('raio_entrega', 2, 1);
            $table->string('logradouro', 100);
            $table->integer('numero');
            $table->string('complemento', 100)->nullable();
            $table->string('bairro', 50);
            $table->string('cidade', 50);
            $table->string('estado', 2);
            $table->string('cep', 8);
            $table->double('latitude');
            $table->double('longitude');
            $table->timestamps();
        });

        /*Schema::create('farmacias', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_usuario')->constrained('usuarios');                      
            $table->boolean('aberto');
            $table->float('raio_entrega', 2, 1);            
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmacias');
    }
}
