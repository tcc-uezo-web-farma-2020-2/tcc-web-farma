<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Categoria;
use App\Models\Produto;

class InsertProdutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        //Categorias - foreign key de produtos
        $categoria1 = new Categoria(['categoria' => 'Medicamentos', 'slug' => 'medicamentos']);
        $categoria1->save();
        $categoria2 = new Categoria(['categoria' => 'Cosméticos', 'slug' => 'cosmeticos']);
        $categoria2->save();
        $categoria3 = new Categoria(['categoria' => 'Higiene Pessoal', 'slug' => 'higiene-pessoal']);
        $categoria3->save();
        $categoria4 = new Categoria(['categoria' => 'Primeiros socorros', 'slug' => 'primeiros-socorros']);
        $categoria4->save();
        $categoria5 = new Categoria(['categoria' => 'Mamães e Bebês', 'slug' => 'mamaes-e-bebes']);
        $categoria5->save();

        //$produto = new \App\Model\Produto(['id_categoria' => $categoria, 'produto' => '', 'cod_barras' => '', 'quantidade' => '', 'variacao' => '', 'composicao' => '', 'detalhes' => '', 'imagens' => 'imagens/.']);
        $produto = new Produto(['id_categoria' => $categoria1->id, 'produto' => 'Advil 400mg 8 cápsulas', 'slug' => 'advil-400mg-8-capsulas', 'cod_barras' => '7891045043564', 'quantidade' => '8 cápsulas', 'variacao' => 'Cápsulas', 'composicao' => 'Ibuprofeno', 'detalhes' => null, 'imagens' => 'imagens/7891045043564.jpg']);
        $produto->save();
        $produto = new Produto(['id_categoria' => $categoria1->id, 'produto' => 'Cimegripe 20 cápsulas', 'slug' => 'cimegripe-20-capsulas', 'cod_barras' => '7896523200576', 'quantidade' => '8 cápsulas', 'variacao' => 'Cápsulas', 'composicao' => 'Paracetamol,Maleato de Clorfenamina,Cloridrato de fenilefrina', 'detalhes' => null, 'imagens' => 'imagens/7896523200576.jpg']);
        $produto->save();
        $produto = new Produto(['id_categoria' => $categoria1->id, 'produto' => 'Florent 100mg 122 cápsulas', 'slug' => 'florent-100mg-122-capsulas', 'cod_barras' => '7898158692221', 'quantidade' => '12 cápsulas', 'variacao' => 'Cápsulas', 'composicao' => 'Saccharomycre boulardii', 'detalhes' => null, 'imagens' => 'imagens/7898158692221.webp']);
        $produto->save();

        $produto = new Produto(['id_categoria' => $categoria2->id, 'produto' => 'Acetona Musa 200ml', 'slug' => 'acetona-musa-200ml', 'cod_barras' => '7896205909599', 'quantidade' => '200ml', 'variacao' => null, 'composicao' => null, 'detalhes' => null, 'imagens' => 'imagens/7896205909599.png']);
        $produto->save();
        $produto = new Produto(['id_categoria' => $categoria2->id, 'produto' => 'Hidratante paixão irresistível 400ml', 'slug' => 'hidratante-paixao-irresistivel-400ml', 'cod_barras' => '7896094909250', 'quantidade' => '400ml', 'variacao' => null, 'composicao' => null, 'detalhes' => null, 'imagens' => 'imagens/7896094909250.webp']);
        $produto->save();
        $produto = new Produto(['id_categoria' => $categoria2->id, 'produto' => 'Esmalte Dailus Queridinhos - Festa do pijama', 'slug' => 'esmalte-dailus-queridinhos-festa-do-pijama', 'cod_barras' => '7894222010137', 'quantidade' => '8ml', 'variacao' => 'Festa do pijama', 'composicao' => null, 'detalhes' => null, 'imagens' => 'imagens/7894222010137.png']);        
        $produto->save();

        $produto = new Produto(['id_categoria' => $categoria3->id, 'produto' => 'Desodorante Antitranspirante Aerosol Rexona Men Impacto', 'slug' => 'desodorante-antitranspirante-aerosol-rexona-men-impacto', 'cod_barras' => '7891150054646', 'quantidade' => '150ml', 'variacao' => 'Impact Aerosol', 'composicao' => null, 'detalhes' => null, 'imagens' => 'imagens/7891150054646.webp']);
        $produto->save();
        $produto = new Produto(['id_categoria' => $categoria3->id, 'produto' => 'Hastes flexíveis Use It 120 unidades', 'slug' => 'hastes-flexiveis-use-it-120-unidades', 'cod_barras' => '7898909332093', 'quantidade' => '120 unidades', 'variacao' => null, 'composicao' => null, 'detalhes' => null, 'imagens' => 'imagens/7898909332093.jpg']);
        $produto->save();
        $produto = new Produto(['id_categoria' => $categoria3->id, 'produto' => 'Shampoo Seda Liso Extremo 325ml', 'slug' => 'shampoo-seda-liso-extremo-325ml', 'cod_barras' => '7891150037502', 'quantidade' => '325ml', 'variacao' => 'liso extremo', 'composicao' => null, 'detalhes' => null, 'imagens' => 'imagens/7891150037502.webp']);
        $produto->save();

        $produto = new Produto(['id_categoria' => $categoria4->id, 'produto' => 'Fita Microporosa Cremer Branca 2,5cm x 4,5m', 'slug' => 'fita-microporosa-cremer-branca-25cm-x-45m', 'cod_barras' => '7891800364880', 'quantidade' => '2,5cm x 4,5m', 'variacao' => 'Branca', 'composicao' => null, 'detalhes' => null, 'imagens' => 'imagens/7891800364880.jpg']);
        $produto->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
