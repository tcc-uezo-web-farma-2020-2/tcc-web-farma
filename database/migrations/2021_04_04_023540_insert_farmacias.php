<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Farmacia;
use App\Models\ProdutoFarmacia;
use App\Models\ValorEntrega;

class InsertFarmacias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 1, 'valor' => 8.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 1, 'valor' => 9.35, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 1, 'valor' => 9.24, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 1, 'valor' => 10.05, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 1, 'valor' => 9.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 1, 'valor' => 8.99, 'estoque' => 33]);
        $pf->save();

        //Cimegripe
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 2, 'valor' => 8.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 2, 'valor' => 9.35, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 2, 'valor' => 9.99, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 2, 'valor' => 10.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 2, 'valor' => 9.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 2, 'valor' => 9.44, 'estoque' => 33]);
        $pf->save();

        //Florent
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 3, 'valor' => 35.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 3, 'valor' => 38.35, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 3, 'valor' => 32.90, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 3, 'valor' => 39.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 3, 'valor' => 37.50, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 3, 'valor' => 33.99, 'estoque' => 33]);
        $pf->save();

        //Acetona
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 4, 'valor' => 4.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 4, 'valor' => 5.40, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 4, 'valor' => 4.99, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 4, 'valor' => 5.15, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 4, 'valor' => 4.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 4, 'valor' => 4.99, 'estoque' => 33]);
        $pf->save();

        //Hidratante
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 5, 'valor' => 12.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 5, 'valor' => 11.99, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 5, 'valor' => 12.50, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 5, 'valor' => 12.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 5, 'valor' => 10.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 5, 'valor' => 11.79, 'estoque' => 33]);
        $pf->save();

        //Esmalte
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 6, 'valor' => 6.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 6, 'valor' => 6.79, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 6, 'valor' => 6.29, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 6, 'valor' => 7.19, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 6, 'valor' => 5.99, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 6, 'valor' => 6.69, 'estoque' => 33]);
        $pf->save();

        //Desodorante
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 7, 'valor' => 11.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 7, 'valor' => 10.99, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 7, 'valor' => 12.69, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 7, 'valor' => 10.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 7, 'valor' => 10.59, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 7, 'valor' => 11.69, 'estoque' => 33]);
        $pf->save();

        //Cotonete
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 8, 'valor' => 4.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 8, 'valor' => 5.19, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 8, 'valor' => 4.79, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 8, 'valor' => 4.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 8, 'valor' => 4.39, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 8, 'valor' => 4.19, 'estoque' => 33]);
        $pf->save();

        //Shampoo
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 9, 'valor' => 9.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 9, 'valor' => 10.59, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 9, 'valor' => 9.67, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 9, 'valor' => 9.99, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 9, 'valor' => 10.19, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 9, 'valor' => 9.39, 'estoque' => 33]);
        $pf->save();

        //Esparadrapo
        $pf = new ProdutoFarmacia(['id_farmacia' => 1, 'id_produto' => 10, 'valor' => 9.99, 'estoque' => 35]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 2, 'id_produto' => 10, 'valor' => 10.99, 'estoque' => 58]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 3, 'id_produto' => 10, 'valor' => 10.29, 'estoque' => 29]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 4, 'id_produto' => 10, 'valor' => 10.79, 'estoque' => 40]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 5, 'id_produto' => 10, 'valor' => 11.29, 'estoque' => 18]);
        $pf->save();
        $pf = new ProdutoFarmacia(['id_farmacia' => 6, 'id_produto' => 10, 'valor' => 9.99, 'estoque' => 33]);
        $pf->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
