<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_perfil')->constrained('perfils');
            $table->string('nome', 100);
            $table->string('email', 100)->unique();            
            $table->string('password');
            $table->string('cpf', 11);
            $table->date('data_nascimento');
            $table->string('telefone', 15);
            $table->string('celular', 15);
            $table->string('genero', 20);
            $table->string('codigo_verificacao')->nullable(); //Onde ficará armazenado o código verificação do e-mail
            $table->boolean('email_verificado')->default(false);
            $table->timestamps();
            $table->softDeletes(); //cria a colina timestamp deleted_at, para um produto não ser deletado de fato do banco
            //$table->timestamp('email_verified_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
