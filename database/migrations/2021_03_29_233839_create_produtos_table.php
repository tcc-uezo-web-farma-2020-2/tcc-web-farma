<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Campos tabela
        Schema::create('produtos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_categoria')->constrained('categorias');
            $table->string('produto', 100)->unique();
            $table->string('slug', 100)->unique();
            $table->string('cod_barras', 30)->unique();
            $table->string('quantidade', 30);
            $table->string('variacao', 30)->nullable();
            $table->string('composicao', 255)->nullable();
            $table->string('detalhes', 255)->nullable();
            $table->string('imagens', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
