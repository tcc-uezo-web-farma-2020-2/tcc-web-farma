<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\FormaPagamento;

class InsertFormaPagamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $formaPag = new FormaPagamento(['forma_pagamento' => 'Dinheiro', 'classe_icone' => 'far fa-money-bill-alt', 'classe_estilo' => 'text-success']);
        $formaPag->save();
        $formaPag = new FormaPagamento(['forma_pagamento' => 'Cartão de crédito', 'classe_icone' => 'fas fa-credit-card', 'classe_estilo' => null]);
        $formaPag->save();
        $formaPag = new FormaPagamento(['forma_pagamento' => 'Cartão de débito', 'classe_icone' => 'far fa-credit-card', 'classe_estilo' => null]);
        $formaPag->save();       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
