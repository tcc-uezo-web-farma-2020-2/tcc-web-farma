<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_usuario')->constrained('usuarios');
            $table->foreignId('id_endereco')->constrained('enderecos');
            $table->foreignId('id_farmacia')->constrained('farmacias');
            $table->foreignId('id_status')->default(1)->constrained('statuses'); //Aguardando confirmação
            $table->foreignId('id_forma_pagamento')->constrained('forma_pagamentos');
            $table->float('valor_pedido', 8, 2);
            $table->float('valor_desconto', 8, 2);
            $table->float('valor_cancelado', 8, 2);
            $table->float('valor_entrega', 8, 2);            
            $table->float('valor_total', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
