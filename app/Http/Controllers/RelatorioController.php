<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Farmacia;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Auth;

class RelatorioController extends Controller
{
    private $arquivo;

    private $farmacia;

    public function index()
    {
        return view('relatorio/index');
    }
    
    public function relatorioEstoque()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $this->farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        //Nome do relatório
        $nomeArquivo = 'Relatorio estoque' . date("d-m-Y H:i") . '.csv';       

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$nomeArquivo.'',
        ];        

        return new StreamedResponse(function () {  
            //Abrindo arquivo
            $this->arquivo = fopen('php://output', 'w');

            //Setando a codficação do arquivo CSV para UTF-8
            fwrite($this->arquivo, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            
            //Cabeçalho das colunas
            fputcsv($this->arquivo, ['produto', 'cod_barras', 'qtd_estoque'], ';');
        
            //Dados - Query
            DB::query()
            ->from('produto_farmacias', 'pf')
            ->select('produtos.produto', 'produtos.cod_barras', 'pf.estoque as qtd_estoque')
            ->join('produtos', 'produtos.id', 'pf.id_produto')
            ->where('pf.id_farmacia', '=', $this->farmacia->id)
            ->orderBy('pf.estoque')
            ->chunk(200, function ($produtos) {
            
                //Incluindo dados no array
                foreach ($produtos as $p) {                    
                    fputcsv($this->arquivo, [$p->produto, $p->cod_barras, $p->qtd_estoque], ';');
                }
                
            });    

            fclose($this->arquivo);

        }, Response::HTTP_OK, $headers);
    }



    public function relatorioVendasDetalhado()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $this->farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        //Nome do relatório
        $nomeArquivo = 'Relatorio de vendas detalhado' . date("d-m-Y H:i") . '.csv';       

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$nomeArquivo.'',
        ];
        

        return new StreamedResponse(function () {  
            //Abrindo arquivo
            $this->arquivo = fopen('php://output', 'w');

            //Setando a codficação do arquivo CSV para UTF-8
            fwrite($this->arquivo, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            
            //Cabeçalho das colunas
            fputcsv($this->arquivo, ['pedido', 'data_pedido', 'status', 'valor_total_pedido', 'valor_entrega', 'valor_cancelado', 'valor_final', 'cliente', 'logradouro', 'numero', 'complemento', 'bairro', 'cidade', 'estado'], ';');
        
            //Dados - Query
            DB::query()
            ->from('pedidos', 'p')
            ->select('p.id', 'p.created_at', 'statuses.status', 'p.valor_pedido', 'p.valor_entrega', 'p.valor_cancelado', 'p.valor_total', 'usuarios.nome', 'enderecos.logradouro', 'enderecos.numero', 'enderecos.complemento', 'enderecos.bairro', 'enderecos.cidade', 'enderecos.estado')
            ->join('usuarios', 'usuarios.id', '=', 'p.id_usuario')
            ->join('enderecos', 'enderecos.id', '=', 'p.id_endereco')
            ->join('statuses', 'statuses.id', '=', 'p.id_status')
            ->where('p.id_farmacia', '=', $this->farmacia->id)
            ->whereIn('p.id_status', [5, 6]) //Concluído e parcialmente concluído
            ->orderBy('p.id')
            ->chunk(200, function ($pedidos) {
            
                //Incluindo dados no array
                foreach ($pedidos as $p) {                    
                    fputcsv($this->arquivo, [$p->id, $p->created_at, $p->status, $p->valor_pedido, $p->valor_entrega, $p->valor_cancelado, $p->valor_total, $p->nome, $p->logradouro, $p->numero, $p->complemento, $p->bairro, $p->cidade, $p->estado], ';');
                }
                
            });    

            fclose($this->arquivo);

        }, Response::HTTP_OK, $headers);
    }



    public function relatorioVendasCanceladasDetalhado()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $this->farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        //Nome do relatório
        $nomeArquivo = 'Relatorio de vendas canceladas detalhado' . date("d-m-Y H:i") . '.csv';       

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$nomeArquivo.'',
        ];
        

        return new StreamedResponse(function () {  
            //Abrindo arquivo
            $this->arquivo = fopen('php://output', 'w');

            //Setando a codficação do arquivo CSV para UTF-8
            fwrite($this->arquivo, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            
            //Cabeçalho das colunas
            fputcsv($this->arquivo, ['pedido', 'data_pedido', 'status', 'valor_total_pedido', 'valor_entrega', 'valor_cancelado', 'valor_final', 'cliente', 'logradouro', 'numero', 'complemento', 'bairro', 'cidade', 'estado'], ';');
        
            //Dados - Query
            DB::query()
            ->from('pedidos', 'p')
            ->select('p.id', 'p.created_at', 'statuses.status', 'p.valor_pedido', 'p.valor_entrega', 'p.valor_cancelado', 'p.valor_total', 'usuarios.nome', 'enderecos.logradouro', 'enderecos.numero', 'enderecos.complemento', 'enderecos.bairro', 'enderecos.cidade', 'enderecos.estado')
            ->join('usuarios', 'usuarios.id', '=', 'p.id_usuario')
            ->join('enderecos', 'enderecos.id', '=', 'p.id_endereco')
            ->join('statuses', 'statuses.id', '=', 'p.id_status')
            ->where('p.id_farmacia', '=', $this->farmacia->id)
            ->where('p.id_status', '=', 7) //Cancelado
            ->orderBy('p.id')
            ->chunk(200, function ($pedidos) {
            
                //Incluindo dados no array
                foreach ($pedidos as $p) {                    
                    fputcsv($this->arquivo, [$p->id, $p->created_at, $p->status, $p->valor_pedido, $p->valor_entrega, $p->valor_cancelado, $p->valor_total, $p->nome, $p->logradouro, $p->numero, $p->complemento, $p->bairro, $p->cidade, $p->estado], ';');
                }
                
            });    

            fclose($this->arquivo);

        }, Response::HTTP_OK, $headers);
    }




    public function relatorioVendasPorPeriodo()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $this->farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        //Nome do relatório
        $nomeArquivo = 'Relatorio de vendas por periodo' . date("d-m-Y H:i") . '.csv';       

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$nomeArquivo.'',
        ];


        return new StreamedResponse(function () {  
            //Abrindo arquivo
            $this->arquivo = fopen('php://output', 'w');

            //Setando a codficação do arquivo CSV para UTF-8
            fwrite($this->arquivo, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            
            //Cabeçalho das colunas
            fputcsv($this->arquivo, ['periodo', 'quantidade', 'faturamento'], ';');
        
            //Dados - Query
            $pedidos[0] = DB::query()
                ->from('pedidos')
                ->select(DB::raw("'hoje' as periodo, count(*) as qtd, sum(valor_total) as faturamento"))
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->where(DB::raw('date(created_at)'), '=', 'current_date')
                ->get()[0];
        
            $pedidos[1] = DB::query()
                ->from('pedidos')
                ->select(DB::raw("'ultima_semana' as periodo, count(*) as qtd, sum(valor_total) as faturamento"))
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->where(DB::raw('date(created_at)'), '>=', DB::raw('date_add(current_date, interval -7 day)'))         
                ->get()[0]; 

            $pedidos[2] = DB::query()
                ->from('pedidos')
                ->select(DB::raw("'ultimo_mes' as periodo, count(*) as qtd, sum(valor_total) as faturamento"))
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->where(DB::raw('date(created_at)'), '>=', DB::raw('date_add(current_date, interval -1 month)'))            
                ->get()[0]; 

            $pedidos[3] = DB::query()
                ->from('pedidos')
                ->select(DB::raw("'ultimo_semestre' as periodo, count(*) as qtd, sum(valor_total) as faturamento"))
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->where(DB::raw('date(created_at)'), '>=', DB::raw('date_add(current_date, interval -6 month)'))            
                ->get()[0];

            $pedidos[4] = DB::query()
                ->from('pedidos')
                ->select(DB::raw("'ultimo_trimestre' as periodo, count(*) as qtd, sum(valor_total) as faturamento"))
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->where(DB::raw('date(created_at)'), '>=', DB::raw('date_add(current_date, interval -3 month)'))            
                ->get()[0];
                
            $pedidos[5] = DB::query()
                ->from('pedidos')
                ->select(DB::raw("'ultimo_ano' as periodo, count(*) as qtd, sum(valor_total) as faturamento"))
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->where(DB::raw('date(created_at)'), '>=', DB::raw('date_add(current_date, interval -1 year)'))            
                ->get()[0];            
            
            
            //Incluindo dados no array
            foreach ($pedidos as $p) 
            {                    
                fputcsv($this->arquivo, [$p->periodo, $p->qtd, $p->faturamento], ';');
            }
            

            fclose($this->arquivo);

        }, Response::HTTP_OK, $headers);
    }




    public function relatorioVendasPorMes()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $this->farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        //Nome do relatório
        $nomeArquivo = 'Relatorio de vendas por mes' . date("d-m-Y H:i") . '.csv';       

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$nomeArquivo.'',
        ];


        return new StreamedResponse(function () {  
            //Abrindo arquivo
            $this->arquivo = fopen('php://output', 'w');

            //Setando a codficação do arquivo CSV para UTF-8
            fwrite($this->arquivo, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            
            //Cabeçalho das colunas
            fputcsv($this->arquivo, ['mes', 'quantidade', 'faturamento'], ';');
        
            //Dados - Query
            $pedidos = DB::query()
                ->from('pedidos')
                ->select(DB::raw("concat(year(created_at), '/', lpad(month(created_at), 2, '0')) as ref, count(*) as qtd, sum(valor_total) as faturamento"))                
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->groupBy('ref')
                ->orderBy('ref')
                ->get();
            
            //Incluindo dados no array
            foreach ($pedidos as $p) 
            {                    
                fputcsv($this->arquivo, [$p->ref, $p->qtd, $p->faturamento], ';');
            }
            

            fclose($this->arquivo);

        }, Response::HTTP_OK, $headers);
    }






    public function relatorioVendasPorRegiao()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $this->farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        //Nome do relatório
        $nomeArquivo = 'Relatorio de vendas por regiao' . date("d-m-Y H:i") . '.csv';       

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$nomeArquivo.'',
        ];


        return new StreamedResponse(function () {  
            //Abrindo arquivo
            $this->arquivo = fopen('php://output', 'w');

            //Setando a codficação do arquivo CSV para UTF-8
            fwrite($this->arquivo, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            
            //Cabeçalho das colunas
            fputcsv($this->arquivo, ['cidade', 'bairro', 'quantidade', 'faturamento'], ';');
        
            //Dados - Query
            $pedidos = DB::query()
                ->from('pedidos', 'p')
                ->select('enderecos.cidade', 'enderecos.bairro', DB::raw('count(*) as qtd, sum(valor_total) as faturamento'))                
                ->join('enderecos', 'enderecos.id', '=', 'p.id_endereco')
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->groupBy('enderecos.cidade')
                ->groupBy('enderecos.bairro')
                ->orderByDesc('qtd')
                ->get();
            
            //Incluindo dados no array
            foreach ($pedidos as $p) 
            {                    
                fputcsv($this->arquivo, [$p->cidade, $p->bairro, $p->qtd, $p->faturamento], ';');
            }
            

            fclose($this->arquivo);

        }, Response::HTTP_OK, $headers);
    }





    public function relatorioMaisVendidos()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $this->farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        //Nome do relatório
        $nomeArquivo = 'Relatorio dos 100 produtos mais vendidos' . date("d-m-Y H:i") . '.csv';       

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$nomeArquivo.'',
        ];
       

        return new StreamedResponse(function () {  
            //Abrindo arquivo
            $this->arquivo = fopen('php://output', 'w');

            //Setando a codficação do arquivo CSV para UTF-8
            fwrite($this->arquivo, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            
            //Cabeçalho das colunas
            fputcsv($this->arquivo, ['produto', 'cod_barras', 'quantidade', 'faturamento'], ';');
        
            //Dados - Query
            $pedidos = DB::query()
                ->from('pedidos', 'p')
                ->select('produtos.produto', 'produtos.cod_barras', DB::raw('sum(pedido_produtos.quantidade) as qtd, sum((pedido_produtos.valor - pedido_produtos.valor_desconto) * pedido_produtos.quantidade) as faturamento'))                                
                ->join('pedido_produtos', 'pedido_produtos.id_pedido', '=', 'p.id')
                ->join('produtos', 'produtos.id', '=', 'pedido_produtos.id_produto')
                ->where('id_farmacia', '=', $this->farmacia->id)
                ->where('pedido_produtos.confirmado', '=', 1)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->groupBy('produtos.produto')
                ->groupBy('produtos.cod_barras')
                ->orderByDesc('qtd')
                ->limit(100)
                ->get();
            
            //Incluindo dados no array
            foreach ($pedidos as $p) 
            {                    
                fputcsv($this->arquivo, [$p->produto, $p->cod_barras, $p->qtd, $p->faturamento], ';');
            }
            

            fclose($this->arquivo);

        }, Response::HTTP_OK, $headers);
    }






    public function relatorioMenosVendidos()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $this->farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        //Nome do relatório
        $nomeArquivo = 'Relatorio dos 100 produtos menos vendidos' . date("d-m-Y H:i") . '.csv';       

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$nomeArquivo.'',
        ];   
       

        return new StreamedResponse(function () {  
            //Abrindo arquivo
            $this->arquivo = fopen('php://output', 'w');

            //Setando a codficação do arquivo CSV para UTF-8
            fwrite($this->arquivo, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            
            //Cabeçalho das colunas
            fputcsv($this->arquivo, ['produto', 'cod_barras', 'quantidade', 'faturamento'], ';');
        
            //Dados - Query
            $pedidoProdutosQuery = DB::query()
            ->from('pedido_produtos')
            ->select('id_produto', 'id_pedido', 'quantidade', 'valor', 'valor_desconto')
            ->where('confirmado', '=', 1);
            
            $pedidosQuery =  DB::query()
            ->from('pedidos')
            ->select('id_farmacia', 'id')
            ->where('id_farmacia', '=', $this->farmacia->id)
            ->whereIn('id_status', [5, 6]); //5 - Concluído / 6 - Parcialmente concluído

            $pedidos = DB::query()
                ->from('produtos', 'p')
                ->select('p.produto', 'p.cod_barras', 
                    DB::raw('case
                                when sum(pp.quantidade) is null then 0
                                else sum(pp.quantidade) 
                            end as qtd, 
                            case
                                when sum((pp.valor - pp.valor_desconto) * pp.quantidade) is null then 0
                                else sum((pp.valor - pp.valor_desconto) * pp.quantidade)
                            end as faturamento'))                              
                ->leftJoinSub($pedidoProdutosQuery, 'pp', function ($join) { $join->on('pp.id_produto', '=', 'p.id'); })
                ->leftJoinSub($pedidosQuery, 'pe', function ($join) { $join->on('pe.id', '=', 'pp.id_pedido'); })                
                ->groupBy('p.produto')
                ->groupBy('p.cod_barras')
                ->orderBy('qtd')
                ->limit(100)
                ->get();


                /*$pedidos = DB::query()
                ->from('produtos', 'p')
                ->select('p.produto', 'p.cod_barras', 
                    DB::raw('sum(pp.quantidade) as qtd, sum((pp.valor - pp.valor_desconto) * pp.quantidade) as faturamento'))                              
                ->leftJoinSub($pedidoProdutosQuery, 'pp', function ($join) { $join->on('pp.id_produto', '=', 'p.id'); })
                ->leftJoinSub($pedidosQuery, 'pe', function ($join) { $join->on('pe.id', '=', 'pp.id_pedido'); })                
                ->groupBy('p.produto')
                ->groupBy('p.cod_barras')
                ->orderBy('qtd')
                ->limit(100)
                ->get();*/
            
            //Incluindo dados no array
            foreach ($pedidos as $p) 
            {                    
                fputcsv($this->arquivo, [$p->produto, $p->cod_barras, $p->qtd, $p->faturamento], ';');
            }
            

            fclose($this->arquivo);

        }, Response::HTTP_OK, $headers);
    }

    
    /*const SIZE_CHUNK = 200;
    
    const DOWNNLOAD_NAME = 'filename_example.csv';
    
    private $fileHandle;
    
    
    public function execute()
    {
        return new StreamedResponse(function () {
            
            $this->openFile();
            
            $this->addBomUtf8();
            
            $this->addContentHeaderInFile();
            
            $this->processUsers();
            
            $this->closeFile();
            
        }, Response::HTTP_OK, $this->headers());
    }
    
    private function openFile()
    {
        $this->fileHandle = fopen('php://output', 'w');
    }
    
    private function addBomUTf8 (){
        fwrite($this->fileHandle, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
    }
    
    private function addContentHeaderInFile()
    {
        $this->putRowInCsv([
            'Example header 1',
            'Example header 2',
            'Example header 3'
        ]);
    }
    
    private function processUsers()
    {
        User::with(['posts'])->chunk(self::SIZE_CHUNK, function ($users) {
            
            foreach ($users as $user) {
                $this->addUserLine($user);
            }
            
        });
    }
    
    private function addUserLine(User $user)
    {
        $this->putRowInCsv([    $user->exampleContent1(),
                            $user->exampleContent2(),
                            $user->exampleContent3()
                       ]);
    }
    
    private function putRowInCsv (array $data){
        fputcsv($this->fileHandle, $data, ';');
    }
    
    private function closeFile()
    {
        fclose($this->fileHandle);
    }
    
    private function headers()
    {
        return [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.self::DOWNNLOAD_NAME.'"',
        ];
    }*/

    /*public function relatorioTeste()
    {
        $nomeArquivo = 'teste.csv';

        $headers = [

            'Content-Disposition' => 'attachment; filename='. $nomeArquivo,

        ];

        $dados = DB::query()
            ->from('produto_farmacias', 'pf')
            ->select('produtos.produto', 'pf.estoque')
            ->join('produtos', 'produtos.id', 'pf.id_produto')
            ->where('pf.id_farmacia', '=', 1)
            ->get();   


        $colunas = ['produto', 'estoque'];

        //ddd($dados);

        return response()->stream(function() use($colunas){
        //$csv = response()->stream(function() use($colunas){
            $arquivo = fopen('php://output', 'w+');

            fputcsv($arquivo, $colunas);

            foreach($dados as $i => $valor)
            {
                //$dados[$i] = (array)$valor;

                fputcsv($arquivo, (array)$valor);
            }

            fclose($arquivo);

        }, 200, $headers); 
        
        
        //ddd($arquivo);

        //return view('relatorio/index');
    }*/


    /*public function relatorioTeste()
    {
        $name = 'users.csv';

        $headers = [

            'Content-Disposition' => 'attachment; filename='. $name,

        ];

        $colom = \Illuminate\Support\Facades\Schema::getColumnListing("usuarios");

        $temp_colom = array_flip($colom);

        unset($temp_colom['id']);

        $colom = array_flip($temp_colom);

        $file = fopen('php://output', 'w+');

        fputcsv($file, $colom);

        $data = \App\Models\Usuario::cursor();

        

        foreach ($data as $key => $value) {

            $data = $value->toArray();

            

            unset($data['id']);


            fputcsv($file, $data);

        }

        ddd($file);

        return response()->stream(function() use($colom){

            $file = fopen('php://output', 'w+');

            fputcsv($file, $colom);

            $data = \App\Models\Usuario::cursor();

            

            foreach ($data as $key => $value) {

                $data = $value->toArray();

                

                unset($data['id']);


                fputcsv($file, $data);

            }

            $blanks = array("\t","\t","\t","\t");

            fputcsv($file, $blanks);

            $blanks = array("\t","\t","\t","\t");

            fputcsv($file, $blanks);

            $blanks = array("\t","\t","\t","\t");

            fputcsv($file, $blanks);


            fclose($file);

        }, 200, $headers);        

    }*/

    /*public function relatorioTeste()
    {
        $nomeArquivo = 'teste.csv';

        $headers = [

            'Content-Disposition' => 'attachment; filename='. $nomeArquivo,

        ];

        $colunas = Schema::getColumnListing("usuarios");

        $temp_coluna = array_flip($colunas);

        unset($temp_coluna['id']);

        $colunas = array_flip($temp_coluna);

        //dd($colunas);



        return response()->stream(function() use($colunas){

            $arquivo = fopen('php://output', 'w+');

            fputcsv($arquivo, $colunas);

            $dados = Usuario::cursor();

            

            foreach ($dados as $key => $value) {

                $dados = $value->toArray();

                

                unset($dados['id']);


                fputcsv($arquivo, $dados);

            }           

            fclose($arquivo);

        }, 200, $headers);  

        //return view('relatorio/index');
    }*/


    /*public function exportExcel()
    {
        $name = 'users.csv';

        $headers = [

            'Content-Disposition' => 'attachment; filename='. $name,

        ];

        $colom = \Illuminate\Support\Facades\Schema::getColumnListing("users");

        $temp_colom = array_flip($colom);

        unset($temp_colom['id']);

        $colom = array_flip($temp_colom);

        return response()->stream(function() use($colom){

            $file = fopen('php://output', 'w+');

            fputcsv($file, $colom);

            $data = \App\Models\User::cursor();

            

            foreach ($data as $key => $value) {

                $data = $value->toArray();

                

                unset($data['id']);


                fputcsv($file, $data);

            }

            $blanks = array("\t","\t","\t","\t");

            fputcsv($file, $blanks);

            $blanks = array("\t","\t","\t","\t");

            fputcsv($file, $blanks);

            $blanks = array("\t","\t","\t","\t");

            fputcsv($file, $blanks);


            fclose($file);

        }, 200, $headers);        

    }*/

}
