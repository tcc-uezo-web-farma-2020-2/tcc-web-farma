<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\SessaoService;
use App\Services\PedidoService;
use Auth;

class CarrinhoController extends Controller
{
    public function carrinho(Request $request)
    {
        $sessao = new SessaoService();
        $endereco = $sessao->getEndereco();

        $carrinho = $this->calculaTotalCarrinho();
        
        $carrinho['endereco'] = $endereco['endereco'];

        return view('carrinho/carrinho', $carrinho);
    }

    public function removeProdutoCarrinho(Request $request)
    {
        //ddd(session('cart'));

        $carrinho = session('cart', []);

        //ddd($carrinho[$request->indice]);
        
        if(empty($carrinho) || empty($carrinho[$request->indice]))
        {
            $mensagem = 'O produto já foi removido do carrinho';
            session()->flash('msg_erro', $mensagem);
        }
        else
        {
            unset($carrinho[$request->indice]);
            session(['cart' => $carrinho]);
        }
        

        return redirect()->back();
        
    }

    public function alteraProdutoCarrinho(Request $request)
    {
        //ddd(session('cart'));

        $carrinho = session('cart', []);

        //ddd($request->indice);

        //ddd($carrinho);

        //ddd($carrinho[$request->indice]);
        
        if(empty($carrinho) || empty($carrinho[$request->indice]))
        {
            $mensagem = 'Produto não encontrado';
            session()->flash('msg_erro', $mensagem);
        }
        else if ($request->quantidade < 1)
        {
            $mensagem = 'Quantidade inválida';
            session()->flash('msg_erro', $mensagem);
        }
        else
        {
            $prodFarm = DB::query()
            ->from('produto_farmacias', 'pf')    
            ->select('pf.estoque')
            ->where('pf.id', '=', $carrinho[$request->indice][0]->id_produto_farmacia)
            ->get();
            
            if($prodFarm[0]->estoque >= $request->quantidade)
            {
                $carrinho[$request->indice][0]->quantidade = $request->quantidade;
                $mensagem = 'Quantidade atualizada com sucesso';
                session()->flash('msg_sucesso', $mensagem);
            }
            else
            {
                if($prodFarm[0]->estoque == 0)
                {
                    $mensagem = 'Estoque indisponível';
                    session()->flash('msg_erro', $mensagem);
                    $carrinho[$request->indice][0]->quantidade = 1;
                }
                else
                {
                    $mensagem = 'Quantidade indisponível no estoque';
                    session()->flash('msg_erro', $mensagem);
                    //$carrinho[$request->indice][0]->quantidade = $prodFarm[0]->estoque;
                }
            }
            session(['cart' => $carrinho]);
        }
        

        return redirect()->back();
        
    }


    public function checkout()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login');
        }

        $statusCarrinho = $this->verificaCarrinho();

        if($statusCarrinho['vazio'])
        {
            $mensagem = 'Não existem produtos no carrinho';
            session()->flash('msg_erro', $mensagem);
            return view('cliente/telaErro');
        }

        if(!$statusCarrinho['areaEntrega'])
        {
            $mensagem = 'O carrinho contém produtos de farmácias que estão fechadas ou não entregam no endereço selecionado';
            session()->flash('msg_erro', $mensagem);
            return redirect()->back();
        }

        if($statusCarrinho['qtdFarmacias'] > 1)
        {
            $mensagem = 'O carrinho contém produtos de farmácias diferentes. Favor fazer um pedido por farmácia';
            session()->flash('msg_erro', $mensagem);
            return redirect()->back();
        }

        if(!$statusCarrinho['estoque'])
        {
            $mensagem = 'O(s) produto(s) ' . $statusCarrinho['produtoSemEstoque'] . ' estão sem estoque para a quantidade selecionada';
            session()->flash('msg_erro', $mensagem);
            return redirect()->back();
        }

        $formasPag = $this->calculaTotalCarrinho();      
        $formasPag['formas_pagamento'] = DB::table('forma_pagamentos')->get();

        //ddd($formasPag);
        
        return view('carrinho/checkout', $formasPag);
        
    }


    public function calculaTotalCarrinho()
    {
        $carrinho = session('cart', []);
        $carrinho = ['itens' => $carrinho];
        //ddd($carrinho['itens']);
        $subtotal = 0;
        $totalEntregas = 0;
        $entregas = [];
        foreach ($carrinho['itens'] as $produto)
        {
            //ddd($produto[0]);
            $subtotal += $produto[0]->valor * $produto[0]->quantidade;

            $qtdEntregas = count($entregas);
            //Colocando o valor das entregas (se tiver mais de um produto da mesma farmácia, calcula a entrega apena 1x)
            if($qtdEntregas == 0)
            {
                $entregas[$qtdEntregas] = new DB();            
                $entregas[$qtdEntregas]->id_farmacia = $produto[0]->id_farmacia;
                $totalEntregas += $produto[0]->entrega;
                //$entregas[$qtdEntregas]->entrega = $produto[0]->entrega;
            }
            else
            {
                foreach($entregas as $i => $ent)
                {
                    //ddd($entregas, $produto);
                    if($ent->id_farmacia == $produto[0]->id_farmacia)
                    {
                        break;
                    }

                    if($i == $qtdEntregas - 1)
                    {
                        $entregas[$qtdEntregas] = new DB();
                        $entregas[$qtdEntregas]->id_farmacia = $produto[0]->id_farmacia;
                        $totalEntregas += $produto[0]->entrega;
                        //$entregas[$qtdEntregas]->entrega = $produto[0]->entrega;
                        break;
                    }
                }
            }
        }

        $carrinho['subtotal'] = $subtotal;
        $carrinho['totalEntrega'] = $totalEntregas;

        return $carrinho;
    }


    public function finalizaPedido(Request $request)
    {
        $usuario = Auth::user();

        //Verificando se o usuário está logado
        if(empty($usuario))
        {
            return redirect()->route('login');
        }

        //Verificando se existe uma forma de pagamento
        if(!isset($request->pagamento))
        {
            $mensagem = 'Selecione uma forma de pagamento para continuar';
            session()->flash('msg_atencao', $mensagem);
            return redirect()->back();
        }

        //Verificando se a forma de pagamento é válida
        $formaPag = DB::table('forma_pagamentos')
            ->where('id', '=', $request->pagamento)
            ->get();

        if(empty($formaPag[0]))
        {
            $mensagem = 'Forma de pagamento inválida';
            session()->flash('msg_erro', $mensagem);
            return redirect()->back();
        }


        //Verificando o status do carrinho
        $statusCarrinho = $this->verificaCarrinho();

        if($statusCarrinho['vazio'])
        {
            $mensagem = 'Não existem produtos no carrinho';
            session()->flash('msg_erro', $mensagem);
            return view('cliente/telaErro');
        }

        if(!$statusCarrinho['areaEntrega'])
        {
            $mensagem = 'O carrinho contém produtos de farmácias que estão fechadas ou não entregam no endereço selecionado';
            session()->flash('msg_erro', $mensagem);
            return redirect()->route('carrinho');
        }

        if($statusCarrinho['qtdFarmacias'] > 1)
        {
            $mensagem = 'O carrinho contém produtos de farmácias diferentes. Favor fazer um pedido por farmácia';
            session()->flash('msg_erro', $mensagem);
            return redirect()->route('carrinho');
        }

        if(!$statusCarrinho['estoque'])
        {
            $mensagem = 'O(s) produto(s) ' . $statusCarrinho['produtoSemEstoque'] . ' estão sem estoque para a quantidade selecionada';
            session()->flash('msg_erro', $mensagem);
            return redirect()->route('carrinho');
        }

        //Calculando o total do carrinho
        $carrinho = $this->calculaTotalCarrinho();

        $service = new PedidoService();

        //Inserindo o pedido no banco de dados
        $pedido = $service->cadastraPedido($usuario, $carrinho, $request->pagamento);

        if($pedido['sucesso'] == 0)
        {
            session()->flash('msg_erro', $pedido['mensagem']);
            return redirect()->back();
        }

        //Limpando os itens do carrinho
        session()->forget('cart');

        $botao['meus_pedidos'] = 1;
        session()->flash('msg_sucesso', $pedido['mensagem']);
        return view('cliente/telaSucesso', $botao);
    }


    public function verificaCarrinho()
    {
        $carrinho = session('cart', []);

        $retorno = [];
        $retorno['vazio'] = false;
        $retorno['areaEntrega'] = true;

        $retorno['estoque'] = true;        
        $retorno['produtoSemEstoque'] = '';

        //Verificando se existem itens no carrinho
        if(empty($carrinho))
        {
            $retorno['vazio'] = true;
            return $retorno;
        }
        

        $sessao = new SessaoService();
        $farmaciasRegiao = $sessao->getFarmaciasRegiao()->get();
        $distinctFarmacias = [];

        

        //Verificando se a farmácia do carrinho entrega no endereço da sessão ou se existe mais de uma farmácia no carrinho
        foreach($carrinho as $c)
        {
            //Verificando quantas farmácias diferentes existem no carrinho
            if(count($distinctFarmacias) == 0)
            {
                $distinctFarmacias[0] = $c[0]->id_farmacia;
            }
            else
            {
                foreach($distinctFarmacias as $i => $df)
                {  
                   
                    if($df == $c[0]->id_farmacia)
                    {
                        break;
                    }
                    if($i == count($distinctFarmacias) - 1)
                    {
                        $distinctFarmacias[count($distinctFarmacias)] = $c[0]->id_farmacia;
                    }
                }
            }

            //Verificando se todas as farmácias do carrinho entregam no endereço selecionado
            $farmaciaDentroAreaEntrega = false;
            foreach($farmaciasRegiao as $f)
            {
                if($c[0]->id_farmacia == $f->id)
                {
                    $farmaciaDentroAreaEntrega = true;
                    break;
                }
            }
            
            if(!$farmaciaDentroAreaEntrega)
            {
                $retorno['areaEntrega'] = false;
                return $retorno;
            } 
            
            
            //Verificando se o estoque atual dos produtos é maior ou igual a quantidade selecionada
            $estoque = DB::table('produto_farmacias')
            ->where('id', '=', $c[0]->id_produto_farmacia)
            ->get();

            //ddd($c[0]->quantidade);

            if($c[0]->quantidade > $estoque[0]->estoque)
            {
                $retorno['estoque'] = false;
                if($retorno['produtoSemEstoque'] == '')
                {
                    $retorno['produtoSemEstoque'] = $c[0]->produto;
                }
                else
                {
                    $retorno['produtoSemEstoque'] = $retorno['produtoSemEstoque'] . ', ' . $c[0]->produto;
                }                
            }
        }

        $retorno['qtdFarmacias'] = count($distinctFarmacias);

        return $retorno;
    }
}
