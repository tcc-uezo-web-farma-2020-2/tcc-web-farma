<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Categoria;
use App\Models\Produto;
use App\Services\SessaoService;
use Auth;

class ProdutoController extends Controller
{
    public function index(Request $request)
    {
        $usuario = Auth::user();

        if(!empty($usuario) && $usuario->id_perfil == 2)
        {
            return redirect()->route('index_farmacia');
        }

        $sessao = new SessaoService();
        $endereco = $sessao->getEndereco();

        $produtosFarmacia = DB::query()
            ->fromSub($this->listaFarmaciasRegiao(), 'f')
            ->distinct()
            ->select('produto_farmacias.id_produto')
            ->join('produto_farmacias', 'produto_farmacias.id_farmacia', '=', 'f.id')
            ->where('produto_farmacias.estoque', '>',  0)
            ->paginate(16);
            //->get();
        
        $listaProdutos['lista'] = $this->listaProdutoFarmacia($produtosFarmacia);
        $listaProdutos['endereco'] = $endereco['endereco'];

        return view('cliente/index', $listaProdutos);
    }



    public function categoria(Request $request)
    {
        $sessao = new SessaoService();
        $endereco = $sessao->getEndereco();

        //ddd($request->nomecat);
        $listaCategorias = DB::query()
        ->from('categorias', 'c')
        ->select('c.categoria', 'c.id', 'c.slug'); 
        
        if($request->nomecat == null)
        {
            $produtosFarmacia = DB::query()
            ->fromSub($this->listaFarmaciasRegiao(), 'f')
            ->distinct()
            ->select('produto_farmacias.id_produto')
            ->join('produto_farmacias', 'produto_farmacias.id_farmacia', '=', 'f.id')
            ->where('produto_farmacias.estoque', '>',  0)
            ->paginate(16);
            //->get();

            $catEscolhida = 'Categorias';
        }
        else
        {
            $produtosFarmacia = DB::query()
            ->fromSub($this->listaFarmaciasRegiao(), 'f')
            ->distinct()
            ->select('produto_farmacias.id_produto')
            ->join('produto_farmacias', 'produto_farmacias.id_farmacia', '=', 'f.id')
            ->join('produtos', 'produto_farmacias.id_produto', '=', 'produtos.id')
            ->join('categorias', 'produtos.id_categoria', '=', 'categorias.id')
            ->where('produto_farmacias.estoque', '>', 0)
            ->where('categorias.slug', '=', $request->nomecat)
            ->paginate(16);
            //->get();

            $catEscolhida =  DB::query()
            ->from('categorias', 'c')
            ->select('c.categoria')
            ->where('c.slug', '=', $request->nomecat)
            ->get(); 


            $catEscolhida = $catEscolhida[0]->categoria;
        }
        //ddd($produtosFarmacia);

        $list['produto'] = $this->listaProdutoFarmacia($produtosFarmacia);

        if(empty($produtosFarmacia[0])) //nome padrão, url da rota
        {
            $mensagem = 'Não existem produtos nessa categoria';
                session()->flash('msg_atencao', $mensagem);
                //redirect()->back();
            //ddd($mensagem);
        }

        else
        {
            $list['produto'] = $this->listaProdutoFarmacia($produtosFarmacia);
        }

        $list['categoria'] = $listaCategorias->get();
        $list['categoria']->catGet = $request->nomecat; //Retornando a categoria escolhida para a view, para aplicar o estilo "active do bootstratp"
        $list['categoria']->nomeCatGet = $catEscolhida;
        $list['endereco'] = $endereco['endereco'];       

        //ddd($list);
        //ddd($list['produto']);
        //ddd($list['endereco']);

        return view('produto/categoria', $list);
    }


    public function pesquisa(Request $request)
    {
        $sessao = new SessaoService();
        $endereco = $sessao->getEndereco();
        //ddd($request->pesquisa);

        $list['categoria'] = DB::query()
        ->from('categorias', 'c')
        ->select('c.categoria', 'c.id', 'c.slug')
        ->get();

        //ddd($request->pesquisa);

        $produtosFarmacia = DB::query()
            ->fromSub($this->listaFarmaciasRegiao(), 'f')
            ->distinct()
            ->select('produto_farmacias.id_produto')
            ->join('produto_farmacias', 'produto_farmacias.id_farmacia', '=', 'f.id')
            ->join('produtos', 'produto_farmacias.id_produto', '=', 'produtos.id')
            ->where('produto_farmacias.estoque', '>', 0)
            ->where(function($query) use($request) {
                    $query->whereRaw("retira_acentuacao(produtos.produto) like '%" . $request->pesquisa . "%'")
                        ->orWhereRaw("retira_acentuacao(produtos.composicao) like '%" . $request->pesquisa . "%'");
            })
            ->paginate(16);      
            //->get();

        //ddd($produtosFarmacia);

        if(empty($produtosFarmacia[0]))
        {
            $mensagem = 'Nenhum produto encontrado';
            session()->flash('msg_atencao', $mensagem);
        }

        $list['produto'] = $this->listaProdutoFarmacia($produtosFarmacia);

        $list['categoria']->catGet = 'categorias';
        $list['categoria']->nomeCatGet = 'Exibindo resultados para: ' . $request->pesquisa;
        $list['endereco'] = $endereco['endereco'];

        return view('produto/categoria', $list);
    }




    public function detalhes(Request $request)
    {
        $farmaciasRegQuery = $this->listaFarmaciasRegiao();

        //ddd($request->nomeprod);

        //Verficando se a farmácia entrega no endereço e se possui o produto
        if(isset($request->id_farmacia))
        {
            $farmacia = DB::query()
            ->from('farmacias')    
            ->where('id', '=', $request->id_farmacia)
            ->get(); 

            if(empty($farmacia[0]))
            {
                $mensagem = 'Produto não encontrado';
                session()->flash('msg_erro', $mensagem);
                return redirect()->back();
            }

            $sessao = new SessaoService();
            $farmaciasRegiao = $sessao->getFarmaciasRegiao()->get();
            $farmaciaDentroAreaEntrega = false;

            foreach($farmaciasRegiao as $farm)
            {
                if($farm->id == $request->id_farmacia)
                $farmaciaDentroAreaEntrega = true;
            }

            if(!$farmaciaDentroAreaEntrega)
            {
                $mensagem = 'Produto não encontrado';
                session()->flash('msg_erro', $mensagem);
                return redirect()->back();
            }
        }

        
        $produto = DB::query()
        ->from('produtos', 'p')    
        ->where('p.slug', '=', $request->nomeprod)
        ->get();        
        
        //ddd($prod);

        if(empty($produto[0]))
        {
            $mensagem = 'Produto não encontrado';
            session()->flash('msg_erro', $mensagem);
            return redirect()->back();
        }

        if(isset($request->id_farmacia))
        {
            //Listando produto da farmácia selecionada
            $prod['escolhido'] = DB::query()
            ->fromSub($farmaciasRegQuery, 'f')
            ->select('f.id as id_farmacia', 'f.nome_farmacia', 'f.entrega', 'produto_farmacias.id as id_produto_farmacia', 'produto_farmacias.estoque', 'produto_farmacias.valor', 'produtos.id as id_produto', 'produtos.produto', 'produtos.slug', 'produtos.imagens', 'produtos.composicao')
            ->join('produto_farmacias', 'f.id', '=', 'produto_farmacias.id_farmacia')
            ->join('produtos', 'produtos.id', '=', 'produto_farmacias.id_produto')
            ->where('produtos.id', '=', $produto[0]->id)
            ->where('f.id', '=', $request->id_farmacia)
            ->where('produto_farmacias.estoque', '>', 0)
            ->get();

            if(empty($prod['escolhido'][0]))
            {
                $mensagem = 'Produto não encontrado';
                session()->flash('msg_erro', $mensagem);
                return redirect()->back();
            }

            //Pega as outra opções de farmácia
            $prod['outrasFarmacias'] = DB::query()
            ->fromSub($farmaciasRegQuery, 'fr')
            ->select('fr.id as id_farmacia', 'fr.nome_farmacia', 'produto_farmacias.valor', 'produto_farmacias.estoque', 'fr.entrega')
            ->join('produto_farmacias', 'fr.id', '=', 'produto_farmacias.id_farmacia')
            ->join('produtos', 'produto_farmacias.id_produto', '=', 'produtos.id')
            ->where('produtos.id', '=', $produto[0]->id)
            ->where('fr.id', '<>', $request->id_farmacia)
            ->where('fr.aberto', '=', 1)
            ->where('produto_farmacias.estoque', '>', 0)
            ->orderBy('produto_farmacias.valor', 'asc')
            ->limit(10)
            ->get();
        }
        //Ordena por preço, exiibe o mais barato e da a opção a partir do segundo mais barato
        else
        {
            //Listando as informações do produto da farmácia mais barata
            $produtosFarmacia[0] = new DB();      
            $produtosFarmacia[0]->id_produto = $produto[0]->id;

            $prod['escolhido'] = $this->listaProdutoFarmacia($produtosFarmacia);

            $prod['outrasFarmacias'] = DB::query()
            ->fromSub($farmaciasRegQuery, 'fr')
            ->select('fr.id as id_farmacia', 'fr.nome_farmacia', 'produto_farmacias.valor', 'produto_farmacias.estoque', 'fr.entrega')
            ->join('produto_farmacias', 'fr.id', '=', 'produto_farmacias.id_farmacia')
            ->join('produtos', 'produto_farmacias.id_produto', '=', 'produtos.id')
            ->where('produtos.id', '=', $produto[0]->id)
            ->where('fr.aberto', '=', 1)
            ->where('produto_farmacias.estoque', '>', 0)
            ->orderBy('produto_farmacias.valor', 'asc')
            ->skip(1)->take(10)
            ->get();
        }
        
        return view('produto/detalheProduto', $prod);

                
    }

    

    public function adicionaProdutoCarrinho(Request $request)
    {
        //ddd($request->quantidade);
        
        //$farmaciasRegQuery = $this->listaFarmaciasRegiao();

        $prod = DB::query()
        ->from('produto_farmacias', 'pf')    
        ->where('pf.id', '=', $request->id)
        ->get();

        //ddd($prod);

        if($request->quantidade > $prod[0]->estoque)
        {
            $mensagem = 'Estoque indisponível para a quantidade desejada';
            session()->flash('msg_erro', $mensagem);
            return redirect()->back();
        }

        $produtosFarmacia[0] = new DB();      
        $produtosFarmacia[0]->id_produto = $prod[0]->id_produto;

        $prod = DB::query()
        ->fromSub($this->listaFarmaciasRegiao(), 'f')
        ->select('f.id as id_farmacia', 'f.nome_farmacia', 'f.entrega', 'produto_farmacias.id as id_produto_farmacia', 'produto_farmacias.estoque', 'produto_farmacias.valor', 'produtos.id as id_produto', 'produtos.produto', 'produtos.slug', 'produtos.imagens', 'produtos.composicao')
        ->join('produto_farmacias', 'f.id', '=', 'produto_farmacias.id_farmacia')
        ->join('produtos', 'produtos.id', '=', 'produto_farmacias.id_produto')
        ->where('produto_farmacias.id', '=', $request->id)
        ->get();
        

        $carrinho = session('cart', []);
        $indice = -1;

        
        //Verificando se o item ja existe no carrinho
        if(!empty($carrinho))
        {
            foreach($carrinho as $i => $prodCar)
            {   
                //ddd($prodCar[$i]->id);
                if($prod[0]->id_produto_farmacia == $prodCar[0]->id_produto_farmacia)
                {
                    $indice = $i;
                }
            }
        }
        
        //ddd($prod[0]); 

        if($indice == -1)
        {  
            $prod[0]->quantidade = $request->quantidade;
            array_push($carrinho, $prod);
            session(['cart' => $carrinho]);            
        }
        else
        {            
            if($prod[0]->estoque >= $carrinho[$indice][0]->quantidade + $request->quantidade)
            {
                $carrinho[$indice][0]->quantidade += $request->quantidade;                
            }
            else
            {
                $mensagem = 'Estoque indisponível para a quantidade desejada';
                session()->flash('msg_erro', $mensagem);           
                return redirect()->back();                     
                //redirect()->route('index', ['estoque' => 'indisponivel']);
            }
        }

        $mensagem = 'Produto adicionado com sucesso ao carrinho';
        session()->flash('msg_sucesso', $mensagem);

        return redirect()->back();        
    }

    public function listaFarmaciasRegiao()
    {
        $sessao = new SessaoService();
        return $sessao->getFarmaciasRegiao();
    }
    
    public function listaProdutoFarmacia($produtosFarmacia)
    {
        $produtos = $produtosFarmacia;
        
        //Pegando o menor preço de cada produto
        foreach($produtosFarmacia as $i => $pf)
        {
            $produto = DB::query()
            ->fromSub($this->listaFarmaciasRegiao(), 'f')
            ->select('f.id as id_farmacia', 'f.nome_farmacia', 'f.entrega', 'produto_farmacias.id as id_produto_farmacia', 'produto_farmacias.estoque', 'produto_farmacias.valor', 'produtos.id as id_produto', 'produtos.produto', 'produtos.slug', 'produtos.imagens', 'produtos.composicao')
            ->join('produto_farmacias', 'f.id', '=', 'produto_farmacias.id_farmacia')
            ->join('produtos', 'produtos.id', '=', 'produto_farmacias.id_produto')
            ->where('produtos.id', '=', $produtosFarmacia[$i]->id_produto)
            ->where('produto_farmacias.estoque', '>', 0)
            ->orderBy('produto_farmacias.valor', 'asc')
            ->orderBy('f.entrega', 'asc')
            ->orderBy('produto_farmacias.estoque', 'desc')
            ->limit(1)
            ->get();
            
            //ddd($produto);

            $produtos[$i] = $produto[0];
        }   
        //ddd($produtos);
        return $produtos;
    }


    /*function transformaLink($string) 
    {
        $string = strtolower($string);
        $string = str_replace(' ', '-', $string);

        $substituir = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-',
            '/[’‘‹›‚]/u'    =>   '',
            '/[“”«»„]/u'    =>   '',
            '/ /'           =>   '',
        );

        return preg_replace(array_keys($substituir), array_values($substituir), $string);
    }*/
}
