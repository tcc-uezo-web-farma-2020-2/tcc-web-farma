<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailCadastro;
use App\Mail\EmailRecuperacao;
use App\Mail\EmailAlteracaoEmail;
use App\Mail\EmailConfirmacaoPedidoCliente;
use App\Mail\EmailConfirmacaoPedidoFarmacia;
use App\Mail\EmailClienteCancelaPedidoCliente;
use App\Mail\EmailClienteCancelaPedidoFarmacia;
use App\Mail\EmailFarmaciaCancelaPedidoCliente;
use App\Mail\EmailFarmaciaCancelaPedidoFarmacia;
use App\Mail\EmailPedidoConcluidoCliente;
use App\Mail\EmailPedidoConcluidoFarmacia;


class EmailController extends Controller
{
    //Funções estáticas para facilitar a chamada, sem precisar instanciar a classe

    //Confirmação de cadastro e verificação de e-mail
    public static function enviaEmailCadastro($nome, $email, $codigo_verificacao)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'codigo_verificacao' => $codigo_verificacao];
        Mail::to($email)->send(new EmailCadastro($dados));
    }



    
    //Verificação de e-mail após alteração
    public static function enviaEmailAlteracaoEmail($nome, $email, $codigo_verificacao)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'codigo_verificacao' => $codigo_verificacao];
        Mail::to($email)->send(new EmailAlteracaoEmail($dados));
    }




    //Recuperação de senha (esqueci a senha)
    public static function enviaEmailRecuperacao($nome, $email, $codigo_recuperacao)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'codigo_recuperacao' => $codigo_recuperacao];
        Mail::to($email)->send(new EmailRecuperacao($dados));
    }




    //E-mails de confirmação de pedido, clente e farmácia
    public static function enviaEmailConfirmacaoPedidoCliente($nome, $email, $id_pedido)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'pedido' => $id_pedido];
        Mail::to($email)->send(new EmailConfirmacaoPedidoCliente($dados));
    }

    public static function enviaEmailConfirmacaoPedidoFarmacia($nome, $email, $id_pedido)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'pedido' => $id_pedido];
        Mail::to($email)->send(new EmailConfirmacaoPedidoFarmacia($dados));
    }




    //E-mails de cancelamento de pedido pelo lado do cliente (após 10 min sem confirmação por parte da farmácia)
    public static function enviaEmailClienteCancelaPedidoCliente($nome, $email, $id_pedido)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'pedido' => $id_pedido];
        Mail::to($email)->send(new EmailClienteCancelaPedidoCliente($dados));
    }

    public static function enviaEmailClienteCancelaPedidoFarmacia($nome, $email, $id_pedido)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'pedido' => $id_pedido];
        Mail::to($email)->send(new EmailClienteCancelaPedidoFarmacia($dados));
    }




    //E-mails de cancelamento de pedido pelo lado da farmácia
    public static function enviaEmailFarmaciaCancelaPedidoCliente($nome, $email, $id_pedido)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'pedido' => $id_pedido];
        Mail::to($email)->send(new EmailFarmaciaCancelaPedidoCliente($dados));
    }

    public static function enviaEmailFarmaciaCancelaPedidoFarmacia($nome, $email, $id_pedido)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'pedido' => $id_pedido];
        Mail::to($email)->send(new EmailFarmaciaCancelaPedidoFarmacia($dados));
    }




    //E-mails de de pedido concluído
    public static function enviaEmailPedidoConcluidoCliente($nome, $email, $id_pedido, $id_status)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'pedido' => $id_pedido, 'id_status' => $id_status];
        Mail::to($email)->send(new EmailPedidoConcluidoCliente($dados));
    }

    public static function enviaEmailPedidoConcluidoFarmacia($nome, $email, $id_pedido, $id_status)
    {
        $dados = ['nome' => $nome, 'email' => $email, 'pedido' => $id_pedido, 'id_status' => $id_status];
        Mail::to($email)->send(new EmailPedidoConcluidoFarmacia($dados));
    }
}
