<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use App\Services\UsuarioService;
use App\Models\Farmacia;
use App\Models\Usuario;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;

class UsuarioController extends Controller
{
    public function verificaEmailUsuario(Request $request)
    {
        //codigo enviado via get pro e-mail do usuário
        $codigo_verificacao = $request->codigo;

        //ddd($codigo_verificacao);
        $usuario = Usuario::where(['codigo_verificacao' => $codigo_verificacao])->first();

        if(!empty($usuario))
        {
            if($usuario->email_verificado == 1)
            {
                session()->flash('msg_sucesso', 'E-mail já verificado');
            }
            else
            {
                $usuario->email_verificado = 1;
                //ddd($usuario);
                $usuario->save();

                session()->flash('msg_sucesso', 'E-mail verificado com sucesso! Clique no botão abaixo para fazer login');
            }

            if($usuario->id_perfil == 1)
            {
                return redirect()->route('tela_sucesso');
            }        
            else
            {
                return redirect()->route('tela_sucesso_farmacia');
            }
        }
        else
        {
            session()->flash('msg_erro', 'Código de verificação inválido');
            return redirect()->route('tela_erro');
        }
    }

    public function telaLogin()
    {
        $usuario = Auth::user();       

        if(!empty($usuario))
        {  
            if($usuario->id_perfil == 1)
            {
                return redirect()->route('index');
            }          
            else
            {          
                return redirect()->route('index_farmacia');
            }
            
        }

        $url = url()->current();

        //ddd($url);

        if($url == 'http://localhost:8000/login')
        {           
            return view('cliente/login');       
        }
             

        return view('farmacia/login');
        
    }




    public function esqueciSenha()
    {
        
        if(url()->previous() == 'http://localhost:8000/login')
        {
            return view('cliente/esqueciSenha');
        }          
        else
        {          
            return view('farmacia/esqueciSenha');
        }
    }




    public function enviaRecuperacaoSenha(Request $request)
    {        
        $usuario = Usuario::where(['email' => $request->email])->first();

        //ddd($usuario);

        //ddd(Carbon::now());

        if(!empty($usuario))
        {
            $token = Str::random(60);            
            
            $reset = DB::table('password_resets')            
                ->where('email', '=', $request->email)                
                ->get();

            //Se o usuário ja tiver uma requisição de reset de senha, apaga para deixar a mais recente
            if(!empty($reset[0]))
            {
                DB::table('password_resets')->where(['email'=> $request->email])->delete();
            }

            DB::table('password_resets')->insert(['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()]);

            

            //Envia e-mail de confirmação
            EmailController::enviaEmailRecuperacao($usuario->nome, $usuario->email, $token);
        }

        session()->flash('msg_sucesso', 'Recuperação de senha enviada com sucesso! Se houver um usuário com esse e-mail cadastrado, um e-mail de recuperação de senha será enviado');
        if($usuario->id_perfil == 1)
        {
            return redirect()->route('tela_sucesso');
        }        
        else
        {
            return redirect()->route('tela_sucesso_farmacia');
        }
        
    }





    //Verifica se o código do link é válido ou se não está expirado
    public function recuperaSenha(Request $request)
    {
        //ddd($request);


        $reset = DB::table('password_resets')            
            ->select('email', 'token', DB::raw('date_add(created_at, interval +1 day) as data_expiracao'))            
            ->where('token', '=', $request->codigo)
            ->get();

        if(!empty($reset[0]))
        {
            $data_expiracao = Carbon::createFromFormat('Y-m-d H:i:s', $reset[0]->data_expiracao);
            $data_atual = Carbon::now();

            //ddd($data_expiracao);

            $token['codigo'] = $request->codigo;

            $usuario = Usuario::where('email', $reset[0]->email)->first();

            if($data_atual->gt($data_expiracao))
            {                
                session()->flash('msg_erro', 'Código de recuperação expirado');
                if($usuario->id_perfil == 1)
                {
                    return redirect()->route('tela_erro');
                }        
                else
                {
                    return redirect()->route('tela_erro_farmacia');
                }                
            }
            else
            {
                if($usuario->id_perfil == 1)
                {
                    return view('cliente/redefinicaoSenha', $token);
                }        
                else
                {
                    return view('farmacia/redefinicaoSenha', $token);
                }
                
            }
        }
        else
        {
            session()->flash('msg_erro', 'Código de recuperação inválido');
            return redirect()->route('tela_erro');
        }        
    }




    public function redefineSenha(Request $request)
    {
        //ddd($request);

        //Verifica se o código do link é válido ou se não está expirado
        $reset = DB::table('password_resets')            
            ->select('email', 'token', DB::raw('date_add(created_at, interval +1 day) as data_expiracao'))            
            ->where('token', '=', $request->codigo)
            ->get();

        if(!empty($reset[0]))
        {
            $data_expiracao = Carbon::createFromFormat('Y-m-d H:i:s', $reset[0]->data_expiracao);
            $data_atual = Carbon::now();
            
            $request->validate([
                'password' => 'required|confirmed'
            ]);

            DB::table('password_resets')->where(['token'=> $request->codigo])->delete();

            $usuario = Usuario::where('email', $reset[0]->email)->first();

            //data_atual greater data_expiracao (data_atual > data_expiracao) - Método do carbon
            if($data_atual->gt($data_expiracao))
            {
                session()->flash('msg_erro', 'Código de recuperação expirado');
                if($usuario->id_perfil == 1)
                {
                    return redirect()->route('tela_erro');
                }        
                else
                {
                    return redirect()->route('tela_erro_farmacia');
                }                
            }
            else
            {                
                $usuario->password = Hash::make($request->password);
                $usuario->save();
                
                session()->flash('msg_sucesso', 'Senha alterada com sucesso!');

                if($usuario->id_perfil == 1)
                {
                    return redirect()->route('tela_sucesso');
                }        
                else
                {
                    return redirect()->route('tela_sucesso_farmacia');
                }               
            }
        }
        else
        {
            session()->flash('msg_erro', 'Código de recuperação inválido');
            return redirect()->route('tela_erro');
        }        
    }




    public function autenticar(Request $request)
    {        

        $dados = $request->only('email', 'password');

        //Usuario logado
        if (Auth::attempt($dados))
        {
            if(Auth::user()->id_perfil == 1)
            {
                return redirect()->route('index');
            }            
            else
            {
                $farmUsuario = DB::table('farmacias')
                ->where('id_usuario', '=', Auth::user()->id)
                ->get();

                //Apagando farmacia sessão, caso exista
                session()->forget('farmacia');

                $farmSessao = session('farmacia', []);
                array_push($farmSessao, $farmUsuario[0]);
                session(['farmacia' => $farmSessao]);                
                return redirect()->route('index_farmacia');
            }
        }
        //Falha na autenticação
        else
        {            
            throw ValidationException::withMessages(['email' => 'E-mail e/ou senha inválido(s)']);
        }        
    }


    //Tela de alteração de cadastro
    public function alteraDados()
    {
        $usuario['usuario'] = Auth::user();

        if(empty($usuario['usuario']))
        {            
            return redirect()->route('login');
        }

        if($usuario['usuario']->id_perfil == 1)
        {
            return view('cliente/alteracaoConta', $usuario);
        }

        return view('farmacia/alteracaoContaAdm', $usuario);
    }


    public function alteraCadastro(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        { 
            if(str_contains(url()->current(), 'farmacia'))
            {
                return redirect()->route('login_farmacia');
            }
            return redirect()->route('login');
        }

        if($usuario->nome != $request->nome || $usuario->cpf != $request->cpf || $usuario->data_nascimento != $request->data_nascimento)
        {
            session()->flash('msg_erro', 'Não é permitido alterar o nome, CPF ou a data de nascimento');
            return redirect()->back();
        }

        if($usuario->email != $request->email)
        {

            $usuarioExistente = DB::table('usuarios', 'u')
            ->select('u.email')            
            ->where('u.email', '=', $request->email)
            ->get();

            if(!empty($usuarioExistente[0]))
            {
                throw ValidationException::withMessages(['email' => 'E-mail já cadastrado']);
            }
            
            $usuario->codigo_verificacao = sha1(time());
            $usuario->email_verificado = 0;            
            
            //Envia e-mail de confirmação
            EmailController::enviaEmailAlteracaoEmail($usuario->nome, $request->email, $usuario->codigo_verificacao);

            session()->flash('msg_sucesso', 'Dados alterados com sucesso! Um e-mail de confirmação foi enviado ao seu novo e-mail');
        }
        else
        {
            session()->flash('msg_sucesso', 'Dados alterados com sucesso!');
        }

        $usuario->fill($request->all());
        $usuario->save();

        //Refazendo o login para não deslogar o usuário
        Auth::user()->email = $request->email;

        if(Auth::attempt(['email' => $usuario->email, 'password' => $usuario->password]))
        {
            if($usuario->id_perfil == 1)
            {
                return redirect()->route('minha_conta');
            } 

            return redirect()->route('minha_conta_farmacia');            
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        } 
        return redirect()->route('index_farmacia');
    }

    public function alteracaoSenha()
    {
        $usuario['usuario'] = Auth::user();

        if(empty($usuario['usuario']))
        { 
            if(str_contains(url()->previous(), 'farmacia'))
            {
                return redirect()->route('login_farmacia');
            }                      
            return redirect()->route('login');
        }

        if($usuario['usuario']->id_perfil == 1)
        {
            return view('cliente/alteracaoSenha', $usuario);
        }

        return view('farmacia/alteracaoSenha', $usuario);
        
    }

    public function confirmaAlteracaoSenha(Request $request)
    {        
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login');
        }

        $request->validate([
            'new_password' => 'required|confirmed'
        ]);

        if(Hash::check($request->password, $usuario->password))//if(Hash::check())
        {
            $usuario->password = Hash::make($request->new_password);
            $usuario->save();
            session()->flash('msg_sucesso', 'Senha alterada com sucesso!');
        }
        else
        {
            throw ValidationException::withMessages(['password' => 'Senha incorreta']);
        }
        
        //Refazendo o login para não deslogar o usuário
        if(Auth::attempt(['email' => $usuario->email, 'password' => $usuario->password]))
        {
            if($usuario->id_perfil == 1)
            {
                return redirect()->route('minha_conta');
            } 

            return redirect()->route('minha_conta_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        } 
        return redirect()->route('index_farmacia');
    }

    public function sair()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('index');
        }

        //Cliente
        if($usuario->id_perfil == 1)
        {
            Auth::logout();
            return redirect()->route('index');
        }
        
        //Farmácia
        $farmacia = Farmacia::where('id_usuario', $usuario->id)
            ->get()[0];

        if($farmacia->aberto == 1)
        {
            $usuarioService = new UsuarioService();
            $retorno = $usuarioService->abreFechaFarmacia($farmacia);            

            if($retorno['sucesso'] == 0)
            {
                session()->flash('msg_erro', $retorno['mensagem']);
                return redirect()->back();
            }
        }

        //Sessão usada para pegar os dados da farmácia
        session()->forget('farmacia');
       
        Auth::logout();
        return redirect()->route('index_farmacia');
    }
}