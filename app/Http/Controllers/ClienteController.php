<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Usuario;
use App\Models\Endereco;
use App\Services\UsuarioService;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\EmailController;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;

class ClienteController extends Controller
{
    public function cadastro()
    {
        return view('cliente/cadastro');
    }

    public function cadastraEndereco(Request $request)
    {
        //ddd($request->descricao);
        //$sessao['usuario'] = session('user', []);
        $sessao['endereco'] = session('end', []);

        $usuario = Auth::user();

        //Se não tiver nenhum usuário logado na sessão, salva o enderço na sessão
        if (empty($usuario))
        {   
            $endereco = new DB();

            $endereco->descricao = $request->descricao;
            $endereco->logradouro = $request->logradouro;
            $endereco->numero = $request->numero;
            $endereco->complemento = $request->complemento;
            $endereco->bairro = $request->bairro;
            $endereco->cidade = $request->cidade;
            $endereco->estado = $request->estado;
            $endereco->latitude = $request->latitude;
            $endereco->longitude = $request->longitude;

            $sessao['endereco'] = $endereco;

            session(['end' => $sessao['endereco']]);

            return redirect()->route('index');
        
        }
        else
        {   
            //Alteração de endereço
            if(isset($request->id_endereco))      
            {
                $endereco = Endereco::where(['id' => $request->id_endereco, 'id_usuario' => $usuario->id])->first();

                if(empty($endereco))
                {
                    session()->flash('msg_erro', 'Endereço inválido');
                    return redirect()->back();
                }

                $endereco->fill($request->all());
                //ddd($endereco);
                $endereco->id_usuario = $usuario->id;
                $endereco->save();

                session()->flash('msg_sucesso', 'Endereço alterado com sucesso!');
                return redirect()->route('minha_conta');
            }  
            //Cadastro de endereço
            else
            {
                $endereco = new Endereco();

                $endereco->descricao = $request->descricao;                
                $endereco->logradouro = $request->logradouro;
                $endereco->numero = $request->numero;
                $endereco->complemento = $request->complemento;
                $endereco->bairro = $request->bairro;
                $endereco->cep = $request->cep;
                $endereco->cidade = $request->cidade;
                $endereco->estado = $request->estado;
                $endereco->latitude = $request->latitude;
                $endereco->longitude = $request->longitude;
                $endereco->id_usuario = $usuario->id;
                $endereco->em_uso = 0;
                $endereco->save();

                //Muda endereço cadastrado pra em uso e retida em uso dos outros endereços
                $salvaEnderecos = new UsuarioService();
                $retorno = $salvaEnderecos->alteraEnderecoUso($endereco);

                if($retorno['sucesso'] == 1)
                {
                    session()->flash('msg_sucesso', 'Endereço cadastrado com sucesso!');
                    return redirect()->route('minha_conta');
                }
                else
                {
                    session()->flash('msg_erro', 'Erro no cadastro do endereço. Tente novamente mais tarde');
                    return redirect()->route('minha_conta');
                }

            }
        }
        
    }



    public function finalizaCadastro(Request $request)
    {     
        $request->validate([
            'nome' => 'required',
            'email' => 'required',
            'cpf' => 'required',
            'password' => 'required',
            'data_nascimento' => 'required',
            'celular' => 'required',
            'genero' => 'required',
            'descricao' => 'required',            
            'numero'  => 'required',
        ]);   

        if(!$this->validaCPF($request->cpf))
        {
            throw ValidationException::withMessages(['cpf' => 'CPF inválido']);
        }  

        if(empty($request->latitude))
        {
            throw ValidationException::withMessages(['latitude' => 'Verifique seu endereço']);
        }

        if(empty($request->longitude))
        {
            throw ValidationException::withMessages(['longitude' => 'Verifique seu endereço']);
        }

        $usuario = new Usuario();
        $usuario->fill($request->all());
        $usuario->id_perfil = 1; //Cliente

        //ddd($usuario);
        $endereco = new Endereco();
        $endereco->fill($request->all());
        $endereco->em_uso = 1;        

        //Tirando máscara do CPF e telefone/celular
        $usuario->cpf = str_replace('.', '', str_replace('-', '', $usuario->cpf));

        //Criptografando as senhas com hash do próprio laravel e inserindo um hash para verificação do e-mail
        $password = $request->input("password", "");
        $usuario->password = Hash::make($password);
        $usuario->codigo_verificacao = sha1(time()); 

        //Salva o usuário no banco de dados, caso os dados únicos não existam no banco
        $salvaUsuario = new UsuarioService();
        $mensagem = $salvaUsuario->cadastraCliente($usuario, $endereco);

        if($mensagem['sucesso'] == 0)
        {
            session()->flash('msg_erro', $mensagem['mensagem']);
            return redirect()->back();
        }
        else
        {
            session()->flash('msg_sucesso', $mensagem['mensagem']);
            return redirect()->route('tela_sucesso');
        }       
    }



    public function minhaConta()
    {
        $usuario['usuario'] = Auth::user();

        if(empty($usuario['usuario']))
        {            
            return redirect()->route('login');
        }

        $usuario['endereco'] = DB::table('enderecos')            
            ->where('id_usuario', '=', $usuario['usuario']->id)
            ->get();

        //ddd($usuario['endereco']);

        return view('cliente/minhaConta', $usuario);
    }



    public function alteraEnderecoEmUso(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login');
        }

        $endereco = Endereco::where(['id' => $request->id_endereco, 'id_usuario' => $usuario->id, 'em_uso' => 0])->first();

        if(empty($endereco))
        {
            session()->flash('msg_erro', 'Endereço inválido');
            return redirect()->back();
        }

        $salvaEnderecos = new UsuarioService();
        $mensagem = $salvaEnderecos->alteraEnderecoUso($endereco);

        if($mensagem['sucesso'] == 0)
        {
            session()->flash('msg_erro', $mensagem['mensagem']);
            return redirect()->back();
        }
        else
        {
            session()->flash('msg_sucesso', $mensagem['mensagem']);
            return redirect()->back();
        }
    }


    public function getEndereco(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login');
        }

        $end['endereco'] = Endereco::where(['id' => $request->id_endereco, 'id_usuario' => $usuario->id])->first();

        if(empty($end['endereco']))
        {
            session()->flash('msg_erro', 'Endereço inválido');
            return redirect()->back();
        }

        //ddd($end['endereco']);

        return view('cliente/cadastroEndereco', $end);
    }
    

    


    public function validaCPF($cpf) {
 
        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
         
        // Valida tamanho da string
        if (strlen($cpf) != 11) {
            return false;
        }
    
        // Verifica se todos os digitos são iguais
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
    
        // Valida cpf
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    
    }
}
