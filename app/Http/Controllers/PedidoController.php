<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\SessaoService;
use App\Services\PedidoService;
use App\Models\Farmacia;
use App\Models\Usuario;
use App\Models\Pedido;
use Auth;

class PedidoController extends Controller
{
    public function meusPedidos()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login');
        }

        //Cliente
        if($usuario->id_perfil == 1)
        {
            $pedidos['pedido'] = DB::query()
            ->from('pedidos', 'p')
            ->select('p.id as id_pedido', 'p.created_at as data_pedido', 'p.valor_total', 'p.valor_cancelado', 'farmacias.nome as farmacia', 'p.id_status', 'statuses.status', 'statuses.classe_estilo', 'forma_pagamentos.forma_pagamento',
                     'enderecos.descricao', 'enderecos.logradouro', 'enderecos.numero', 'enderecos.complemento', 'enderecos.bairro', 'enderecos.cidade', 'enderecos.estado', 
                     DB::raw('case when (p.id_status = 1 and timestampadd(MINUTE,10,p.created_at) < now())
                                                                or (p.id_status in(2, 3, 4) and timestampadd(HOUR,2,p.updated_at) < now()) then 1
                                                            else 0 
                                                            end as tempo_confirmacao_esgotado')) //Verificando se o pedido está aguardando confirmação após 10 min ou se ainda não saiu pra entrega/foi entregue após 2h da última alteração
            ->join('farmacias', 'farmacias.id', '=', 'p.id_farmacia')
            ->join('statuses', 'statuses.id', '=', 'p.id_status')
            ->join('forma_pagamentos', 'forma_pagamentos.id', '=', 'p.id_forma_pagamento')
            ->join('enderecos', 'enderecos.id', '=', 'p.id_endereco')
            ->where('p.id_usuario', '=', $usuario->id)
            ->orderBy('p.created_at', 'desc')
            ->paginate(10);
            //->get();

            return view('cliente/meusPedidos', $pedidos);
        }
        
        //Farmácia        
        $farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        $pedidos['pedido'] = DB::query()
            ->from('pedidos', 'p')
            ->select('p.id as id_pedido', 'p.created_at as data_pedido', 'p.valor_total', 'p.valor_cancelado', 'usuarios.nome as cliente', 'p.id_status', 'statuses.status', 'statuses.classe_estilo', 'forma_pagamentos.forma_pagamento',
                     'enderecos.logradouro', 'enderecos.numero', 'enderecos.complemento', 'enderecos.bairro', 'enderecos.cidade', 'enderecos.estado', 
                     DB::raw('case when (p.id_status = 1 and timestampadd(MINUTE,10,p.created_at) < now())
                                                                or (p.id_status in(2, 3, 4) and timestampadd(HOUR,2,p.updated_at) < now()) then 1
                                                            else 0 
                                                            end as tempo_confirmacao_esgotado')) //Verificando se o pedido está aguardando confirmação após 10 min ou se ainda não saiu pra entrega/foi entregue após 2h da última alteração
            ->join('usuarios', 'usuarios.id', '=', 'p.id_usuario')
            ->join('statuses', 'statuses.id', '=', 'p.id_status')
            ->join('forma_pagamentos', 'forma_pagamentos.id', '=', 'p.id_forma_pagamento')
            ->join('enderecos', 'enderecos.id', '=', 'p.id_endereco')
            ->where('p.id_farmacia', '=', $farmacia->id)
            ->orderBy('p.created_at', 'desc')
            ->paginate(10);
            //->get();

        return view('farmacia/meusPedidos', $pedidos);
      

    }


    public function detalhePedido(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login');
        }

        //Cliente
        if($usuario->id_perfil == 1)
        {
            $pedido['pedido'] = DB::query()
            ->from('pedidos', 'p')
            ->select('p.id as id_pedido', 'p.created_at as data_pedido', 'p.valor_total', 'p.valor_cancelado', 'p.valor_entrega', 'farmacias.nome as farmacia', 'p.id_status', 'statuses.status', 'statuses.classe_estilo', 
                    '.forma_pagamento', 'enderecos.descricao', 'enderecos.logradouro', 'enderecos.numero', 'enderecos.complemento', 'enderecos.bairro', 'enderecos.cidade', 'enderecos.estado', 
                    DB::raw('case when (p.id_status = 1 and timestampadd(MINUTE,10,p.created_at) < now())
                                                                or (p.id_status in(2, 3, 4) and timestampadd(HOUR,2,p.updated_at) < now()) then 1
                                                            else 0 
                                                            end as tempo_confirmacao_esgotado')) //Verificando se o pedido está aguardando confirmação após 10 min ou se ainda não saiu pra entrega/foi entregue após 2h da última alteração
            ->join('farmacias', 'farmacias.id', '=', 'p.id_farmacia')
            ->join('statuses', 'statuses.id', '=', 'p.id_status')
            ->join('forma_pagamentos', 'forma_pagamentos.id', '=', 'p.id_forma_pagamento')
            ->join('enderecos', 'enderecos.id', '=', 'p.id_endereco')
            ->where('p.id_usuario', '=', $usuario->id)
            ->where('p.id', '=', $request->id_pedido)
            ->orderBy('p.created_at', 'desc')
            ->get();

            if(empty($pedido['pedido'][0]))
            {
                session()->flash('msg_erro', 'Pedido inválido');
                return redirect()->back();
            }

            $pedido['produto'] = DB::query()
            ->from('pedido_produtos', 'pp')
            ->select('pp.id_produto', 'pp.valor', 'pp.quantidade', 'pp.confirmado', 'produtos.produto', 'produtos.slug', 'produtos.imagens')
            ->join('produtos', 'produtos.id', '=', 'pp.id_produto')
            ->where('pp.id_pedido', '=', $request->id_pedido)
            ->get();

            return view('cliente/detalhePedido', $pedido);
        }


        //Farmácia
        $farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];
        
        
        $pedido['pedido'] = DB::query()
            ->from('pedidos', 'p')
            ->select('p.id as id_pedido', 'p.created_at as data_pedido', 'p.valor_total', 'p.valor_cancelado', 'p.valor_entrega', 'usuarios.nome as cliente', 'p.id_status', 'statuses.status', 'statuses.classe_estilo', 
                    '.forma_pagamento', 'enderecos.logradouro', 'enderecos.numero', 'enderecos.complemento', 'enderecos.bairro', 'enderecos.cidade', 'enderecos.estado', 
                    DB::raw('case when (p.id_status = 1 and timestampadd(MINUTE,10,p.created_at) < now())
                                                                or (p.id_status in(2, 3, 4) and timestampadd(HOUR,2,p.updated_at) < now()) then 1
                                                            else 0 
                                                            end as tempo_confirmacao_esgotado')) //Verificando se o pedido está aguardando confirmação após 10 min ou se ainda não saiu pra entrega/foi entregue após 2h da última alteração
            ->join('usuarios', 'usuarios.id', '=', 'p.id_usuario')
            ->join('statuses', 'statuses.id', '=', 'p.id_status')
            ->join('forma_pagamentos', 'forma_pagamentos.id', '=', 'p.id_forma_pagamento')
            ->join('enderecos', 'enderecos.id', '=', 'p.id_endereco')
            ->where('p.id_farmacia', '=', $farmacia->id)
            ->where('p.id', '=', $request->id_pedido)
            ->orderBy('p.created_at', 'desc')
            ->get();

        if(empty($pedido['pedido'][0]))
        {
            session()->flash('msg_erro', 'Pedido inválido');
            return redirect()->back();
        }

        $pedido['produto'] = DB::query()
        ->from('pedido_produtos', 'pp')
        ->select('pp.id_produto', 'pp.valor', 'pp.quantidade', 'pp.confirmado', 'produtos.produto', 'produtos.slug', 'produtos.imagens')
        ->join('produtos', 'produtos.id', '=', 'pp.id_produto')
        ->where('pp.id_pedido', '=', $request->id_pedido)
        ->get();

        return view('farmacia/detalhePedido', $pedido);
    }


    public function cancelaPedido(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login');
        }

        //Cliente
        if($usuario->id_perfil == 1)
        {
            $pedido = DB::query()
            ->from('pedidos', 'p')
            ->select('p.id', 'p.id_farmacia', DB::raw('case when (p.id_status = 1 and timestampadd(MINUTE,10,p.created_at) < now())
                                                                or (p.id_status in(2, 3, 4) and timestampadd(HOUR,2,p.updated_at) < now()) then 1
                                                            else 0 
                                                            end as tempo_confirmacao_esgotado')) //Verificando se o pedido está aguardando confirmação após 10 min ou se ainda não saiu pra entrega/foi entregue após 2h da última alteração
            ->where('p.id_usuario', '=', $usuario->id)
            ->where('p.id', '=', $request->id_pedido)
            ->get();

            if(empty($pedido[0]) || $pedido[0]->tempo_confirmacao_esgotado == 0)
            {
                session()->flash('msg_erro', 'Não é possível cancelar esse pedido');
                return redirect()->back();
            }

            $farmacia = Farmacia::where('id', $pedido[0]->id_farmacia)->get()[0];
            
            $pedidoService = new PedidoService();
            $cancelamento = $pedidoService->cancelaPedido($usuario, $farmacia, $pedido[0]->id, 1);

            
        }
        //Farmácia
        else
        {            
            $farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

            $pedido = DB::table('pedidos')
                ->where('id_farmacia', '=', $farmacia->id)
                ->where('id', '=', $request->id_pedido)
                ->whereIn('id_status', [1, 2, 3, 4]) //Aguardando confirmação, confirmado, parcialmente confirmado, em entrega - Status que podem ser mudados pra cancelado
                ->get();

            if(empty($pedido[0]))
            {
                session()->flash('msg_erro', 'Não é possível cancelar esse pedido');
                return redirect()->back();
            }

            $cliente = Usuario::where('id', $pedido[0]->id_usuario)->get()[0];
            
            $pedidoService = new PedidoService();
            $cancelamento = $pedidoService->cancelaPedido($cliente, $farmacia, $pedido[0]->id, 2);
        }
        
        if($cancelamento['sucesso'] == 0)
        {
            session()->flash('msg_erro', $cancelamento['mensagem']);
        }
        else
        {
            session()->flash('msg_sucesso', $cancelamento['mensagem']);
        }

        return redirect()->back();
    }


    public function cancelaProdutoPedido(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {            
            return redirect()->route('index');
        }
                   
        $farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];        

        //Verificando se o pedido pertence a farmácia
        $pedido = Pedido::where('id_farmacia', $farmacia->id)
            ->where('id', $request->id_pedido)
            ->where('id_status', 1) //Aguardando confirmação - único status que pode ter status de produtos alterado para cancelado. Os outros podem apenas cancelar o pedido inteiro
            ->get();

        if(empty($pedido[0]))
        {
            session()->flash('msg_erro', 'Não é possível cancelar esse produto');
            return redirect()->back();
        }        

        //Verificando se o produto pertence ao pedido
        $pedidoProduto = DB::query()
            ->from('pedido_produtos', 'pp')
            ->select('pp.id')
            ->join('produtos', 'produtos.id', '=', 'pp.id_produto')
            ->where('pp.id_pedido', '=', $pedido[0]->id)
            ->where('produtos.slug', '=', $request->slug)
            ->whereNull('pp.confirmado')
            ->get();

        if(empty($pedidoProduto[0]))
        {
            session()->flash('msg_erro', 'Não é possível cancelar esse produto');
            return redirect()->back();
        }
        
        $pedidoService = new PedidoService();
        $cancelamento = $pedidoService->cancelaProdutoPedido($pedido[0], $pedidoProduto[0]->id);

        
        if($cancelamento['sucesso'] == 0)
        {
            session()->flash('msg_erro', $cancelamento['mensagem']);
        }
        else
        {
            session()->flash('msg_sucesso', $cancelamento['mensagem']);
        }

        return redirect()->back();
    }

    




    public function confirmaPedido(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        
        $farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        $pedido = Pedido::where('id_farmacia', '=', $farmacia->id)            
            ->where('id', $request->id_pedido)
            ->where('id_status', 1) //Aguardando confirmação - Único status que pode ser alterado para confirmado/parcialmente confirmado
            ->get();

        if(empty($pedido[0]))
        {
            session()->flash('msg_erro', 'Não é possível confirmar esse pedido');
            return redirect()->back();
        }
        
        $pedidoService = new PedidoService();
        $confirmacao = $pedidoService->confirmaPedido($pedido[0]);
        
        
        if($confirmacao['sucesso'] == 0)
        {
            session()->flash('msg_erro', $confirmacao['mensagem']);
        }
        else
        {
            session()->flash('msg_sucesso', $confirmacao['mensagem']);
        }

        return redirect()->back();
    }




    public function confirmaProdutoPedido(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {            
            return redirect()->route('index');
        }
                   
        $farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];        

        //Verificando se o pedido pertence a farmácia
        $pedido = Pedido::where('id_farmacia', $farmacia->id)
            ->where('id', $request->id_pedido)
            ->where('id_status', 1) //Aguardando confirmação - único status que pode ter status de produtos alterados para confirmado.
            ->get();

        if(empty($pedido[0]))
        {
            session()->flash('msg_erro', 'Não é possível confirmar esse produto');
            return redirect()->back();
        }        

        //Verificando se o produto pertence ao pedido
        $pedidoProduto = DB::query()
            ->from('pedido_produtos', 'pp')
            ->select('pp.id')
            ->join('produtos', 'produtos.id', '=', 'pp.id_produto')
            ->where('pp.id_pedido', '=', $pedido[0]->id)
            ->where('produtos.slug', '=', $request->slug)
            ->whereNull('pp.confirmado')
            ->get();

        if(empty($pedidoProduto[0]))
        {
            session()->flash('msg_erro', 'Não é possível confirmar esse produto');
            return redirect()->back();
        }
        
        $pedidoService = new PedidoService();
        $confirmacao = $pedidoService->confirmaProdutoPedido($pedido[0], $pedidoProduto[0]->id);

        
        if($confirmacao['sucesso'] == 0)
        {
            session()->flash('msg_erro', $confirmacao['mensagem']);
        }
        else
        {
            session()->flash('msg_sucesso', $confirmacao['mensagem']);
        }

        return redirect()->back();
    }


    public function entregaPedido(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login_farmacia');
        }
        
        $farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

        $pedido = Pedido::where('id_farmacia', '=', $farmacia->id)            
        ->where('id', $request->id_pedido)
        ->whereIn('id_status', [2, 3]) //Confirmado, parcialmente confirmado - Status que pode ser alterados para em entrega
        ->get();                 

        if(empty($pedido[0]))
        {
            session()->flash('msg_erro', 'Não é possível colocar esse pedido em entrega');
            return redirect()->back();
        }
        
        $pedido[0]->id_status = 4;
        $pedido[0]->save();    
        
        session()->flash('msg_sucesso', 'Pedido alterado com sucesso!');
        

        return redirect()->back();
    }


    public function concluiPedido(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {            
            return redirect()->route('login_farmacia');
        }

        //Cliente
        if($usuario->id_perfil == 1)
        {  
            $pedido = Pedido::where('id_usuario', '=', $usuario->id)            
            ->where('id', $request->id_pedido)
            ->where('id_status', 4) //Em entrega - Único status que pode ser alterado para concluído/parcialmente concluído
            ->get();            
        }
        //Farmácia
        else
        {
            $farmacia = Farmacia::where('id_usuario', $usuario->id)->get()[0];

            $pedido = Pedido::where('id_farmacia', '=', $farmacia->id)            
            ->where('id', $request->id_pedido)
            ->where('id_status', 4) //Em entrega - Único status que pode ser alterado para concluído/parcialmente concluído
            ->get(); 
        }        

        if(empty($pedido[0]))
        {
            session()->flash('msg_erro', 'Não é possível concluir esse pedido');
            return redirect()->back();
        }

        $pedidoService = new PedidoService();

        //Cliente
        if(empty($farmacia))
        {
            $farmacia = Farmacia::where('id', $pedido[0]->id_farmacia)->get()[0];
            $conclusao = $pedidoService->concluiPedido($usuario, $farmacia, $pedido[0]);
        }
        //Farmácia
        else
        {
            $cliente = Usuario::where('id', $pedido[0]->id_usuario)->get()[0];
            $conclusao = $pedidoService->concluiPedido($cliente, $farmacia, $pedido[0]);
        }
        
        
        
        
        
        if($conclusao['sucesso'] == 0)
        {
            session()->flash('msg_erro', $conclusao['mensagem']);
        }
        else
        {
            session()->flash('msg_sucesso', $conclusao['mensagem']);
        }

        return redirect()->back();
    }
}
