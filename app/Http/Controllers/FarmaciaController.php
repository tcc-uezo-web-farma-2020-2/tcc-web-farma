<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Farmacia;
use App\Models\Endereco;
use App\Models\ValorEntrega;
use App\Models\ProdutoFarmacia;
use App\Services\PedidoService;
use App\Services\UsuarioService;
use Illuminate\Validation\ValidationException;
use Auth;

class FarmaciaController extends Controller
{
    public function index(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $farmacia['andamento'] = $this->pedidosAndamento();       
        $farmacia['concluido'] = $this->pedidosConcluidosPorPeriodo();

        $farmaciaUsuario = $this->getFarmaciaAuth();
        $farmacia['aberto'] = $farmaciaUsuario->aberto;

        $farmacia['poucoEstoque'] = $this->poucoEstoque();
        $farmacia['maisVendidos'] = $this->maisVendidos();

        //ddd($farmacia);

        return view('farmacia/index', $farmacia);
    }





    public function cadastro(Request $request)
    {
        return view('farmacia/cadastro');
    }




    public function finalizaCadastro(Request $request)
    {   
        $request->validate([
            'nome' => 'required',
            'cpf' => 'required',
            'email' => 'required',
            'password' => 'required',
            'data_nascimento' => 'required',
            'celular' => 'required',
            'genero' => 'required',
            'descricao' => 'required',
            'numero'  => 'required',
            'nome_farmacia' => 'required',
            'cnpj' => 'required',
            'raio_entrega' => 'required',
            'cep' => 'required',
            'numero' => 'required',
        ]);

        if(!$this->validaCPF($request->cpf))
        {
            throw ValidationException::withMessages(['cpf' => 'CPF inválido']);
        }   

        if(!$this->validaCPF($request->cnpj))
        {
            throw ValidationException::withMessages(['cpf' => 'CPF inválido']);
        }

        if(empty($request->latitude))
        {
            throw ValidationException::withMessages(['latitude' => 'Verifique seu endereço']);
        }

        if(empty($request->longitude))
        {
            throw ValidationException::withMessages(['longitude' => 'Verifique seu endereço']);
        }

        $usuario = new Usuario();
        $usuario->fill($request->all());
        $usuario->id_perfil = 2; //Administrador da farmácia

        //ddd($usuario);
        $farmacia = new Farmacia();
        $farmacia->fill($request->all());
        $farmacia->aberto = 0;

        //Tirando máscara do CPF
        $usuario->cpf = str_replace('.', '', str_replace('-', '', $usuario->cpf));
        
        $entregas1 = new ValorEntrega();
        $entregas2 = new ValorEntrega();
        $entregas3 = new ValorEntrega();
        
        //Farmácia preencheu valores de entrega
        if($request->entrega_de_1 != null && $request->entrega_ate_1 != null && $request->entrega_valor_1 != null)
        {
            $entregas1->raio_km_de = $request->entrega_de_1;
            $entregas1->raio_km_ate = $request->entrega_ate_1;
            $entregas1->valor = $request->entrega_valor_1;

            if($request->entrega_de_2 != null && $request->entrega_ate_2 != null && $request->entrega_valor_2 != null)
            {
                $entregas2->raio_km_de = $request->entrega_de_2;
                $entregas2->raio_km_ate = $request->entrega_ate_2;
                $entregas2->valor = $request->entrega_valor_2;
            }
            if($request->entrega_de_3 != null && $request->entrega_ate_3 != null && $request->entrega_valor_3 != null)
            {
                $entregas3->raio_km_de = $request->entrega_de_3;
                $entregas3->raio_km_ate = $request->entrega_ate_3;
                $entregas3->valor = $request->entrega_valor_3;
            }
        }
        //Usuário não preencheu valores de entrega (entregas serão grátis)
        else
        {
            $entregas1->raio_km_de = 0;
            $entregas1->raio_km_ate = $request->raio_entrega;
            $entregas1->valor = 0;
        }

        //Criptografando as senhas com hash do próprio laravel e inserindo um hash para verificação do e-mail
        $password = $request->input("password", "");
        $usuario->password = Hash::make($password);
        $usuario->codigo_verificacao = sha1(time()); 

        //Salva o usuário no banco de dados, caso os dados únicos não existam no banco
        $salvaUsuario = new UsuarioService();
        $mensagem = $salvaUsuario->cadastraFarmacia($usuario, $farmacia, $entregas1, $entregas2, $entregas3);

        if($mensagem['sucesso'] == 0)
        {
            session()->flash('msg_erro', $mensagem['mensagem']);
            return redirect()->back();
        }
        else
        {
            session()->flash('msg_sucesso', $mensagem['mensagem']);
            return redirect()->route('tela_sucesso');
        }       
    }




    public function abreFecha()
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $farmaciaUsuario = $this->getFarmaciaAuth();
        
        $usuarioService = new UsuarioService();
        $retorno = $usuarioService->abreFechaFarmacia($farmaciaUsuario);

        if($retorno['sucesso'] == 0)
        {         
            session()->flash('msg_erro', $retorno['mensagem']);
            return redirect()->back();
        }

        session()->flash('msg_sucesso', $retorno['mensagem']);
        return redirect()->route('index_farmacia');
    }


    public function minhaConta()
    {
        $usuario['usuario'] = Auth::user();

        if(empty($usuario['usuario']))
        {            
            return redirect()->route('login_farmacia');
        }

        if($usuario['usuario']->id_perfil == 1)
        {            
            return redirect()->route('index');
        }

        $usuario['farmacia'] = $this->getFarmaciaAuth();

        $usuario['entregas'] = DB::table('valor_entregas')            
            ->where('id_farmacia', '=', $usuario['farmacia']->id)
            ->get();

        //ddd($usuario);

        return view('farmacia/minhaConta', $usuario);
    }


    public function alteraDadosAdm()
    {
        $usuario['usuario'] = Auth::user();

        if(empty($usuario['usuario']))
        {            
            return redirect()->route('login_farmacia');
        }

        if($usuario['usuario']->id_perfil == 1)
        {            
            return redirect()->route('index');
        }

        return view('farmacia/alteracaoContaAdm', $usuario);
    }


    public function alteraDadosFarmacia()
    {
        $usuario['usuario'] = Auth::user();

        if(empty($usuario['usuario']))
        {            
            return redirect()->route('login_farmacia');
        }

        if($usuario['usuario']->id_perfil == 1)
        {            
            return redirect()->route('index');
        }

        $usuario['farmacia'] = $this->getFarmaciaAuth();

        $usuario['entregas'] = DB::table('valor_entregas')            
            ->where('id_farmacia', '=', $usuario['farmacia']->id)
            ->get();

        return view('farmacia/alteracaoContaFarmacia', $usuario);
    }




    public function alteraCadastroFarmacia(Request $request)
    {
        //ddd($request);
        $usuario = Auth::user();

        if(empty($usuario))
        {             
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {            
            return redirect()->route('index');
        }

        $farmacia = $this->getFarmaciaAuth();        

        if($farmacia->cnpj != $request->cnpj)
        {
            session()->flash('msg_erro', 'Não é permitido alterar o CNPJ');
            return redirect()->back();
        }

        DB::table('valor_entregas')
            ->where('id_farmacia', '=', $farmacia->id)->delete();

        $farmacia->fill($request->all());
        $farmacia->save();

        $entregas1 = new ValorEntrega();
        $entregas2 = new ValorEntrega();
        $entregas3 = new ValorEntrega();

        if($request->entrega_de_1 != null && $request->entrega_ate_1 != null && $request->entrega_valor_1 != null)
        {            
            $entregas1->id_farmacia = $farmacia->id;
            $entregas1->raio_km_de = $request->entrega_de_1;
            $entregas1->raio_km_ate = $request->entrega_ate_1;
            $entregas1->valor = $request->entrega_valor_1;
            $entregas1->save();

            if($request->entrega_de_2 != null && $request->entrega_ate_2 != null && $request->entrega_valor_2 != null)
            {               
                $entregas2->id_farmacia = $farmacia->id;
                $entregas2->raio_km_de = $request->entrega_de_2;
                $entregas2->raio_km_ate = $request->entrega_ate_2;
                $entregas2->valor = $request->entrega_valor_2;
                $entregas2->save();
            }
            if($request->entrega_de_3 != null && $request->entrega_ate_3 != null && $request->entrega_valor_3 != null)
            {
                $entregas3->id_farmacia = $farmacia->id;
                $entregas3->raio_km_de = $request->entrega_de_3;
                $entregas3->raio_km_ate = $request->entrega_ate_3;
                $entregas3->valor = $request->entrega_valor_3;
                $entregas3->save();
            }
        }
        //Usuário não preencheu valores de entrega (entregas serão grátis)
        else
        {            
            $entregas1->id_farmacia = $farmacia->id;
            $entregas1->raio_km_de = 0;
            $entregas1->raio_km_ate = $request->raio_entrega;
            $entregas1->valor = 0;
            $entregas1->save();
        }      

        session()->flash('msg_sucesso', 'Dados alterados com sucesso!');
        return redirect()->route('minha_conta_farmacia');            
       
    }


    public function estoque()
    {
        return view('farmacia/estoque');
    }


    public function relatorios()
    {
        return redirect()->route('index_farmacia');
    }





    public function getProduto(Request $request)
    {
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        $farmaciaUsuario = $this->getFarmaciaAuth();

        //ddd($request);
        /*$produtosFarmacia = DB::table('produtos', 'p')
            ->select('produto_farmacias.id', 'p.id as id_produto', 'p.produto', 'p.variacao', 'p.quantidade', 'p.composicao', 'p.imagens', 'produto_farmacias.estoque', 'produto_farmacias.valor', 'categorias.categoria')
            ->join('categorias', 'categorias.id', '=', 'p.id_categoria')
            ->leftJoin('produto_farmacias', 'produto_farmacias.id_produto', '=', 'p.id')            
            ->where('p.cod_barras', '=',  $request->codbarras)
            ->where('produto_farmacias.id_farmacia', '=', $farmaciaUsuario->id)
            ->get();*/       
        

        $produdoExiste = DB::query()
            ->from('produto_farmacias', 'pf')
            ->select('pf.id', 'pf.estoque', 'pf.valor', 'pf.id_produto')
            ->join('produtos', 'produtos.id', '=', 'pf.id_produto')
            ->where('produtos.cod_barras', '=',  $request->codbarras)
            ->where('pf.id_farmacia', '=', $farmaciaUsuario->id);

        $produtosFarmacia = DB::query()
            ->from('produtos', 'p')
            ->select('pf.id', 'p.id as id_produto', 'p.produto', 'p.variacao', 'p.quantidade', 'p.composicao', 'p.imagens', 'pf.estoque', 'pf.valor', 'categorias.categoria')
            ->join('categorias', 'categorias.id', '=', 'p.id_categoria')
            ->leftJoinSub($produdoExiste, 'pf', function ($join) {
                $join->on('pf.id_produto', '=', 'p.id');
            })
            ->where('p.cod_barras', '=',  $request->codbarras)        
            ->get();

        if (empty($produtosFarmacia[0]))
        {
            $categorias = DB::table('categorias')
            ->select('id', 'categoria')  
            ->orderBy('id')     
            ->get(); 

            return response()->json([$categorias]);
        }

        //return response()->json(['teste' => 'sucesso']);
        return response()->json([$produtosFarmacia[0]]);
    }



    public function alteraEstoque(Request $request)
    {
        //Validando o usuáro logado
        $usuario = Auth::user();

        if(empty($usuario))
        {
            return redirect()->route('login_farmacia');
        }

        if($usuario->id_perfil == 1)
        {
            return redirect()->route('index');
        }

        
        $farmacia = $this->getFarmaciaAuth();
        
        //Validando se o produto existe
        $produto = DB::table('produtos')
            ->where('id', '=', $request->id_produto)
            ->get();

        if(empty($produto[0]))
        {
            session()->flash('msg_erro', 'Produto não encontrado');
            return redirect()->back();
        }     
        
        //Verificando se o produto já existe na farmácia, se não existir, cadastra
        $prodFarm = ProdutoFarmacia::where('id_farmacia', $farmacia->id)
            ->where('id_produto', $request->id_produto)
            ->get();

        if(!empty($prodFarm[0]))
        {
            $prodFarm[0]->estoque = $request->estoque;
            $prodFarm[0]->valor = floatval(number_format(floatval(str_replace(',', '.', $request->preco)), 2));
            $prodFarm[0]->save();


            session()->flash('msg_sucesso', 'Dados alterados com sucesso!');
            return redirect()->back();
        }
        else
        {
            $prodFarm = new ProdutoFarmacia();
            $prodFarm->id_farmacia = $farmacia->id;
            $prodFarm->id_produto = $produto[0]->id;
            $prodFarm->valor = floatval(number_format(floatval(str_replace(',', '.', $request->preco)), 2));
            $prodFarm->estoque = $request->estoque;
            $prodFarm->save();
            
            session()->flash('msg_sucesso', 'Produto cadastrado com sucesso!');
            return redirect()->back();
        }

        

        
        
        if(empty($prodFarm[0]))
        {
            session()->flash('msg_sucesso', 'Produto não encontrado');
            return redirect()->back();
        } 
    }




    public function pedidosAndamento()
    {
        $farmacia = $this->getFarmaciaAuth();
        $pedidos = [];

        $pedidos['totalAndamento'] = DB::query()
                ->from('pedidos')
                ->select(DB::raw('count(*) as qtd'))
                ->where('id_farmacia', '=', $farmacia->id)
                ->whereIn('id_status', [1, 2, 3, 4]) //1 - Aguardando confirmação / 2 - Confirmado / 3 - Parcialmente confirmado / 4 - Em entrega
                ->get()[0];
        
        $emAndamento = DB::query()
            ->from('pedidos')
            ->select(DB::raw('count(*) as qtd'), 'id_status')
            ->where('id_farmacia', '=', $farmacia->id)
            ->whereIn('id_status', [1, 2, 3, 4]) //1 - Aguardando confirmação / 2 - Confirmado / 3 - Parcialmente confirmado / 4 - Em entrega
            ->groupBy('id_status')
            ->orderBy('id_status')
            ->get(); 

            foreach($emAndamento as $ea)
            {
                if($ea->id_status == 1)
                {
                    $pedidos['ac'] = $ea;
                }
                if($ea->id_status == 2)
                {
                    $pedidos['co'] = $ea;
                }
                if($ea->id_status == 3)
                {
                    $pedidos['pc'] = $ea;
                }
                if($ea->id_status == 4)
                {
                    $pedidos['ee'] = $ea;
                }
            }
    
            if(empty($pedidos['ac']))
            {
                $pedidos['ac'] = new DB();
                $pedidos['ac']->qtd = 0;
            }
            if(empty($pedidos['co']))
            {
                $pedidos['co'] = new DB();
                $pedidos['co']->qtd = 0;
            }
            if(empty($pedidos['pc']))
            {
                $pedidos['pc'] = new DB();
                $pedidos['pc']->qtd = 0;
            }
            if(empty($pedidos['ee']))
            {
                $pedidos['ee'] = new DB();
                $pedidos['ee']->qtd = 0;
            }

            return $pedidos;

    }

    public function pedidosConcluidosPorPeriodo()
    {
        $farmacia = $this->getFarmaciaAuth();
        $pedidos = [];

        $pedidos['hoje'] = DB::query()
                ->from('pedidos')
                ->select(DB::raw('count(*) as qtd'))
                ->where('id_farmacia', '=', $farmacia->id)
                ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
                ->where(DB::raw('date(created_at)'), '=', 'current_date')
                ->get()[0];
        
        $pedidos['semana'] = DB::query()
            ->from('pedidos')
            ->select(DB::raw('count(*) as qtd'))
            ->where('id_farmacia', '=', $farmacia->id)
            ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
            ->where(DB::raw('date(created_at)'), '>=', DB::raw('date_add(current_date, interval -7 day)'))         
            ->get()[0]; 

        $pedidos['mes'] = DB::query()
            ->from('pedidos')
            ->select(DB::raw('count(*) as qtd'))
            ->where('id_farmacia', '=', $farmacia->id)
            ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
            ->where(DB::raw('date(created_at)'), '>=', DB::raw('date_add(current_date, interval -1 month)'))            
            ->get()[0]; 

        $pedidos['semestre'] = DB::query()
            ->from('pedidos')
            ->select(DB::raw('count(*) as qtd'))
            ->where('id_farmacia', '=', $farmacia->id)
            ->whereIn('id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
            ->where(DB::raw('date(created_at)'), '>=', DB::raw('date_add(current_date, interval -6 month)'))            
            ->get()[0];   

            return $pedidos;

    }




    public function poucoEstoque()
    {
        $farmacia = $this->getFarmaciaAuth();

        $produtos = DB::query()
                ->from('produto_farmacias', 'pf')
                ->select('pf.id_produto', 'pf.estoque', 'produtos.produto', 'produtos.cod_barras', 'produtos.imagens')
                ->join('produtos', 'produtos.id', '=', 'pf.id_produto')
                ->where('pf.id_farmacia', '=', $farmacia->id)
                ->where('pf.estoque', '>', 0)
                ->orderBy('pf.estoque')
                ->limit(10)
                ->get();   

        return $produtos;
    }



    public function maisVendidos()
    {
        $farmacia = $this->getFarmaciaAuth();

        /*$contQuery = DB::query()
            ->from('pedido_produtos', 'pp')
            ->select(DB::raw('count(distinct(pp.id_produto)) as qtd'), 'pp.id_produto')
            ->join('pedidos', 'pedidos.id', '=', 'pp.id_pedido')
            ->where('pedidos.id_farmacia', '=', $farmacia->id)
            ->whereIn('pedidos.id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
            ->groupBy('pp.id_produto')
            ->orderBy('pp.id_produto')
            ->limit(10);*/

        $contQuery = DB::query()
            ->from('pedido_produtos', 'pp')
            ->select(DB::raw('sum(pp.quantidade) as qtd'), 'pp.id_produto')
            ->join('pedidos', 'pedidos.id', '=', 'pp.id_pedido')
            ->where('pedidos.id_farmacia', '=', $farmacia->id)
            ->where('pp.confirmado', '=', 1)
            ->whereIn('pedidos.id_status', [5, 6]) //5 - Concluído / 6 - Parcialmente concluído
            ->groupBy('pp.id_produto')
            ->orderBy('pp.id_produto')
            ->limit(10);

        $produtos = DB::query()
                ->from('produtos', 'p')
                ->select('p.imagens', 'p.produto', 'p.cod_barras', 'cont.qtd')
                ->joinSub($contQuery, 'cont', function ($join) {
                    $join->on('p.id', '=', 'cont.id_produto');
                })  
                ->orderBy('cont.qtd', 'desc')              
                ->get();

        return $produtos;
    }

    public function getFarmaciaAuth()
    {
        $usuario = Auth::user();

        return Farmacia::where('id_usuario', $usuario->id)
            ->get()[0];
    }


    public function validaCPF($cpf) {
 
        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
         
        // Valida tamanho da string
        if (strlen($cpf) != 11) {
            return false;
        }
    
        // Verifica se todos os digitos são iguais
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
    
        // Valida cpf
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    
    }


    public function validaCNPJ($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        
        // Valida tamanho da string
        if (strlen($cnpj) != 14)
            return false;

        // Verifica se todos os digitos são iguais
        if (preg_match('/(\d)\1{13}/', $cnpj))
            return false;	

        // Valida cnpj
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($cnpj[12] != ($resto < 2 ? 0 : 11 - $resto))
            return false;
        
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $cnpj[13] == ($resto < 2 ? 0 : 11 - $resto);
    }
}
