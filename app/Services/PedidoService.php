<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\Pedido;
use App\Models\PedidoProduto;
use App\Models\Usuario;
use App\Models\Farmacia;
use App\Http\Controllers\EmailController;
use Log;
use Auth;

class PedidoService
{
    public function cadastraPedido(Usuario $usuario, $carrinho, $id_pagamento)
    {    

        foreach($carrinho['itens'] as $prod)
        {
            $id_farmacia = $prod[0]->id_farmacia;
            break;
        }   

        $farmacia = DB::table('farmacias')
        ->where('id', '=', $id_farmacia)
        ->get();

        $endereco =  DB::table('enderecos')
        ->where('id_usuario', '=', $usuario->id)
        ->where('em_uso', '=', 1)
        ->get();

        if($farmacia[0]->aberto == 0)
        {
            return ['sucesso' => 0, 'mensagem' => 'A farmácia está fechada. Tente novamente amanhã'];
        }

        try
        {
            $pedido = new Pedido();

            $pedido->id_usuario = $usuario->id;
            $pedido->id_endereco = $endereco[0]->id;
            $pedido->id_farmacia = $farmacia[0]->id;
            $pedido->id_forma_pagamento = $id_pagamento;
            $pedido->valor_pedido = $carrinho['subtotal'];
            $pedido->valor_desconto = 0; //Desconto entrará em desenvolvimentos futuros
            $pedido->valor_cancelado = 0;
            $pedido->valor_entrega = $carrinho['totalEntrega'];
            $pedido->valor_total = $carrinho['subtotal'] + $carrinho['totalEntrega'];

            DB::beginTransaction(); //Não vai salvar o pedido sem salvar os itens do pedido
            $pedido->save(); //salvando o pedido no banco

            foreach($carrinho['itens'] as $prod)
            {           
                $pedidoProduto = new PedidoProduto();     
                $pedidoProduto->id_pedido = $pedido->id;
                $pedidoProduto->id_produto = $prod[0]->id_produto;
                $pedidoProduto->valor = $prod[0]->valor;
                $pedidoProduto->valor_desconto = 0; //Desconto entrará em desenvolvimentos futuros
                $pedidoProduto->quantidade = $prod[0]->quantidade;
                $pedidoProduto->confirmado = null;
                $pedidoProduto->save();

                //Atualizando a quantidade do produto na farmácia
                DB::table('produto_farmacias')       
                ->where('id', '=', $prod[0]->id_produto_farmacia)            
                ->update(['estoque' => DB::raw('estoque - ' . $prod[0]->quantidade)]);
            }     

            $admFarmacia = DB::table('usuarios')
            ->where('id', '=', $farmacia[0]->id_usuario)
            ->get();

            EmailController::enviaEmailConfirmacaoPedidoCliente($usuario->nome, $usuario->email, $pedido->id);
            EmailController::enviaEmailConfirmacaoPedidoFarmacia($farmacia[0]->nome, $admFarmacia[0]->email, $pedido->id);

            DB::commit();

            return ['sucesso' => 1, 'mensagem' => 'Pedido realizado com sucesso!'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/PedidoService', 'mensagem' => $e->gerMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao realizar pedido. Tente novamente mais tarde'];
        }
    }

    public function cancelaPedido(Usuario $cliente, Farmacia $farmacia, $id_pedido, $id_perfil)
    {    
        
        $pedido = Pedido::where('id', $id_pedido)->get();
        //ddd($pedido[0]);

        try
        {

            $pedido[0]->id_status = 7;
            $pedido[0]->valor_cancelado = $pedido[0]->valor_pedido + $pedido[0]->valor_entrega - $pedido[0]->valor_desconto;

            DB::beginTransaction(); //Não vai salvar o pedido sem salvar os itens do pedido
            $pedido[0]->save(); 

            //Repondo o estoque dos itens cancelados na farmácia que não estejam cancelados
            DB::table('produto_farmacias')  
            ->join('pedidos', 'pedidos.id_farmacia', '=', 'produto_farmacias.id_farmacia')            
            ->join("pedido_produtos",function($join){
                $join->on("pedido_produtos.id_pedido","=","pedidos.id")
                    ->on("pedido_produtos.id_produto","=","produto_farmacias.id_produto");
            })
            ->where('pedidos.id', '=', $pedido[0]->id)
            ->where(function($query) {
                $query->whereNull('pedido_produtos.confirmado')
                      ->orWhere('pedido_produtos.confirmado', '=', 1);
            })
            ->update(['produto_farmacias.estoque' => DB::raw('produto_farmacias.estoque + pedido_produtos.quantidade')]);

            //Alterando a flag "confirmado dos produtos dos pedidos pra 0
            $pedidoProduto = DB::table('pedido_produtos')  
            ->where('id_pedido', '=', $pedido[0]->id)
            ->update(['confirmado' => 0]);            

            $admFarmacia = DB::table('usuarios')
            ->where('id', '=', $farmacia->id_usuario)
            ->get();

            if($id_perfil == 1)
            {
                EmailController::enviaEmailClienteCancelaPedidoCliente($cliente->nome, $cliente->email, $pedido[0]->id);
                EmailController::enviaEmailClienteCancelaPedidoFarmacia($farmacia->nome, $admFarmacia[0]->email, $pedido[0]->id);
            }
            else
            {
                EmailController::enviaEmailFarmaciaCancelaPedidoCliente($cliente->nome, $cliente->email, $pedido[0]->id);
                EmailController::enviaEmailFarmaciaCancelaPedidoFarmacia($farmacia->nome, $admFarmacia[0]->email, $pedido[0]->id);
            }
            

            DB::commit();

            return ['sucesso' => 1, 'mensagem' => 'Pedido cancelado com sucesso!'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/PedidoService', 'mensagem' => $e->getMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao cancelar pedido. Tente novamente mais tarde'];
        }
    }




    public function cancelaProdutoPedido(Pedido $pedido, $id_produto_pedido)
    {  
        $pedidoProduto = PedidoProduto::where('id', $id_produto_pedido)->get()[0];

        $produtoAguardandoConfirmacao = PedidoProduto::where('id_pedido', $pedido->id)            
            ->whereNull('confirmado')
            ->where('id', '!=', $id_produto_pedido)
            ->get();

        try
        {
            $pedidoProduto->confirmado = 0;

            DB::beginTransaction();
            $pedidoProduto->save(); 

            //Atualizando o valor cancelado do pedido
            $pedido->valor_cancelado += ($pedidoProduto->valor * $pedidoProduto->quantidade);

            //Repondo o estoque do produto cancelado na farmácia
            DB::table('produto_farmacias')  
            ->join('pedido_produtos', 'pedido_produtos.id_produto', '=', 'produto_farmacias.id_produto')  
            ->where('pedido_produtos.id', '=', $pedidoProduto->id)
            ->update(['produto_farmacias.estoque' => DB::raw('produto_farmacias.estoque + pedido_produtos.quantidade')]);    
            
            //Se o pedido ainda tiver um produto aguardando confirmação, mantém o status do pedido
            if(!empty($produtoAguardandoConfirmacao[0]))
            {  
                $pedido->save(); 
                DB::commit();
                return ['sucesso' => 1, 'mensagem' => 'Produto cancelado com sucesso!'];
            }
            else
            {                
                $produtoConfirmado = PedidoProduto::where('id_pedido', $pedido->id)            
                ->where('confirmado', 1)
                ->where('id', '!=', $id_produto_pedido)
                ->get();

                //Se o pedido tiver algum produto confirmado, e mais nenhum produto aguardando confirmação, muda o status do pedido para parcialmente confirmado
                if(!empty($produtoConfirmado[0]))
                {
                    $pedido->id_status = 3;                    
                    $pedido->save();
                    DB::commit();
                    return ['sucesso' => 1, 'mensagem' => 'Produto cancelado com sucesso!'];
                }
                //Se o pedido não tiver nenhum produto confirmado, e mais nenhum produto aguardando confirmação, muda o status do pedido cancelado
                else
                {
                    $cliente = Usuario::where('id', $pedido->id_usuario)->get()[0];
                    $farmacia = Farmacia::where('id', $pedido->id_farmacia)->get()[0];

                    $cancelaPedido = $this->cancelaPedido($cliente, $farmacia, $pedido->id, 2);

                    if($cancelaPedido['sucesso'] == 1)
                    {
                        DB::commit();
                    }
                    
                    return $cancelaPedido;
                }
            }
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/PedidoService', 'mensagem' => $e->getMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao cancelar produto. Tente novamente mais tarde'];
        }
    }




    public function confirmaPedido(Pedido $pedido)
    { 
        try
        {

            $pedido->id_status = 2;           

            DB::beginTransaction(); //Não vai salvar o pedido sem salvar os itens do pedido
            

            //Alterando a flag "confirmado dos produtos dos pedidos pra 0
            $pedidoProduto = DB::table('pedido_produtos')  
            ->where('id_pedido', '=', $pedido->id)
            ->whereNull('confirmado')
            ->update(['confirmado' => 1]);            

            $produtoCancelado = DB::table('pedido_produtos')  
                ->where('id_pedido', '=', $pedido->id)
                ->where('confirmado', '=', 0)
                ->get();
            
            //Se não tiver nenhum produto cancelado, muda o pedido pra confirmado
            if(empty($produtoCancelado[0]))
            {
                $pedido->id_status = 2;
            }
            //Se tiver algum produto cancelado, muda o pedido pra parcialmente confirmado
            else
            {
                $pedido->id_status = 3;
            }

            $pedido->save();

            DB::commit();

            return ['sucesso' => 1, 'mensagem' => 'Pedido confirmado com sucesso!'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/PedidoService', 'mensagem' => $e->getMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao confirmar pedido. Tente novamente mais tarde'];
        }
    }






    public function confirmaProdutoPedido(Pedido $pedido, $id_produto_pedido)
    {  
        $pedidoProduto = PedidoProduto::where('id', $id_produto_pedido)->get()[0];

        $produtoAguardandoConfirmacao = PedidoProduto::where('id_pedido', $pedido->id)            
            ->whereNull('confirmado')
            ->where('id', '!=', $id_produto_pedido)
            ->get();

        try
        {
            $pedidoProduto->confirmado = 1;            

            DB::beginTransaction();
            $pedidoProduto->save(); 
            
            //Se o pedido ainda tiver um produto aguardando confirmação, mantém o status do pedido
            if(!empty($produtoAguardandoConfirmacao[0]))
            {                 
                DB::commit();
                return ['sucesso' => 1, 'mensagem' => 'Produto confirmado com sucesso!'];
            }
            else
            {                
                $produtoCancelado = PedidoProduto::where('id_pedido', $pedido->id)            
                ->where('confirmado', 0)
                ->where('id', '!=', $id_produto_pedido)
                ->get();

                //Se o pedido tiver algum produto cancelado, e mais nenhum produto aguardando confirmação, muda o status do pedido para parcialmente confirmado
                if(!empty($produtoCancelado[0]))
                {
                    $pedido->id_status = 3;                    
                    $pedido->save();
                    DB::commit();                    
                }
                //Se o pedido não tiver nenhum produto cancelado, e mais nenhum produto aguardando confirmação, muda o status do pedido confirmado
                else
                {
                    $pedido->id_status = 2;                    
                    $pedido->save();
                    DB::commit();
                }
                return ['sucesso' => 1, 'mensagem' => 'Produto confirmado com sucesso!'];
            }
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/PedidoService', 'mensagem' => $e->getMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao confirmar produto. Tente novamente mais tarde'];
        }
    }






    public function concluiPedido(Usuario $cliente, Farmacia $farmacia, Pedido $pedido)
    { 
        try
        {
            $pedidoProduto = PedidoProduto::where('id_pedido', $pedido->id)
                ->where('confirmado', 0)
                ->get();

            //Se não tiver nenhum produto cancelado muda o status pra concluído
            if(empty($pedidoProduto[0]))
            {
                $pedido->id_status = 5;
            }
            //Se tiver algum produto cancelado muda o status pra parcialmente concluído
            else
            {
                $pedido->id_status = 6;
            }        

            DB::beginTransaction();
            
            $pedido->save();

            $admFarmacia = Usuario::where('id', $farmacia->id_usuario)->get()[0];

            EmailController::enviaEmailPedidoConcluidoCliente($cliente->nome, $cliente->email, $pedido->id, $pedido->id_status);
            EmailController::enviaEmailPedidoConcluidoFarmacia($farmacia->nome, $admFarmacia->email, $pedido->id, $pedido->id_status);
            

            DB::commit();

            return ['sucesso' => 1, 'mensagem' => 'Pedido Concluído com sucesso!'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/PedidoService', 'mensagem' => $e->getMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao Concluir pedido. Tente novamente mais tarde'];
        }
    }

    /*public function cancelaPedido(Usuario $usuario, $id_pedido)
    {    
        
        $pedido = Pedido::where('id', $id_pedido)->get();
        //ddd($pedido[0]);

        try
        {

            $pedido[0]->id_status = 7;
            $pedido[0]->valor_cancelado = $pedido[0]->valor_pedido + $pedido[0]->valor_entrega - $pedido[0]->valor_desconto;

            DB::beginTransaction(); //Não vai salvar o pedido sem salvar os itens do pedido
            $pedido[0]->save(); 

            //Repondo o estoque dos itens cancelados na farmácia
            DB::table('produto_farmacias')  
            ->join('pedidos', 'pedidos.id_farmacia', '=', 'produto_farmacias.id_farmacia')            
            ->join("pedido_produtos",function($join){
                $join->on("pedido_produtos.id_pedido","=","pedidos.id")
                    ->on("pedido_produtos.id_produto","=","produto_farmacias.id_produto");
            })
            ->where('pedidos.id', '=', $pedido[0]->id)
            ->update(['produto_farmacias.estoque' => DB::raw('produto_farmacias.estoque + pedido_produtos.quantidade')]);

            //Alterando a flag "confirmado dos produtos dos pedidos pra 0
            $pedidoProduto = DB::table('pedido_produtos')  
            ->where('id_pedido', '=', $pedido[0]->id)
            ->update(['confirmado' => 0]);

            $farmacia = DB::table('farmacias')
            ->where('id', '=', $pedido[0]->id_farmacia)
            ->get();

            $admFarmacia = DB::table('usuarios')
            ->where('id', '=', $farmacia[0]->id_usuario)
            ->get();

            EmailController::enviaEmailClienteCancelaPedidoCliente($usuario->nome, $usuario->email, $pedido[0]->id);
            EmailController::enviaEmailClienteCancelaPedidoFarmacia($farmacia[0]->nome, $admFarmacia[0]->email, $pedido[0]->id);

            DB::commit();

            return ['sucesso' => 1, 'mensagem' => 'Pedido cancelado com sucesso!'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/PedidoService', 'mensagem' => $e->getMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao realizar pedido. Tente novamente mais tarde'];
        }
    }



    public function cancelaPedidoFarmacia(Farmacia $farmacia, $id_pedido)
    {    
        
        $pedido = Pedido::where('id', $id_pedido)->get();
        //ddd($pedido[0]);

        try
        {

            $pedido[0]->id_status = 7;
            $pedido[0]->valor_cancelado = $pedido[0]->valor_pedido + $pedido[0]->valor_entrega - $pedido[0]->valor_desconto;

            DB::beginTransaction(); //Não vai salvar o pedido sem salvar os itens do pedido
            $pedido[0]->save(); 

            //Repondo o estoque dos itens cancelados na farmácia
            DB::table('produto_farmacias')  
            ->join('pedidos', 'pedidos.id_farmacia', '=', 'produto_farmacias.id_farmacia')            
            ->join("pedido_produtos",function($join){
                $join->on("pedido_produtos.id_pedido","=","pedidos.id")
                    ->on("pedido_produtos.id_produto","=","produto_farmacias.id_produto");
            })
            ->where('pedidos.id', '=', $pedido[0]->id)
            ->update(['produto_farmacias.estoque' => DB::raw('produto_farmacias.estoque + pedido_produtos.quantidade')]);

            //Alterando a flag "confirmado dos produtos dos pedidos pra 0
            $pedidoProduto = DB::table('pedido_produtos')  
            ->where('id_pedido', '=', $pedido[0]->id)
            ->update(['confirmado' => 0]);

            $usuario = DB::table('usuarios')
            ->where('id', '=', $pedido[0]->id_usuario)
            ->get();

            $admFarmacia = Auth::user();

            EmailController::enviaEmailFarmaciaCancelaPedidoCliente($usuario[0]->nome, $usuario[0]->email, $pedido[0]->id);
            EmailController::enviaEmailFarmaciaCancelaPedidoFarmacia($farmacia->nome, $admFarmacia->email, $pedido[0]->id);

            DB::commit();

            return ['sucesso' => 1, 'mensagem' => 'Pedido cancelado com sucesso!'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/PedidoService', 'mensagem' => $e->getMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao realizar pedido. Tente novamente mais tarde'];
        }
    }*/
}