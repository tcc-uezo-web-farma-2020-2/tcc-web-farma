<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\Usuario;
use App\Models\Endereco;
use App\Http\Controllers\EmailController;
use Auth;
use Log;

class SessaoService
{
    public function getEndereco()
    {
        $sessao['endereco'] = session('end', []);
        $endereco = new DB();

        $usuario = Auth::user();

        //ddd($sessao);

        //Só vai colocar o endereço padrão se não tiver cliente logado e se não tiver nenhum endereço cadastrado na sessão (alterado pelo usuário)
        if (empty($usuario) && empty($sessao['endereco']))
        {
            $endereco->descricao = 'UEZO';
            $endereco->logradouro = 'Avenida Manuel Caldeira de Alvarenga';
            $endereco->numero = 1203;
            $endereco->complemento = '';
            $endereco->bairro = 'Campo Grande';
            $endereco->cidade = 'Rio de Janeiro';
            $endereco->estado = 'RJ';
            $endereco->latitude = -22.9003256;
            $endereco->longitude = -43.57868000000001;

            $sessao['endereco'] = $endereco;     
        }
        else if (!empty($usuario))
        {
            $end = $this->getDadosEndereco($usuario->id);

            $endereco->descricao = $end[0]->descricao;
            $endereco->logradouro = $end[0]->logradouro;
            $endereco->numero = $end[0]->numero;
            $endereco->complemento = $end[0]->complemento;
            $endereco->bairro = $end[0]->bairro;
            $endereco->cidade = $end[0]->cidade;
            $endereco->estado = $end[0]->estado;
            $endereco->latitude = $end[0]->latitude;
            $endereco->longitude = $end[0]->longitude;

            $sessao['endereco'] = $endereco;
        }

        
        session(['end' => $sessao['endereco']]);

        return $sessao;
    }

    public function getFarmaciasRegiao()
    {
        $endereco = $this->getEndereco();

        if(!empty($usuario))
        {
            //Selecionando o cliente da sessão e o endereço
            $latlngClienteQuery = DB::table('usuarios', 'u')
            ->select('enderecos.latitude', 'enderecos.longitude')
            ->join('enderecos', 'u.id', '=', 'enderecos.id_usuario')            
            ->where('enderecos.em_uso', '=', 1)            
            ->where('u.id_perfil', '=', 1)
            ->where('u.id', '=', $usuario->id)
            ->get();

            //ddd($latlngClienteQuery->get());
        
            $latlngCliente = $latlngClienteQuery[0];
        }
        else
        {
            $latlngCliente = new DB();
            $latlngCliente->latitude = $endereco['endereco']->latitude;
            $latlngCliente->longitude = $endereco['endereco']->longitude;
        }
        

         
        
        //Pega as farmácias que entregam no endereço do cliente
        return DB::table('farmacias')
            ->select('farmacias.id', 'farmacias.aberto', 'farmacias.nome as nome_farmacia', 'valor_entregas.valor as entrega')
            ->join('valor_entregas', 'farmacias.id', '=', 'valor_entregas.id_farmacia')
            ->where('farmacias.aberto', '=', 1)
            ->whereRaw('distanciaCoordenadas(farmacias.latitude, farmacias.longitude, ' . $latlngCliente->latitude . ', ' . $latlngCliente->longitude . ') < farmacias.raio_entrega')
            ->whereRaw('distanciaCoordenadas(farmacias.latitude, farmacias.longitude, ' . $latlngCliente->latitude . ', ' . $latlngCliente->longitude . ') >= valor_entregas.raio_km_de')
            ->whereRaw('distanciaCoordenadas(farmacias.latitude, farmacias.longitude, ' . $latlngCliente->latitude . ', ' . $latlngCliente->longitude . ') <= valor_entregas.raio_km_ate');

        //ddd($farmaciasReg->get());
    }

    public function getDadosEndereco($id_usuario)       
    {
        return DB::table('enderecos')            
            ->where('id_usuario', '=', $id_usuario)
            ->where('em_uso', '=', 1)
            ->get();
    }
}