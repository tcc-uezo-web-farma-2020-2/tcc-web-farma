<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\Usuario;
use App\Models\Farmacia;
use App\Models\Endereco;
use App\Models\ValorEntrega;
use App\Http\Controllers\EmailController;
use App\Services\PedidoService;
use Log;

class UsuarioService
{
    public function cadastraCliente(Usuario $usuario, Endereco $endereco)
    {
        $usuarioExistente = DB::table('usuarios', 'u')
            ->select('u.email', 'u.cpf')
            ->where('u.cpf', '=', $usuario->cpf)
            ->whereOr('u.email', '=', $usuario->email)
            ->get();

        if (!empty($usuarioExistente[0]))
        {
            if($usuarioExistente[0]->cpf == $usuario->cpf)
            {
                return ['sucesso' => 0, 'mensagem' => 'CPF já cadastrado'];
            }
            return ['sucesso' => 0, 'mensagem' => 'E-mail já cadastrado'];
        }

        try
        {
            DB::beginTransaction(); //Só vai salvar o usuário se o endereço também for salvo
            $usuario->save(); //salvando o usuário no banco
            $endereco->id_usuario = $usuario->id;
            $endereco->save();

            //Envia e-mail de confirmação
            EmailController::enviaEmailCadastro($usuario->nome, $usuario->email, $usuario->codigo_verificacao);

            DB::commit();
            
            return ['sucesso' => 1, 'mensagem' => 'Cadastro realizado com sucesso! Um e-mail de confirmação foi enviado ao seu e-mail'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/UsuarioService', 'mensagem' => $e->gettMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao realizar cadastro. Tente novamente mais tarde'];
        }
    }



    public function cadastraFarmacia(Usuario $usuario, Farmacia $farmacia, ValorEntrega $entrega1, ValorEntrega $entrega2, ValorEntrega $entrega3)
    {
        $usuarioExistente = DB::table('usuarios', 'u')
            ->select('u.email', 'u.cpf')
            ->where('u.cpf', '=', $usuario->cpf)
            ->whereOr('u.email', '=', $usuario->email)
            ->get();

        if (!empty($usuarioExistente[0]))
        {
            if($usuarioExistente[0]->cpf == $usuario->cpf)
            {
                return ['sucesso' => 0, 'mensagem' => 'CPF já cadastrado'];
            }
            return ['sucesso' => 0, 'mensagem' => 'E-mail já cadastrado'];
        }

        try
        {
            DB::beginTransaction(); //Só vai salvar o usuário se o endereço também for salvo
            $usuario->save(); //salvando o usuário no banco
            $farmacia->id_usuario = $usuario->id;
            $farmacia->save();
            $entrega1->id_farmacia = $farmacia->id;
            $entrega1->save();

            if(!empty($entrega2->raio_km_de))
            {
                $entrega2->id_farmacia = $farmacia->id;
                $entrega2->save();
            }

            if(!empty($entrega3->raio_km_de))
            {
                $entrega3->id_farmacia = $farmacia->id;
                $entrega3->save();
            }

            //Envia e-mail de confirmação
            EmailController::enviaEmailCadastro($usuario->nome, $usuario->email, $usuario->codigo_verificacao);

            DB::commit();
            
            return ['sucesso' => 1, 'mensagem' => 'Cadastro realizado com sucesso! Um e-mail de confirmação foi enviado ao seu e-mail'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/UsuarioService', 'mensagem' => $e->gettMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao realizar cadastro. Tente novamente mais tarde'];
        }
    }



    public function alteraEnderecoUso(Endereco $endereco)
    {     

        $endereco->em_uso = 1;

        try
        {
            DB::beginTransaction(); 

            DB::table('enderecos')       
            ->where('id_usuario', '=', $endereco->id_usuario)
            ->where('em_uso', '=', 1)
            ->update(['em_uso' => 0]);

            $endereco->save();                 
            
            DB::commit();
            
            return ['sucesso' => 1, 'mensagem' => 'Endereço em uso alterado com sucesso'];
        }
        catch(Exception $e)
        {
            //Se der alguma exceção no meio da transação da um rollback para garantir que não va salvar
            DB::rollback(); 
            //Armazena o erro no arquivo de log (storage/logs/laravel.log)
            Log::error('Erro', ['Arquivo' => 'App/Services/UsuarioService', 'mensagem' => $e->getMessage()]);
            //ddd($e);
            return ['sucesso' => 0, 'mensagem' => 'Erro ao alterar endereço em uso. Tente novamente mais tarde'];
        }
    }


    public function abreFechaFarmacia(Farmacia $farmaciaUsuario)
    {
        //Abrir farmácia
        if($farmaciaUsuario->aberto == 0)
        {
            $farmaciaUsuario->aberto = 1;
            $farmaciaUsuario->save();

            $retorno['mensagem'] = 'Farmácia aberta com sucesso';
            $retorno['sucesso'] = 1; 
        }
        //Fechar farmácia
        else
        {   
            //Cancelando pedidos aguardando confirmação
            $pedidos = DB::table('pedidos')
            ->where('id_farmacia', '=', $farmaciaUsuario->id)
            ->where('id_status', '=', 1)
            ->get();

            foreach($pedidos as $p)
            {
                $pedidoService = new PedidoService();
                $retorno = $pedidoService->cancelaPedidoFarmacia($farmaciaUsuario, $p->id);       

                if($retorno['sucesso'] == 0)
                {
                    $retorno['mensagem'] = 'Não é possível realizar essa ação. Tente novamente mais tarde';
                    return $retorno;
                }
            }  
            
            //Fechando a farmácia
            $farmaciaUsuario->aberto = 0;
            $farmaciaUsuario->save();

            $retorno['mensagem'] = 'Farmácia fechada com sucesso';
            $retorno['sucesso'] = 1; 
            
        }

        return $retorno;
    }
    
}

