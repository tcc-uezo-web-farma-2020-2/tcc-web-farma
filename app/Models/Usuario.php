<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

//use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Notifications\Notifiable;
//use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Model implements Authenticatable //extends ConfigModel
{
    use HasFactory;
    protected $fillable = [ //Campos que a tabela terá
        'id_perfil',
        'nome',
        'email',          
        'password',
        'cpf',
        'data_nascimento',
        'telefone',
        'celular',
        'genero',
        'codigo_verificacao',
        'email_verificado'
    ];


    //Implementando os métodos da interface Authenticatable
    //Métodos de autenticação
    public function getAuthIdentifierName()
    {
        //Retornando a chave com o nome do objeto autenticado
        return 'email'; //$this->getKey();
    }

    public function getAuthIdentifier()
    {
        //Retornar um identificador do usuário
        return $this->email;
    }
  
    public function getAuthPassword()
    {
        //Retorna a senha do usuário
        return $this->password;
    }

    //Métodos para manter o usuário logado (Implementação futura) 
    public function getRememberToken()
    {

    }
 
    public function setRememberToken($value)
    {

    }

    public function getRememberTokenName()
    {

    }
    
}
