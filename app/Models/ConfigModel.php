<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConfigModel extends Model
{
    use HasFactory;

    protected $primaryKey = 'id'; //padrão dos ids
    public $timestamps = true; //created_at e Updated_at
    public $autoIncrement = true; //id auto incremento
    protected $fillable = []; //atributos da tabela

    public function sucessoSave()
    {
        return true;
    }

    //sobrescrevendo método save do laravel
    public function save(array $options = [])
    {
        try
        {
            if (!$this->sucessoSave())
            {
                return false;
            }

            //Persistindo dados no banco
            return parent::save($options);
        }
        catch (\Exception $e)
        {
            throw new \Exception($e->getMessage());
        }
    }

}