<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FormaPagamento extends ConfigModel
{
    use HasFactory;

    protected $fillable = //Campos que a tabela terá
    [ 
        'forma_pagamento',
        'classe_icone',
        'classe_estilo'
    ];
}
