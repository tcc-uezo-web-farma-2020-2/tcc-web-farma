<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ValorEntrega extends ConfigModel
{
    use HasFactory;

    protected $fillable = [
        'id_farmacia',
        'raio_km_de',
        'raio_km_ate',
        'valor'
    ];
}
