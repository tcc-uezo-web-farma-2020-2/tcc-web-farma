<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PedidoProduto extends ConfigModel
{
    use HasFactory;

    protected $fillable = //Campos que a tabela terá
    [
        'id_pedido',
        'id_produto',
        'valor',
        'valor_desconto',
        'quantidade',
        'confirmado',
    ];
}
