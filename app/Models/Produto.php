<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Produto extends ConfigModel
{
    use HasFactory;

    protected $fillable = //Campos que a tabela terá
    [ 
        'id_categoria',
        'produto',
        'slug',
        'cod_barras',
        'quantidade',
        'variacao',
        'composicao',
        'detalhes',
        'imagens'
    ];
}
