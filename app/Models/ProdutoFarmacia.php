<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProdutoFarmacia extends ConfigModel
{
    use HasFactory;

    protected $fillable = //Campos que a tabela terá
    [ 
        'id_farmacia',
        'id_produto',
        'valor',
        'estoque'
    ];
}
