<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Endereco extends ConfigModel
{
    use HasFactory;

    protected $fillable = //Campos que a tabela terá
    [ 
        'id_usuario',
        'descricao',
        'em_uso',
        'logradouro',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'estado',
        'cep',
        'latitude',
        'longitude'
    ];
}
