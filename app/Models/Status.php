<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Status extends ConfigModel
{
    use HasFactory;

    protected $fillable = //Campos que a tabela terá
    [ 
        'status',
        'classe_estilo'
    ];
}
