<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Farmacia extends ConfigModel
{
    use HasFactory;

    protected $fillable = //Campos que a tabela terá
    [ 
        'id_usuario',
        'nome',
        //'email',
        'cnpj',
        'aberto',
        'raio_entrega',
        'logradouro',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'estado',
        'cep',
        'latitude',
        'longitude'

        /*'id_usuario',       
        'aberto',
        'raio_entrega',*/
    ];
}
