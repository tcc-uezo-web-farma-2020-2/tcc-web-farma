<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Categoria extends ConfigModel
{
    use HasFactory;

    protected $fillable = [ //Campos que a tabela terá
        'categoria',
        'slug'
    ];
}
