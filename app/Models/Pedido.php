<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pedido extends ConfigModel
{
    use HasFactory;

    protected $fillable = //Campos que a tabela terá
    [ 
        'id_usuario',
        'id_endereco',
        'id_farmacia',
        'id_status',
        'id_forma_pagamento',
        'valor_pedido',
        'valor_desconto',
        'valor_cancelado',
        'valor_entrega',         
        'valor_total'
    ];
}
