function getProduto()
{   
    if(document.getElementById('codbarras').value != null && document.getElementById('codbarras').value.length == 13 && document.getElementById('codbarras').value.startsWith('789'))
    {
        document.getElementById('codbarras').classList.remove("border-danger");

        let link = '/farmacia/estoque/' + document.getElementById('codbarras').value;	

        console.log(link);

        let xmlHttp = new XMLHttpRequest();
        xmlHttp.open('GET', link);

        //console.log(xmlHttp);

        xmlHttp.onreadystatechange = () =>
        {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
            {			
                let produto = JSON.parse(xmlHttp.responseText);

                //console.log(produto);

                //Verifica se o objeto retornado foi um json
                if(!produto[0].hasOwnProperty('produto'))
                {
                    /*if(document.getElementById('categoria').type == 'text')
                    {
                        document.getElementById('categoria').remove();
                        let categ = document.createElement("select");
                        categ.setAttribute("id", "categoria");
                        categ.setAttribute("type", "text");
                        categ.setAttribute("name", "categoria");                        
                        categ.classList.add("form-control");
                        categ.classList.add("w-75");                          

                        produto[0].forEach(criaOption);                        

                        function criaOption(item, index)
                        {                            
                            let option = document.createElement("option");
                            option.innerHTML = item.categoria;
                            option.value = item.id;
                            categ.appendChild(option);
                        }

                        document.getElementById('divcategoria').appendChild(categ);
                    }*/

                    document.getElementById('produto').value = null;
                    document.getElementById('categoria').value = null;
                    document.getElementById('quantidade').value = null;
                    document.getElementById('variacao').value = null;
                    document.getElementById('composicao').value = null;
                    document.getElementById('estoque').value = null;
                    document.getElementById('id_produto').value = null;
                    document.getElementById('preco').value = null;
                    document.getElementById('imagem').removeAttribute('src');

                    if(!document.getElementById('erro'))
                    {
                        let erro = document.createElement("small");
                        erro.setAttribute("id", "erro");        
                        erro.classList.add("text-danger");
                        erro.classList.add("font-weight-bold");
                        document.getElementById('codbarras').classList.add("border-danger");
                        erro.innerHTML = 'Produto não encontrado';

                        document.getElementById('diverro').appendChild(erro);  
                    }
                    else
                    {
                        erro.innerHTML = 'Produto não encontrado';
                    }
                    
                    /*document.getElementById('erro').remove();
                    document.getElementById('produto').readOnly = false;
                    document.getElementById('produto').setAttribute("placeholder", "Ex: Advil 400mg 2 cápsulas");
                    document.getElementById('categoria').readOnly = false;
                    document.getElementById('quantidade').readOnly = false;
                    document.getElementById('quantidade').setAttribute("placeholder", "Ex: 10 cápsulas, 300ml, 50 unidades, 20m, etc.");
                    document.getElementById('variacao').readOnly = false;
                    document.getElementById('variacao').setAttribute("placeholder", "Ex: Cor, cápsulas, pomada, sachê, 100mg, 200mg, etc.");
                    document.getElementById('composicao').readOnly = false;
                    document.getElementById('composicao').setAttribute("placeholder", "Ex: Ibuprofeno, Paracetamol, etc.");*/

                }
                else
                {     
                    /*if(document.getElementById('categoria').type == 'select-one')
                    {
                        document.getElementById('categoria').remove();
                        let categ = document.createElement("input");
                        categ.setAttribute("id", "categoria");
                        categ.setAttribute("type", "text");
                        categ.setAttribute("name", "categoria");                       
                        categ.classList.add("form-control");
                        categ.classList.add("w-75");
                        categ.readOnly = true;

                        document.getElementById('divcategoria').appendChild(categ);
                    }*/
                    
                    document.getElementById('produto').readOnly = true;
                    document.getElementById('variacao').readOnly = true;
                    document.getElementById('quantidade').readOnly = true;
                    document.getElementById('variacao').readOnly = true;
                    document.getElementById('composicao').readOnly = true;
                    

                    if(document.getElementById('erro'))
                    {
                        document.getElementById('erro').remove();
                    }
                    document.getElementById('produto').value = produto[0].produto;
                    document.getElementById('categoria').value = produto[0].categoria;
                    document.getElementById('quantidade').value = produto[0].quantidade;
                    document.getElementById('variacao').value = produto[0].variacao;
                    document.getElementById('variacao').removeAttribute('placeholder');
                    document.getElementById('composicao').value = produto[0].composicao;
                    document.getElementById('composicao').removeAttribute('placeholder');
                    document.getElementById('estoque').value = produto[0].estoque;
                    document.getElementById('id_produto').value = produto[0].id_produto;
                    document.getElementById('imagem').src = '/' + produto[0].imagens;
                    
                    if(produto[0].valor != null)
                    {
                        document.getElementById('preco').value = formatoNumero().format(produto[0].valor);
                        //document.getElementById('preco').value = produto[0].valor.toLocaleString('pt-br',{currency: 'BRL'});
                    }
                }

                //console.log(coordenadas.results[0]);

                //document.getElementById('latitude').value = coordenadas.results[0].geometry.location.lat; 
                //document.getElementById('longitude').value = coordenadas.results[0].geometry.location.lng; 
                
            }
        }	

        xmlHttp.send();
    }
    else
    {
        if(!document.getElementById('erro'))
        {
            let erro = document.createElement("small");
            erro.setAttribute("id", "erro");        
            erro.classList.add("text-danger");
            erro.classList.add("font-weight-bold");
            document.getElementById('codbarras').classList.add("border-danger");
            erro.innerHTML = 'Código de barras inválido';

            document.getElementById('diverro').appendChild(erro);  
        }
        else
        {
            erro.innerHTML = 'Código de barras inválido';
        }

        document.getElementById('produto').value = null;
        document.getElementById('categoria').value = null;
        document.getElementById('quantidade').value = null;
        document.getElementById('variacao').value = null;
        document.getElementById('composicao').value = null;
        document.getElementById('estoque').value = null;
        document.getElementById('id_produto').value = null;
        document.getElementById('preco').value = null;
        document.getElementById('imagem').removeAttribute('src');
              
    }
}

function formatoNumero()
{
    let formato = new Intl.NumberFormat("pt-br", {minimumFractionDigits: 2, maximumFractionDigits: 2})
    return formato;
}



