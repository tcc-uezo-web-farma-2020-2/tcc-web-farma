//https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyDyadhuXbOmAlWizte0xXop7WngiowbV08


function verificaNumero()
{
    var numero = document.getElementById('numero').value;

    if(numero == null || numero == '')
    {
        document.getElementById('numero').classList.add("border-danger");
    }    
}

function getCoordenadas()
{
    //Se o endereço estiver completo
    if(document.getElementById('logradouro').value != "" && document.getElementById('numero').value != "" && document.getElementById('bairro').value != "" && document.getElementById('cidade').value != "" && document.getElementById('estado').value != "")
    {
        document.getElementById("numero").classList.remove("is-invalid");
        if(document.getElementById("erronumero"))
        {
            document.getElementById("erronumero").remove();
        }
        
        //document.getElementById("divnumero").removeChild(document.getElementById("erronumero"));

        var endereco = document.getElementById('logradouro').value + '+' + document.getElementById('numero').value + '+' + document.getElementById('bairro').value + '+' + document.getElementById('cidade').value + '+' + document.getElementById('estado').value;
        endereco = endereco.replace(/ /g, "+");
        //console.log(endereco);

        let link = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + endereco + '&key=AIzaSyDyadhuXbOmAlWizte0xXop7WngiowbV08';	

        console.log(link);

        let xmlHttp = new XMLHttpRequest();
        xmlHttp.open('GET', link);

        xmlHttp.onreadystatechange = () =>
        {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
            {			
                let coordenadas = JSON.parse(xmlHttp.responseText);

                //console.log(coordenadas.results[0]);

                document.getElementById('latitude').value = coordenadas.results[0].geometry.location.lat; 
                document.getElementById('longitude').value = coordenadas.results[0].geometry.location.lng; 
                
            }
        }	

        xmlHttp.send();
    }
    else
    {
        document.getElementById('numero').classList.add("is-invalid");
        let erro = mensagemErro('O endereço está incompleto', 'numero');
        if (!document.getElementById('erronumero'))
        {
            document.getElementById('divnumero').appendChild(erro);
        }
        //document.getElementById('divnumero').appendChild(erro);
    }
}

function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('logradouro').value=("");
        document.getElementById('numero').value=("");
        document.getElementById('complemento').value=("");
        document.getElementById('bairro').value=("");
        document.getElementById('cidade').value=("");
        document.getElementById('estado').value=("");
        //document.getElementById('ibge').value=("");
        document.getElementById('latitude').value=("");
        document.getElementById('longitude').value=("");

        //Permite a alteração dos campos de novo
        /*document.getElementById('logradouro').readOnly = false;
        document.getElementById('bairro').readOnly = false;
        document.getElementById('cidade').readOnly = false;
        document.getElementById('estado').readOnly = false;
        //document.getElementById('ibge').readOnly = false; */
}

function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //console.log(conteudo); 

        document.getElementById("cep").classList.remove("is-invalid");

        if(document.getElementById("errocep"))
        {
            document.getElementById("errocep").remove();
        }

        //document.getElementById("divcep").removeChild(document.getElementById("errocep"));
        
        //Exibe os campos número e complemento, após encontrar o cep
        /*document.getElementById('numero').style.display = "inline";
        document.getElementById('numeroLabel').style.display = "inline";
        document.getElementById('complemento').style.display = "inline";
        document.getElementById('complementoLabel').style.display = "inline";*/

        //Atualiza os campos com os valores.
        document.getElementById('cep').classList.remove("border-danger");
        document.getElementById('logradouro').value=(conteudo.logradouro);
        document.getElementById('bairro').value=(conteudo.bairro);
        document.getElementById('cidade').value=(conteudo.localidade);
        document.getElementById('estado').value=(conteudo.uf);
        //document.getElementById('ibge').value=(conteudo.ibge);

        //Impede a alteração dos campos
        document.getElementById('logradouro').readOnly = true;
        document.getElementById('bairro').readOnly = true;
        document.getElementById('cidade').readOnly = true;
        document.getElementById('estado').readOnly = true;
        //document.getElementById('ibge').readOnly = true;

        
    } //end if.
    else {
        //CEP não Encontrado.
        limpa_formulário_cep();
        document.getElementById('cep').classList.add("is-invalid");
        let erro = mensagemErro('CEP não encontrado', 'cep');
        if (!document.getElementById('errocep'))
        {
            document.getElementById('divcep').appendChild(erro);
        }
        //alert("CEP não encontrado.");
    }
}
    
function pesquisacep(valor) {

    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            document.getElementById('logradouro').value="...";
            document.getElementById('bairro').value="...";
            document.getElementById('cidade').value="...";
            document.getElementById('estado').value="...";
            //document.getElementById('ibge').value="...";

            //Cria um elemento javascript.
            var script = document.createElement('script');

            //Sincroniza com o callback.
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);

        } //end if.
        else {
            //cep é inválido.
            limpa_formulário_cep();
            document.getElementById('cep').classList.add("is-invalid");
            let erro = mensagemErro('CEP inválido', 'cep');
            if (!document.getElementById('errocep'))
            {
                document.getElementById('divcep').appendChild(erro);
            }
            
            //alert("Formato de CEP inválido.");
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
}

function mensagemErro(mensagem, id)
{
    var div = document.createElement("small");
    div.classList.add("text-danger");
    div.classList.add("font-weight-bold");
    div.setAttribute("id", "erro" + id)
    
    div.innerHTML = mensagem;

    return div;
}
