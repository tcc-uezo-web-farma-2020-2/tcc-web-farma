function adicionar(id)
{
    var quantidade = parseInt(document.getElementById('qtd' + id).value);
    quantidade += 1;
    document.getElementById('qtd' + id).value = quantidade;
}

function remover(id)
{
    var quantidade = document.getElementById('qtd' + id).value;
    
    if (quantidade > 1)
    {
        quantidade -= 1;
        document.getElementById('qtd' + id).value = quantidade;
    }   
}