function validaCpf(cpf)
{
    var mask = new StringMask('000.000.000-00');
    if(mask.validate(cpf))
    {
        document.getElementById('cpf').value = mask.apply(cpf);
    }
}