<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">

	<!--<link rel="stylesheet" type="text/css" href="/css/style.css"> --><!-- Arquivo CSS está no diretório público e está sendo recuperado de lá pelo servidor HTTP -->

</head>
<body>

    <div class="container">

        <div id="login" class="col-12 border">
            <h3 class="text-center text-dark pt-5">Olá, {{ $dados['nome'] }}!</h3>

            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-6 justify-content-center align-items-center">
                    <div id="login-box" class="col-md-12">
                        @if($dados['id_status'] == 5)
                            <p>O pedido <strong>{{ $dados['pedido'] }}</strong> foi concluído. Para ver os detalhes, basta acessar a aba meus pedidos ou clicar no botão abaixo. Obrigado por comprar na Web Farma!</p>
                        @else
                            <p>O pedido <strong>{{ $dados['pedido'] }}</strong> foi parcialmente concluído. A farmácia não pôde entregar todos os produtos, mas não desanime. É só fazer uma nova compra em outra farmácia! Para ver os detalhes do pedido, basta acessar a aba meus pedidos ou clicar no botão abaixo. Obrigado por comprar na Web Farma!</p>
                        @endif
                        
                        <br>                                                   
                        <a href="http://localhost:8000/meus_pedidos"><button class="btn-lg btn-info botao-login mb-5 mt-4">Ver meus pedidos</button></a>   
                        
                    </div>
                </div>
            </div>

        </div>
    </div>

</body>