@extends("cliente/layoutCliente")

@section("titulo", "Checkout")

@section("conteudo")   


<h1>Pagamento</h1>
    <div class="row d-flex justify-content-center pb-5 mt-5">
        <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
        @include("_mensagens")  
        <div class="col-sm-5 col-md-5 ml-1">

            <div class="pt-2">
                <p>Selecione a forma de pagamento para continuar</p>
                <form action="{{ route('finalizar_pedido') }}" method="post">
                    @csrf
                    @foreach($formas_pagamento as $fp)                        
                        <div class="pb-3">
                            <div class="d-flex flex-row align-content-center">
                                <div class="pt-2 pr-2"><input type="radio" name="pagamento" value="{{ $fp->id }}"></div>
                                <div class="rounded border d-flex w-100 px-2">
                                    <div class="pt-2 pb-2">
                                        <span class="{{ $fp->classe_estilo }}"><i class="{{ $fp->classe_icone }}"></i></span>
                                        <span class="ml-3">{{ $fp->forma_pagamento }}</span>
                                    </div>                                    
                                </div>
                            </div>                            
                        </div>
                    @endforeach
                    <button type="submit" class="btn btn-success btn-block">Finalizar pedido</button>
                </form>
                
                <!--<form class="pb-3">
                    <div class="d-flex flex-row align-content-center">
                        <div class="pt-2 pr-2"><input type="radio" name="radio1" id="r1" checked></div>
                        <div class="rounded border d-flex w-100 px-2">
                            <div class="pt-2 pb-2">
                                <span class="text-success"><i class="far fa-money-bill-alt"></i></span>
                                <span class="ml-3">Dinheiro</span>
                            </div>
                            
                        </div>
                    </div>
                </form>
                <form class="pb-3">
                    <div class="d-flex flex-row align-content-center">
                        <div class="pt-2 pr-2"><input type="radio" name="radio1" id="r1" checked></div>
                        <div class="rounded border d-flex w-100 px-2">
                            <div class="pt-2 pb-2">
                                <i class="fas fa-credit-card"></i> 
                                <span class="ml-3">Cartão de crédito</span>
                            </div>                            
                        </div>
                    </div>
                </form>
                <form class="pb-3">
                    <div class="d-flex flex-row w-100">
                        <div class="pt-2 pr-2"><input type="radio" name="radio2" id="r2"></div>
                        <div class="rounded border d-flex w-100 px-2">
                            <div class="pt-2 pb-2">                                
                                <i class="far fa-credit-card"></i>
                                <span class="ml-3">Cartão de débito</span>
                            </div>                            
                        </div>
                    </div>
                </form>
                <div> <input type="button" value="Finalizar pedido" class="btn btn-success btn-block"> </div> -->
            </div>
        </div>
        <div class="col-sm-3 col-md-4 offset-md-1 mt-5" style="width: 700px">
            <div class="bg-light rounded d-flex flex-column">
                <div class="p-2 ml-3">
                    <h4>Informações do pedido</h4>
                </div>
                <div class="p-2 d-flex">
                    <div class="col-8">Total do pedido</div>
                    <div class="ml-auto">R$ {{ number_format($subtotal, 2, ',', '.') }}</div>
                </div>
                <div class="p-2 d-flex">
                    <div class="col-8">Entrega</div>
                    <div class="ml-auto">R$ {{ number_format($totalEntrega, 2, ',', '.') }}</div>
                </div>
                <div class="p-2 d-flex">
                    <div class="col-8">Taxa</div>
                    <div class="ml-auto">R$ {{ number_format(0, 2, ',', '.') }} <br></div>
                </div>
                <div class="border-top px-4 mx-3"></div>
                <div class="p-2 d-flex pt-3">
                    <div class="col-8"><b>Total</b></div>
                    <div class="ml-auto"><b class="green">R$ {{ number_format($subtotal + $totalEntrega, 2, ',', '.') }}</b></div>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection