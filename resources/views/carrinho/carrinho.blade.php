@extends("cliente/layoutCliente")

@section("titulo", "Carrinho")

@section("conteudo")   

   <h1 class="mb-5">Carrinho</h1>
   
   

   <div class="container">
        <div>
           <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
            @include("_mensagens")    
            

            @if (!empty($itens))

                <h2>Seu carrinho possui {{ count($itens) }} produto(s)</h2>
                <hr>
                <!-- Produtos do carrinho -->
                @foreach ($itens as $indice => $produto)                
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-2">
                                <a href="{{ route('nome_produto', ['nomeprod' => $produto[0]->slug]) }}"><img src="{{ asset($produto[0]->imagens) }}" alt="item" style="max-height: 75px"></a>                    
                            </div>

                            <div class="col-10">
                                <div class="row">
                                    <div class="col-12 mt-2 descricao-produto-detalhe-pedido">
                                        <a href="{{ route('nome_produto', ['nomeprod' => $produto[0]->slug]) }}">{{ $produto[0]->produto }}</a>
                                    </div>
                                    <div class="col-12 mt-2 descricao-produto-detalhe-pedido">
                                        Vendido e entregue por: {{ $produto[0]->nome_farmacia }}
                                    </div>
                                </div>   
                            </div>                   
                        </div>                            
                    </div>

                    <div class="col-6">
                        <div class="row">
                            <div class="col-2 mt-2">           
                                <form action="{{ route('remover_carrinho') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="indice" value="{{ $indice }}">
                                    <button type="submit" class="btn-sm btn-danger"><i class="fas fa-times"></i></button>
                                </form>
                            </div>
                            <div class="col-5 mt-2">           
                                <form action="{{ route('atualizar_carrinho') }}" method="post">
                                    @csrf
                                    <input class="input col-form-label col-5 border rounded pt-2" name="quantidade" type="number" value="{{ $produto[0]->quantidade }}" min="1">
                                    <input type="hidden" name="indice" value="{{ $indice }}">
                                    <button style="height: 40px" type="submit" class="btn-sm btn-primary">Ok</a>
                                </form>
                            </div>            
                            <div class="col-5 mt-2">           
                                <h5>R$ {{ number_format($produto[0]->valor * $produto[0]->quantidade, 2, ',', '.') }}</h5>
                            </div>
                        </div>     
                    </div> 
                </div> 
                <hr>
                @endforeach


                <!-- Total do pedido -->
                <div class="total-carrinho">
                    <div class="row">
                        <div class="col-7">                            
                                Detalhes do pedido
                                <br>
                                <div class="mt-2">
                                    <strong>Endereço de entrega:</strong>
                                    <br>
                                    {{ $endereco->logradouro }}
                                    <br>
                                    {{ $endereco->numero }}, {{ $endereco->complemento }}
                                    <br>
                                    {{ $endereco->bairro }} - {{ $endereco->cidade }}

                                </div>
                        </div>

                        <div class="col-2">
                            <div class="col-12 ml-auto">Subtotal</div>
                            <div class="col-12 ml-auto">Entrega</div>
                            <div class="col-12 ml-auto">Taxa</div>
                            <div class="col-12 ml-auto">
                                <h5 class="font-weight-bold mt-3">Total</h5>
                            </div>
                        </div>

                        <div class="col-3">
                            <div>
                                <div class="col-12 ml-auto">R$ {{ number_format($subtotal, 2, ',', '.') }} </div>
                                <div class="col-12 ml-auto"> R$ {{ number_format($totalEntrega, 2, ',', '.') }} </div>
                                <div class="col-12 ml-auto"> R$ {{ number_format(0, 2, ',', '.') }} </div>
                                <div class="col-12 ml-auto">
                                    <h5 class="font-weight-bold mt-3">R$ {{ number_format($subtotal + $totalEntrega, 2, ',', '.') }}</h5>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div> 


                <div class="row mt-3">
                    <div class="col-6">
                        <a href="{{ route('index') }}"><button class="btn btn-primary">Continuar comprando</button></a>
                    </div>
                    
                    <div class="ml-auto">
                        <a href="{{ route('checkout') }}"><button class="btn btn-success">Ir para pagamento</button></a>
                    </div>
                    
                </div>
                

            @else

                <h3>O seu carrinho está vazio</h3>
                <div class="mt-3">
                    <a href="{{ route('index') }}"><button class="btn btn-primary">Continuar comprando</button></a>
                </div>
                <div class="spacer"></div>

            @endif

        </div>

    </div> <!-- end cart-section -->
    

@endsection

@section("js-extras") 
    <script type="text/javascript" src="{{ asset('js/carrinho.js') }}"></script>
@endsection