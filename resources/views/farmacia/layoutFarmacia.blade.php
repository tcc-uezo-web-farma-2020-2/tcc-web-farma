<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Web Farma - @yield("titulo")</title>

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">

    <link rel="stylesheet" href="{{asset('css/estilo.css')}}" crossorigin="anonymous">

	<!--<link rel="stylesheet" type="text/css" href="/css/style.css"> --><!-- Arquivo CSS está no diretório público e está sendo recuperado de lá pelo servidor HTTP -->

</head>

<body>

    <nav class="sticky-top navbar navbar-info navbar-expand-md bg-info pl-5 pr-5 mb-5 barra-navegacao">

    
        
        @if(Auth::user())
            <a href= "{{ route('index_farmacia') }}" class="navbar-brand text-white">Web farma</a>
            <div class="col-1">
                <div class="navbar-nav">
                    <a class="nav-link text-white" href="{{ route('index_farmacia') }}">Home</a>                          
                </div>
            </div>

            <div class="col-6">
                <div class="navbar-nav">
                    <h4 class="text-white">Painel - {{ session('farmacia', [])[0]->nome }}</h4>
                </div>
            </div>
        
            <div class="col-5">
                <div class="navbar-nav">                    
                    <a class="nav-link text-white" href="{{ route('minha_conta_farmacia') }}">Minha conta</a>
                    <a class="nav-link text-white" href="{{ route('meus_pedidos') }}">Meus pedidos</a>                    
                    <a class="nav-link text-white" href="{{ route('estoque') }}">Cadastro de produtos</a>
                    <a class="nav-link text-white" href="{{ route('relatorios') }}">Relatórios</a>
                    <a class="nav-link text-white" href="{{ route('sair') }}">Sair</a>
                </div>
            </div>

            
            <!--<div class="col-3">
                <form action="{{ route('pesquisa') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-10">                            
                            <input name="pesquisa" type="text" class="form-control" placeholder="O que está procurando?">                            
                        </div>
                        <div class="col-2">                            
                            <button type="submit" class="btn btn-primary botao-pesquisa"><i class="fas fa-search"></i></button>                            
                        </div>
                    </div>
                </form>
            </div> --->
            
           
        @else
            <div class="col-12">
                <div class="navbar-nav">
                    <div class="mx-auto">
                        <h4 class="text-white">Web Farma - Farmácia</h4>                          
                    </div>
                </div>
            </div>
        @endif

        
        
    </nav>

    <!--<header class="sticky-top">
        <div class="container">
            <div class="row align-items-center">
                <div id="pesquisa" class="col-12 col-lg-5 d-none d-lg-block">
                    <div class="input-group pb-3">
                        <input name="pesquisa" type="text" class="form-control" placeholder="O que está procurando?">
                    </div>
                    <div class="input-group-append">
                        <buton class="btn btn-primary"><i class="fas fa-search"></i></buton>
                    </div>
                </div>
            </div>
        </div>
    </header> -->

    <div class="container">
        <div class="row">  
        <!-- Conteúdo a ser inserido pelas outras views -->
        @yield("conteudo")
        </div> 
    </div>

    <script type="text/javascript" src="{{ asset('js/produto.js') }}"></script>

    @yield("js-extras")

</body>

</html>