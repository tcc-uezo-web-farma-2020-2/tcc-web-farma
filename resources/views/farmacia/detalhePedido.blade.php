@extends("farmacia/layoutFarmacia")

@section("titulo", "Detalhes do pedido")

@section("conteudo") 

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <div class="container">
        <h1 class="mb-5">Detalhes do pedido</h1>
        <div class="row">
            <div class="col-8">
                <div class="col-12">                            
                    <h4 class="font-weight-bold mt-2">Pedido {{$pedido[0]->id_pedido}}</h4>
                </div>
                <div class="col-12">              
                    <strong>Data do pedido: </strong>
                    {{ date("d/m/Y H:i", strtotime($pedido[0]->data_pedido)) }}
                </div>
                <div class="col-12">
                    <strong>Status: </strong> 
                    <span class="text-{{ $pedido[0]->classe_estilo }}">{{ $pedido[0]->status }}</span>
                </div>      
                <div class="col-12">
                    <strong>Cliente: </strong>
                    {{ $pedido[0]->cliente }}
                </div>
                @if($pedido[0]->tempo_confirmacao_esgotado == 1)
                    <div class="col-12">
                        <strong>Endereço de entrega: </strong> 
                        {{ $pedido[0]->logradouro }}, {{ $pedido[0]->numero }}, @if(!empty($pedido[0]->complemento)) {{ $pedido[0]->complemento }}, @endif {{ $pedido[0]->bairro }}, {{ $pedido[0]->cidade }} - {{ $pedido[0]->estado }}
                    </div>     
                    <div class="col-12 mb-5">
                        @if($pedido[0]->id_status == 1)
                            <span class="text-danger">O pedido já está a mais de 10 minutos sem confirmação, o cliente poderá cancelar a qualquer momento</span>
                        @else
                            <span class="text-danger">O pedido já está a mais de 2 horas sem atualização, o cliente poderá cancelar a qualquer momento</span>
                        @endif
                    </div>
                @else
                    <div class="col-12 mb-5">
                        <strong>Endereço de entrega: </strong> 
                            {{ $pedido[0]->logradouro }}, {{ $pedido[0]->numero }}, @if(!empty($pedido[0]->complemento)) {{ $pedido[0]->complemento }}, @endif {{ $pedido[0]->bairro }}, {{ $pedido[0]->cidade }} - {{ $pedido[0]->estado }}
                    </div> 
                @endif
            </div>
            <div class="col-4 mt-5">                
                @if($pedido[0]->id_status == 1)
                    <a href="{{ route('confirmar_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-success mr-2">Confirmar tudo</button></a>                        
                @elseif($pedido[0]->id_status == 2 || $pedido[0]->id_status == 3)
                    <a href="{{ route('entregar_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-success mr-2">Entregar pedido</button></a>                        
                @elseif($pedido[0]->id_status == 4)
                    <a href="{{ route('concluir_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-success mr-2">Concluir pedido</button></a>                        
                @endif
            </div>
        </div>

                            
            
        </div>

        @if(!empty($pedido[0]))
            <div class="col-12">
                <div class="row">                     
                    <div class="col-5 mt-4">
                        <h4 class="font-weight-bold">Produto</h4>
                    </div> 
                    <div class="col-2 mt-4" style="margin-left: -10px">           
                        <h4 class="font-weight-bold">Valor unitário</h4>
                    </div>
                    <div class="col-1 mt-4">           
                        <h4 class="font-weight-bold"> Qtd.</h4>
                    </div>            
                    <div class="col-2 mt-4">           
                        <h4 class="font-weight-bold">Subtotal</h4>
                    </div> 
                    <div class="col-2 mt-4" style="margin-left: -20px">           
                        <h4 class="font-weight-bold">Ação/Status</h4>
                    </div>
                </div>
                
                <hr>
                @foreach ($produto as $prod)            
                <div class="row">
                    <div class="col-1">
                        <a href="{{ route('nome_produto', ['nomeprod' => $prod->slug]) }}"><img src="{{ asset($prod->imagens) }}" alt="item" style="max-height: 75px"></a>                    
                    </div>
                    <div class="col-4 mt-3 descricao-produto-detalhe-pedido" style="line-height: 20px; height: 40px;">
                        <a href="{{ route('nome_produto', ['nomeprod' => $prod->slug]) }}">{{ $prod->produto }}</a>                        
                    </div> 
                    <div class="col-2 mt-4">           
                        R$ {{ number_format($prod->valor, 2, ',', '.') }}
                    </div>
                    <div class="col-1 mt-4">           
                        x {{ $prod->quantidade }}
                    </div>            
                    <div class="col-2 mt-4">           
                        <h5 class="font-weight-bold">R$ {{ number_format($prod->valor * $prod->quantidade, 2, ',', '.') }}</h5>
                    </div>   
                    @if($prod->confirmado === null)
                        <div class="col-1 mt-4">                    
                            <a href="{{ route('confirmar_produto_pedido', ['id_pedido' => $pedido[0]->id_pedido, 'slug' => $prod->slug]) }}"><button class="btn btn-success" style="margin-left: -40px">Confirmar</button></a>
                        </div>        
                        <div class="col-1 mt-4">                    
                            <a href="{{ route('cancelar_produto_pedido', ['id_pedido' => $pedido[0]->id_pedido, 'slug' => $prod->slug]) }}"><button class="btn btn-danger" style="margin-left: -20px">Cancelar</button></a>
                        </div>
                    @else
                        <div class="col-2 mt-4" class="descricao-status-detalhe-pedido">
                            @if($prod->confirmado == 1)
                                <span class="text-success">Confirmado</span>
                            @else
                                <span class="text-danger">Cancelado</span>                        
                            @endif
                        </div>
                    @endif
                </div> 
                <hr>
                @endforeach

            </div>

            <div class="col-12 mt-5">
                <div class="bg-light rounded d-flex flex-column">                
                    <div class="p-2 d-flex">
                        <div class="col-8">Total do pedido</div>
                        <div class="ml-auto">R$ {{ number_format($pedido[0]->valor_total, 2, ',', '.') }}</div>
                    </div>
                    <div class="p-2 d-flex">
                        <div class="col-8">Entrega</div>
                        <div class="ml-auto">R$ {{ number_format($pedido[0]->valor_entrega, 2, ',', '.') }}</div>
                    </div>
                    <div class="p-2 d-flex">
                        <div class="col-8">Taxa</div>
                        <div class="ml-auto">R$ {{ number_format(0, 2, ',', '.') }} <br></div>
                    </div>
                    @if($pedido[0]->valor_cancelado > 0)
                        <div class="p-2 d-flex">
                            <div class="col-8 text-danger">Valor cancelado</div>
                            <div class="ml-auto text-danger">R$ {{ number_format($pedido[0]->valor_cancelado, 2, ',', '.') }} <br></div>
                        </div>
                    @endif
                    <div class="border-top px-4 mx-3"></div>
                    <div class="p-2 d-flex pt-3">
                        <div class="col-8"><h4 class="font-weight-bold">Total</h4></div>
                        <div class="ml-auto"><h4 class="font-weight-bold">R$ {{ number_format($pedido[0]->valor_total + $pedido[0]->valor_entrega - $pedido[0]->valor_cancelado, 2, ',', '.') }}</h4></div>
                    </div>
                </div>
            </div>

            
        
            <div class="col-12 mt-5 mb-5">
                <div class="row">
                    <div class="col-8">
                        <a href="/meus_pedidos"><button class="btn btn-primary">Voltar</button></a>
                    </div>
                
                    <div class="ml-auto">
                        @if($pedido[0]->id_status == 1)
                            <a href="{{ route('confirmar_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-success mr-2">Confirmar tudo</button></a>
                            <a href="{{ route('cancelar_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-danger">Cancelar pedido</button></a>
                        @elseif($pedido[0]->id_status == 2 || $pedido[0]->id_status == 3)
                            <a href="{{ route('entregar_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-success mr-2">Entregar pedido</button></a>
                            <a href="{{ route('cancelar_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-danger">Cancelar pedido</button></a>
                        @elseif($pedido[0]->id_status == 4)
                            <a href="{{ route('concluir_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-success mr-2">Concluir pedido</button></a>
                            <a href="{{ route('cancelar_pedido', ['id_pedido' => $pedido[0]->id_pedido]) }}"><button class="btn btn-danger">Cancelar pedido</button></a>
                        @endif
                        
                    </div>
                </div>
            </div>
                
                
            </div> 
    

            
            
        @else
            Nenhum pedido encontrado
        @endif
    </div>
    
@endsection