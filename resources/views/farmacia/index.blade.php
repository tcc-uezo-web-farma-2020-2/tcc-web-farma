@extends("farmacia/layoutFarmacia")

@section("titulo", "Página inicial")

@section("conteudo")    

    <h1 class="mb-4">Página inicial</h1>

    <div class="container">

        <div class="status-atual border rounded">
            <div class="row">
                <div class="col-10">
                    <div class="col-12">
                        <h5>Status atual da farmácia</h5>                        
                    </div>

                    <div class="col-12">              
                        @if($aberto == 1)                   
                            <span class="text-success"><strong>Aberta. </strong>Disponível para receber novos pedidos</span>
                            <br>
                            <small class="mt-4">Obs: Ao clicar em "Fechar" todos os pedidos aguardando confirmação serão automaticamente cancelados</small>
                        @else
                            <span class="text-danger"><strong>Fechada. </strong>Seus produtos não aparecerão para os clientes</span> 
                        @endif
                    </div>
                </div>

                <div class="col-2">
                    @if($aberto == 1)
                        <a href="{{ route('abrir_fechar') }}"><buton class="btn btn-danger mt-3 ml-2">Fechar</buton></a>
                    @else
                        <a href="{{ route('abrir_fechar') }}"><buton class="btn btn-success mt-3 ml-4">Abrir</buton></a>          
                    @endif
                </div>
                
            </div>
        </div>

        <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
        @include("_mensagens")

        <h2 class="mb-4">Pedidos</h3>
        <div class="row">
            <div class="col-6">
                <div class="border rounded farmacia-index-box mb-5">
                    <div class="w-100 titulo-box">
                        <div class="bg-secondary">
                            <div class="d-flex justify-content-center">
                                <h4 class="text-white font-weight-bold">Em andamento</h4>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-center">
                        <h4 class="mt-4">Existe(m) {{$andamento['totalAndamento']->qtd}} pedido(s) em andamento</h4>
                    </div>

                    <div class="d-flex mt-3">
                        <div class="col-3">
                            <div class="col-12 status-pedidos-box">
                                <strong>Aguardando confirmação</strong>
                            </div>
                            <div class="col-12">
                                {{$andamento['ac']->qtd}}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="col-12 status-pedidos-box">
                                <strong>Confirmado</strong>
                            </div>
                            <div class="col-12">
                                {{$andamento['co']->qtd}}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="col-12 status-pedidos-box">
                                <strong>Parcialmente confirmado</strong>
                            </div>
                            <div class="col-12">
                                {{$andamento['pc']->qtd}}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="col-12 status-pedidos-box">
                                <strong>Em entrega</strong>
                            </div>
                            <div class="col-12">
                                {{$andamento['ee']->qtd}}
                            </div>
                        </div>
                    </div>

                    <div class="mt-4 ml-5 mr-5">
                        <a href="{{ route('meus_pedidos') }}"><button class="btn btn-success botao-box">Ver pedidos</button></a>
                    </div>
                </div>
            </div>


            <div class="col-6">
                <div class="border rounded farmacia-index-box mb-5">
                    <div class="w-100 titulo-box">
                        <div class="bg-secondary">
                            <div class="d-flex justify-content-center">
                                <h4 class="text-white font-weight-bold">Concluídos</h4>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-center">
                        <h4 class="mt-4">Total de pedidos concluídos</h4>
                    </div>

                    <div class="d-flex mt-3">
                        <div class="col-3">
                            <div class="col-12 status-pedidos-box">
                                <strong>Hoje</strong>
                            </div>
                            <div class="col-12">
                                {{ $concluido['hoje']->qtd }}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="col-12 status-pedidos-box">
                                <strong>Última semana</strong>
                            </div>
                            <div class="col-12">
                                {{ $concluido['semana']->qtd }}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="col-12 status-pedidos-box">
                                <strong>Último mês</strong>
                            </div>
                            <div class="col-12">
                                {{ $concluido['mes']->qtd }}
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="col-12 status-pedidos-box">
                                <strong>Último semestre</strong>
                            </div>
                            <div class="col-12">
                                {{ $concluido['semestre']->qtd }}
                            </div>
                        </div>
                    </div>

                    <div class="mt-4 ml-5 mr-5">
                        <a href="{{ route('relatorios') }}"><button class="btn btn-primary botao-box">Ver relatórios</button></a>
                    </div>
                </div>
            </div>
        </div>



        <h2 class="mb-4 mt-5">Produtos</h3>
        <div class="row">
            <div class="col-6">
                @if(!empty($poucoEstoque[0]))
                    <div class="border rounded farmacia-index-box mb-5">
                        <div class="w-100 titulo-box">
                            <div class="bg-secondary">
                                <div class="d-flex justify-content-center">
                                    <h4 class="text-white font-weight-bold">Com pouco estoque</h4>
                                </div>
                            </div>
                        </div>                        

                        <div class="d-flex justify-content-center">
                            <h4 class="mt-4">Seus 10 produtos com menos estoque</h4>
                        </div>

                        <div class="d-flex mt-3">
                            <div class="col-9">
                                <div class="col-12 status-pedidos-box">
                                    <strong>Produto</strong>
                                </div>                            
                            </div>
                            <div class="col-3">
                                <div class="col-12 status-pedidos-box">
                                    <strong>Estoque</strong>
                                </div>                            
                            </div>                        
                        </div>

                        
                        <hr>
                        @foreach ($poucoEstoque as $prod)                        
                        <div class="d-flex">
                            <div class="col-2">
                                <img src="{{ asset($prod->imagens) }}" alt="item" style="max-height: 75px">
                            </div>
                            <div class="col-8 mt-2 descricao-produto-detalhe-pedido">
                                <div class="col-12"> 
                                    {{ $prod->produto }}
                                </div>
                                <div class="col-12"> 
                                    <small class="text-secondary">Cod barras: {{ $prod->cod_barras }}</small>
                                </div>
                            </div> 
                            <div class="col-2 mt-2">           
                                {{ $prod->estoque }}
                            </div>                              
                        </div> 
                        
                            
                        <hr>
                        @endforeach

                        <div class="mt-5 ml-5 mr-5">
                            <a href="{{ route('estoque') }}"><button class="btn btn-primary botao-box">Ver meus produtos</button></a>
                        </div>

                    </div>
                    
                @else
                    <div class="border rounded farmacia-index-box-vazio mb-5">
                        <div class="w-100 titulo-box">
                            <div class="bg-secondary">
                                <div class="d-flex justify-content-center">
                                    <h4 class="text-white font-weight-bold">Com pouco estoque</h4>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="col-12 ml-4 mt-4">
                                <h4>Você ainda não tem produtos cadastrados</h4>
                            </div>
                        </div>

                        <div class="mt-4 ml-5 mr-5">
                            <a href="{{ route('estoque') }}"><button class="btn btn-primary botao-box">Cadastrar produtos</button></a>
                        </div>
                    </div>
                @endif                   
                
            </div>


            <div class="col-6">
                @if(!empty($maisVendidos[0]))
                    <div class="border rounded farmacia-index-box mb-5">
                        <div class="w-100 titulo-box">
                            <div class="bg-secondary">
                                <div class="d-flex justify-content-center">
                                    <h4 class="text-white font-weight-bold">Mais vendidos</h4>
                                </div>
                            </div>
                        </div>                

                        <div class="d-flex justify-content-center">
                            <h4 class="mt-4">Seus 10 produtos mais vendidos</h4>
                        </div>

                        <div class="d-flex mt-3">
                            <div class="col-9">
                                <div class="col-12 status-pedidos-box">
                                    <strong>Produto</strong>
                                </div>                            
                            </div>
                            <div class="col-3">
                                <div class="col-12 status-pedidos-box">
                                    <strong>Vendidos</strong>
                                </div>                            
                            </div>                        
                        </div>
                        
                        <hr>
                        @foreach ($maisVendidos as $prod)                        
                        <div class="d-flex">
                            <div class="col-2">
                                <img src="{{ asset($prod->imagens) }}" alt="item" style="max-height: 75px">
                            </div>
                            <div class="col-8 mt-2 descricao-produto-detalhe-pedido">
                                <div class="col-12"> 
                                    {{ $prod->produto }}
                                </div>
                                <div class="col-12"> 
                                    <small class="text-secondary">Cod barras: {{ $prod->cod_barras }}</small>
                                </div>
                            </div> 
                            <div class="col-2 mt-2">           
                                {{ $prod->qtd }}
                            </div>                              
                        </div> 
                        
                            
                        <hr>
                        @endforeach

                        <div class="mt-5 ml-5 mr-5">
                            <a href="{{ route('relatorios') }}"><button class="btn btn-primary botao-box">Ver relatórios</button></a>
                        </div>
                    </div>
                @else
                    <div class="border rounded farmacia-index-box-vazio mb-5">
                        <div class="w-100 titulo-box">
                            <div class="bg-secondary">
                                <div class="d-flex justify-content-center">
                                    <h4 class="text-white font-weight-bold">Mais vendidos</h4>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="col-12 ml-5 mt-4">
                                <h4>Você ainda não fez nenhuma venda</h4>
                            </div>
                        </div>

                        <div class="mt-4 ml-5 mr-5">
                            <a href="{{ route('relatorios') }}"><button class="btn btn-primary botao-box">Ver outros relatórios</button></a>
                        </div>
                    </div>
                @endif
                    
                
            </div>
        </div>
    </div>
    

@endsection