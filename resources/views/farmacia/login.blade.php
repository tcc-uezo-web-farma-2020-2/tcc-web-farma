@extends("farmacia/layoutFarmacia")

@section("titulo", "Login farmácia")


@section("conteudo") 

<div id="login" class="col-12">
    <h3 class="text-center text-dark pt-5">Entre com seu e-mail e senha</h3>

    <div id="login-row" class="row justify-content-center align-items-center">
        <div id="login-column" class="col-6 justify-content-center align-items-center">
            <div id="login-box" class="col-md-12">
                <form id="login-form" class="form" action="{{route('autenticar')}}" method="post">
                    @csrf
                    <h3 class="text-center text-info">Login</h3>
                    
                    <div class="form-group">
                        <label for="email" class="text-secondary">E-mail:</label><br>
                        <input type="email" name="email" id="email" class="form-control">
                        @error('email')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="senha" class="text-secondary">Senha:</label><br>
                        <input type="password" name="password" id="senha" class="form-control">
                    </div>
                    <div class="text-left esqueci-senha">    
                        <a href="{{route('esqueci_senha')}}" class="text-info">Esqueci a senha</a>
                    </div>
                    
                    <div class="text-right fazer-cadastro">
                        <a href="{{route('cadastro_farmacia')}}" class="text-info">Fazer cadastro</a>
                    </div>

                    
                    <button type="submit" class="btn-lg btn-info botao-login mb-5 mt-4">Entrar</button>
                    
                </form>
            </div>
        </div>
    </div>

</div>

@endsection