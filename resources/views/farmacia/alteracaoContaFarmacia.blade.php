@extends("farmacia/layoutFarmacia")

@section("titulo", "Alterar dados")

@section("conteudo")    

    <h1 class="mb-4">Alteração de dados - Farmácia</h1>

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <form method="post" action="{{ route('alterar_cadastro_farmacia') }}">
        <!--Cross site request forgery. Token enviado junto ao formulário, com um tempo de expiração, para garantir que a mesma pessoa que acessou o formulário é a que está enviando -->
        @csrf        
        <h3 class="mt-5 ml-3">Dados da farmácia</h3>        
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <div class="form-group">Nome:
                        <input name="nome" type="text" id="nome" size="60" class="form-control w-75" value="{{ $farmacia->nome }}"/>
                        @error('nome')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>               

                <div class="col-8">
                    <div class="form-group">CNPJ:
                        <input name="cnpj" type="text" id="cnpj" class="form-control w-25" readonly onblur="validaCpf(this.value)" value="{{ $farmacia->cnpj }}"/>
                        @error('cnpj')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Raio máximo de entrega (Km):
                        <input name="raio_entrega" type="text" id="raio_entrega"  class="form-control w-50" placeholder="Máximo recomendado: 3" value="{{ $farmacia->raio_entrega }}"/>
                        @error('raio_entrega')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <h3 class="mt-5 ml-3">Valores de entrega</h3>
        <p class="ml-3"><small class="text-secondary">Caso a entrega seja grátis independente da distância, deixar os campos em branco</small></p>
        <div class="col-12">     
            <div class="row">
                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - De:                        
                        <input name="entrega_de_1" type="text" id="entrega_de_1" class="form-control w-50" placeholder="Ex: 0" value="{{ $entregas[0]->raio_km_de }}"/>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - Até:
                        <input name="entrega_ate_1" type="text" id="entrega_ate_1"  class="form-control w-50" placeholder="Ex: 1,5" value="{{ $entregas[0]->raio_km_ate }}"/>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Valor (R$):
                        <input name="entrega_valor_1" type="text" id="entrega_valor_1"  class="form-control w-50" placeholder="Ex: 0,00" value="{{ $entregas[0]->valor }}"/>
                    </div>
                </div>               
                
                

                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - De:
                        @if (isset($entregas[1]->valor))                            
                            <input name="entrega_de_2" type="text" id="entrega_de_2" class="form-control w-50" placeholder="Ex: 1,5" value="{{ $entregas[1]->raio_km_de }}"/>
                        @else
                            <input name="entrega_de_2" type="text" id="entrega_de_2" class="form-control w-50" placeholder="Ex: 1,5"/>
                        @endif
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - Até:
                        @if (isset($entregas[1]->valor))
                            <input name="entrega_ate_2" type="text" id="entrega_ate_2" class="form-control w-50" placeholder="Ex: 2,5" value="{{ $entregas[1]->raio_km_ate }}"/>
                        @else
                            <input name="entrega_ate_2" type="text" id="entrega_ate_2" class="form-control w-50" placeholder="Ex: 2,5"/>
                        @endif
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Valor (R$):
                        @if (isset($entregas[1]->valor))
                            <input name="entrega_valor_2" type="text" id="entrega_valor_2" class="form-control w-50" placeholder="Ex: 2,00" value="{{ $entregas[1]->valor }}"/>                            
                        @else
                            <input name="entrega_valor_2" type="text" id="entrega_valor_2" class="form-control w-50" placeholder="Ex: 2,00"/>
                        @endif
                    </div>
                </div>
                



                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - De:
                        @if (isset($entregas[2]->valor))                            
                            <input name="entrega_de_3" type="text" id="entrega_de_3" class="form-control w-50" placeholder="Ex: 2,5" value="{{ $entregas[2]->raio_km_de }}"/>
                        @else
                            <input name="entrega_de_3" type="text" id="entrega_de_3" class="form-control w-50" placeholder="Ex: 2,5"/>
                        @endif
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - Até:
                        @if (isset($entregas[2]->valor))
                            <input name="entrega_ate_3" type="text" id="entrega_ate_3"class="form-control w-50" placeholder="Ex: 3" value="{{ $entregas[2]->raio_km_ate }}"/>
                        @else
                            <input name="entrega_ate_3" type="text" id="entrega_ate_3"class="form-control w-50" placeholder="Ex: 3"/>
                        @endif
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Valor (R$):
                        @if (isset($entregas[2]->valor))
                            <input name="entrega_valor_3" type="text" id="entrega_valor_3" class="form-control w-50" placeholder="Ex: 3,00" value="{{ $entregas[2]->valor }}"/>
                        @else
                            <input name="entrega_valor_3" type="text" id="entrega_valor_3" class="form-control w-50" placeholder="Ex: 3,00"/>
                        @endif
                    </div>
                </div>                
            </div>  
        </div>

        <h3 class="mt-5 ml-3">Endereço</h3>
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <div class="form-group" id="divcep">Cep:
                            <input name="cep" type="text" id="cep" maxlength="9" onblur="pesquisacep(this.value);" class="form-control w-25" value="{{ $farmacia->cep }}"/>
                            @error('cep')
                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                            @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">Logradouro:
                            <input name="logradouro" type="text" id="logradouro" class="form-control w-75" readonly value="{{ $farmacia->logradouro }}"/>
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group" id="divnumero">Numero:
                            <input name="numero" type="text" id="numero" size="60" onblur="getCoordenadas()"class="form-control w-25" value="{{ $farmacia->numero }}"/>
                            @error('numero')
                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                            @enderror
                            @error('latitude')
                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                            @enderror
                            @error('longitude')
                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                            @enderror
                    </div>
                </div>

                <div class="col-7">
                    <div class="form-group">Complemento:
                            <input name="complemento" type="text" id="complemento" size="100" class="form-control w-75" onblur="verificaNumero()" value="{{ $farmacia->complemento }}"/>
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">Bairro:
                            <input name="bairro" type="text" id="bairro" size="40" class="form-control w-50" readonly value="{{ $farmacia->bairro }}"/>
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group">Cidade:
                            <input name="cidade" type="text" id="cidade" size="40" class="form-control w-75" readonly value="{{ $farmacia->cidade }}"/>
                    </div>
                </div>

                <div class="col-7">
                    <div class="form-group">Estado:
                            <input name="estado" type="text" id="estado" size="2" class="form-control w-25" readonly value="{{ $farmacia->estado }}"/>
                    </div>
                </div>          


                <input name="latitude" type="hidden" id="latitude" size="20" value="{{ $farmacia->latitude }}"/>
                <input name="longitude" type="hidden" id="longitude" size="20" value="{{ $farmacia->longitude }}"/>
            
            </div>
        </div>
        

        <button type="sumbit" method="post" class="btn-lg btn-success mt-3 ml-3 mb-5">Salvar</button>
    </form>

@endsection

@section("js-extras")
    <script type="text/javascript" src="{{ asset('js/localizacao.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/mascara.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/cadastro.js') }}"></script>
@endsection

    
