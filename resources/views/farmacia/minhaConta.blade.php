@extends("farmacia/layoutFarmacia")

@section("titulo", "Minha conta")

@section("conteudo") 

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <h1>Meus dados</h1>
       
               
        <div class="mt-2 col-12 mb-3">
            <div class="row">
                <div class="col-8">
                    <strong>Nome:</strong>
                        {{ $usuario->nome }}                    
                </div>

                <div class="col-8 mt-3">
                    <strong>E-mail:</strong>
                        {{ $usuario->email }}                    
                </div>

                <div class="col-8 mt-3">
                    <strong>CPF:</strong>
                        {{ $usuario->cpf }}                    
                </div>

                <div class="col-6 mt-3">
                    <strong>Telefone:</strong>
                        {{ $usuario->telefone }}                    
                </div>

                <div class="col-6 mt-3">
                    <strong>Celular:</strong>
                        {{ $usuario->celular }}                    
                </div>                

                <div class="col-6 mt-3">
                    <strong>Data de nascimento:</strong>
                        {{ date("d/m/Y", strtotime($usuario->data_nascimento)) }}                   
                </div>

                <div class="col-6 mt-3">
                    <strong>Gênero:</strong>
                        {{ $usuario->genero }} 
                </div>
            </div>

            <div class="mt-5 row">
                <div class="col-2">
                    <a href="{{ route('alterar_dados') }}"><button class="btn btn-success">Alterar dados</button></a>
                </div>

                <div class="col-2">
                    <a href="{{ route('alterar_senha') }}"><button class="btn btn-success">Alterar senha</button></a>
                </div>
            </div>
        </div>


        <h1 class="mt-5">Farmácia</h1>

        <div class="mt-2 col-12 mb-3">
            <div class="row">
                <div class="col-8">
                    <strong>Nome:</strong>
                        {{$farmacia->nome}}
                </div>

                <div class="col-8 mt-3">
                    <strong>CNPJ:</strong>
                        {{$farmacia->cnpj}}
                </div>

                <div class="col-8 mt-3">
                    <strong>Raio de entrega:</strong>
                        Até {{$farmacia->raio_entrega}} km
                </div>





                <div class="col-12 mt-5">
                    <h4>Endereço</h4>                        
                </div>

                <div class="col-6 mt-3">
                    <strong>Logradouro:</strong>
                        {{$farmacia->logradouro}}
                </div>

                <div class="col-6 mt-3">
                    <strong>Número:</strong>
                        {{$farmacia->numero}}
                </div>

                <div class="col-6 mt-3">
                    <strong>Complemento:</strong>
                        {{$farmacia->complemento}}
                </div>

                <div class="col-6 mt-3">
                    <strong>Bairro:</strong>
                        {{$farmacia->bairro}}
                </div>

                <div class="col-6 mt-3">
                    <strong>Cidade:</strong>
                        {{$farmacia->cidade}}
                </div>                

                <div class="col-6 mt-3">
                    <strong>Estado:</strong>
                        {{$farmacia->estado}}
                </div> 




                <div class="col-12 mt-5">
                    <h4>Valores de entrega</h4>                        
                </div>  

                @foreach($entregas as $e)
                    <div class="col-12 mt-3">
                        <strong>De {{ $e->raio_km_de }} km a {{ $e->raio_km_ate }} km: </strong>
                            R$ {{$e->valor}}
                    </div>                     
                @endforeach             
            </div>           

            <div class="mt-5 row">
                <div class="col-2">
                    <a href="{{ route('alterar_dados_farmacia') }}"><button class="btn btn-success">Alterar dados</button></a>
                </div>                
            </div>
        </div>

@endsection

@section("js-extras")
    <script type="text/javascript" src="js/mascara.js"></script>
    <script type="text/javascript" src="js/cadastro.js"></script>
@endsection