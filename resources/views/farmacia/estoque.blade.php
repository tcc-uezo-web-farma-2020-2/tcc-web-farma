@extends("farmacia/layoutFarmacia")

@section("titulo", "Estoque")

@section("conteudo")    

    <h1 class="mb-5">Cadastro de produtos / Estoque</h1>

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    
    <form method="post" action="{{ route('alterar_estoque') }}">
        <!--Cross site request forgery. Token enviado junto ao formulário, com um tempo de expiração, para garantir que a mesma pessoa que acessou o formulário é a que está enviando -->
        @csrf                       
        <div class="row">
            <div class="col-8">
                <div class="form-row">
                    <div class="col-7">
                        <div class="form-group">Código de barras:                            
                            <input name="codbarras" type="text" id="codbarras" class="form-control w-50" onblur="getProduto()"/>                           
                            <div id="diverro"></div>                         
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">Produto:
                            <input name="produto" type="text" id="produto" class="form-control w-75" readonly/>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group" id="divcategoria">Categoria:
                            <input name="categoria" type="text" id="categoria" class="form-control w-50" readonly/>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">Quantidade/Dosagem:
                            <input name="quantidade" type="text" id="quantidade" class="form-control w-50" readonly/>
                        </div>
                    </div>

                    <div class="col-7">
                        <div class="form-group">Variação:
                            <input name="variacao" type="text" id="variacao" class="form-control w-50" readonly/>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">Composição:
                            <input name="composicao" type="text" id="composicao" class="form-control w-75" readonly/>
                        </div>
                    </div>

                    <div class="col-7">
                        <div class="form-group">Preço (R$):
                            <input name="preco" type="text" id="preco" class="form-control w-25"/>
                        </div>
                    </div>

                    <div class="col-7">
                        <div class="form-group">Quantidade em estoque:
                            <input name="estoque" type="text" id="estoque" class="form-control w-25"/>
                        </div>
                    </div>

                    <!--<div class="col-8">
                        <div class="form-group">Enviar imagem: <br>
                            <input name="foto" type="file" id="foto"/>
                        </div>
                    </div> -->
                    <input name="id_produto" type="hidden" id="id_produto"/>
                </div>
            </div>

            <div class="col-4">
                <img id="imagem" class="img-cadastro-estoque">
            </div>
        </div>

        <button type="sumbit" method="post" class="btn-lg btn-success mt-3 mb-5">Salvar</button>
    </form>

@endsection

@section("js-extras")
    <script type="text/javascript" src="{{ asset('js/estoque.js') }}"></script>
@endsection