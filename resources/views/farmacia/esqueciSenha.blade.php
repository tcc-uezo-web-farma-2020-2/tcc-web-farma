@extends("farmacia/layoutFarmacia")

@section("titulo", "Esqueci a senha")

@section("conteudo") 

<div id="recuperacao" class="col-12">

    <div id="recuperacao-row" class="row justify-content-center align-items-center">
        <div id="recuperacao-column" class="col-6 justify-content-center align-items-center">
            <div id="recuperacao-box" class="col-md-12">
                <form id="recuperacao-form" class="form" action="{{route('enviar_email_recuperacao')}}" method="post">
                    @csrf
                    <h3 class="text-center text-dark">Digite seu e-mail</h3>
                    
                    <div class="form-group">                        
                        <input type="email" name="email" id="email" class="form-control">
                    </div>
                    
                    <button type="submit" class="btn-lg btn-info botao-recuperacao mb-5 mt-4">Enviar</button>
                    
                </form>
            </div>
        </div>
    </div>

</div>

@endsection