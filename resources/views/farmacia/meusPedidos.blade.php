@extends("farmacia/layoutFarmacia")

@section("titulo", "Meus pedidos")

@section("conteudo") 

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <h1 class="mb-5">Meus pedidos</h1>

    <div class="container">
        @if(!empty($pedido[0]))
            @foreach($pedido as $p)
                <div class="col-12 border border-{{ $p->classe_estilo }} rounded pt-2 pb-4 mb-5">
                    <div class="row">
                        <div class="col-8">
                            <div class="col-12">                            
                                <h4 class="font-weight-bold mt-2">Pedido {{$p->id_pedido}}</h4>
                            </div>

                            <div class="col-12">              
                                <strong>Data do pedido: </strong>
                                {{ date("d/m/Y H:i", strtotime($p->data_pedido)) }}
                            </div>
                            <div class="col-12">
                                <strong>Valor total: </strong>
                                @if($p->status != 'Cancelado')
                                    R$ {{ number_format($p->valor_total - $p->valor_cancelado, 2, ',', '.') }}
                                @else
                                    R$ {{ number_format($p->valor_total, 2, ',', '.') }}
                                @endif
                            </div>
                            <div class="col-12">
                                <strong>Forma de pagamento: </strong>
                                    {{ $p->forma_pagamento }}
                            </div>
                            <div class="col-12">
                                <strong>Status: </strong> 
                                <span class="text-{{ $p->classe_estilo }}">{{ $p->status }}</span>
                            </div>
                            <div class="col-12">
                                <strong>Cliente: </strong>
                                {{ $p->cliente }}
                            </div>
                            <div class="col-12">
                                <strong>Endereço de entrega: </strong> 
                                {{ $p->logradouro }}, {{ $p->numero }} - {{ $p->bairro }}
                            </div>
                            @if($p->tempo_confirmacao_esgotado == 1)
                                <div class="col-12">
                                    @if($p->id_status == 1)
                                        <span class="text-danger">O pedido já está a mais de 10 minutos sem confirmação, o cliente poderá cancelar a qualquer momento</span>
                                    @else
                                        <span class="text-danger">O pedido já está a mais de 2 horas sem atualização, o cliente poderá cancelar a qualquer momento</span>
                                    @endif
                                </div>
                            @endif
                            
                        </div> 

                        <div class="col-2">                                            
                            @if($p->id_status == 4)
                                <a href="{{ route('concluir_pedido', ['id_pedido' => $p->id_pedido]) }}"><button type="submit" class="btn btn-success" style="margin-top: 70px">Concluir pedido</button></a>                              
                            @endif
                        </div>

                        <div class="col-2">                                            
                            <a href="{{ route('detalhe_pedido', ['id_pedido' => $p->id_pedido]) }}"><button type="submit" class="btn btn-info" style="margin-top: 70px">Ver detalhes</button></a>  
                        </div>
                        
                    </div>
                </div>            
                
            @endforeach


            <div class="col-12">
                <div class="navbar-nav">
                    <div class="mx-auto">
                        {{$pedido->links()}}
                    </div>
                </div>
            </div>

            <div class="mb-5 mt-4">
                <a href="/"><button class="btn btn-primary">Voltar</button></a>
            </div>

            
            
        @else
            <h3>Nenhum pedido encontrado</h3>
            <div class="mt-3">
                <a href="{{ route('index_farmacia') }}"><button class="btn btn-primary">Ir para página inicial</button></a>
            </div>
            <div class="spacer"></div>
            
        @endif
    </div>

    
    
@endsection