@extends("farmacia/layoutFarmacia")

@section("titulo", "Cadastro")

@section("conteudo")    

    <h1 class="mb-4">Farmácia - Cadastro</h1>

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <form method="post" action="{{ route('finalizar_cadastro_farmacia') }}">
        <!--Cross site request forgery. Token enviado junto ao formulário, com um tempo de expiração, para garantir que a mesma pessoa que acessou o formulário é a que está enviando -->
        @csrf        
        <h3 class="mt-5 ml-3">Dados do administrador</h3>        
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <div class="form-group">Nome:
                        <input name="nome" type="text" id="nome" size="60" class="form-control w-75"/>
                        @error('nome')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">E-mail:
                        <input name="email" type="email" id="email" value="" size="60" class="form-control w-75" placeholder="Usado para login e envio dos status dos pedidos"/>
                        @error('email')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">Senha:
                        <input name="password" type="password" id="senha" size="60" class="form-control w-50"/>
                        @error('password')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">CPF:
                        <input name="cpf" type="text" id="cpf" class="form-control w-25"/>
                        @error('cpf')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Telefone:
                        <input name="telefone" type="text" id="telefone" value="" class="form-control w-50"/>
                        @error('telefone')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Celular:
                        <input name="celular" type="text" id="celular" value="" class="form-control w-50"/>
                        @error('celular')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>                

                <div class="col-6">
                    <div class="form-group">Data de nascimento:
                        <input name="data_nascimento" type="date" id="data_nascimento" class="form-control w-50"/>
                        @error('data_nascimento')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Gênero:
                        <select class="form-control w-50" id="genero" name="genero">
                            <option selected>Selecionar</option>
                            <option value="Masculino">Masculino</option>
                            <option value="Feminino">Feminino</option>
                            <option value="Outros">Outros</option>
                        </select>
                        @error('genero')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                        <!--<input name="genero" type="text" id="genero" class="form-control w-50"/>-->
                    </div>
                </div>
            </div>
        </div>

        <h3 class="mt-5 ml-3">Dados da farmácia</h3>        
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <div class="form-group">Nome:
                        <input name="nome_farmacia" type="text" id="nome_farmacia" size="60" class="form-control w-75"/>
                        @error('nome_farmacia')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>               

                <div class="col-8">
                    <div class="form-group">CNPJ:
                        <input name="cnpj" type="text" id="cnpj" class="form-control w-25"/>
                        @error('cnpj')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Raio máximo de entrega (Km):
                        <input name="raio_entrega" type="text" id="raio_entrega" value="" class="form-control w-50" placeholder="Máximo recomendado: 3"/>
                        @error('raio_entrega')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <h3 class="mt-5 ml-3">Valores de entrega</h3>
        <p class="ml-3"><small class="text-secondary">Caso a entrega seja grátis independente da distância, deixar os campos em branco</small></p>
        <div class="col-12">     
            <div class="row">
                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - De:
                        <input name="entrega_de_1" type="text" id="entrega_de_1" class="form-control w-50" placeholder="Ex: 0"/>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - Até:
                        <input name="entrega_ate_1" type="text" id="entrega_ate_1" value="" class="form-control w-50" placeholder="Ex: 1,5"/>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Valor (R$):
                        <input name="entrega_valor_1" type="text" id="entrega_valor_1" value="" class="form-control w-50" placeholder="Ex: 0,00"/>
                    </div>
                </div>
                
                

                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - De:
                        <input name="entrega_de_2" type="text" id="entrega_de_2" class="form-control w-50"  placeholder="Ex: 1,5"/>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - Até:
                        <input name="entrega_ate_2" type="text" id="entrega_ate_2" value="" class="form-control w-50" placeholder="Ex: 2,5"/>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Valor (R$):
                        <input name="entrega_valor_2" type="text" id="entrega_valor_2" value="" class="form-control w-50" placeholder="Ex: 2,00"/>
                    </div>
                </div>



                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - De:
                        <input name="entrega_de_3" type="text" id="entrega_de_3" class="form-control w-50" placeholder="Ex: 2,5"/>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Distância da entrega (Km) - Até:
                        <input name="entrega_ate_3" type="text" id="entrega_ate_3" value="" class="form-control w-50" placeholder="Ex: 3"/>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">Valor (R$):
                        <input name="entrega_valor_3" type="text" id="entrega_valor_3" value="" class="form-control w-50" placeholder="Ex: 3,00"/>
                    </div>
                </div>
            </div>  
        </div>

        <h3 class="mt-5 ml-3">Endereço</h3>
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <div class="form-group" id="divcep">Cep:
                            <input name="cep" type="text" id="cep" value="" maxlength="9" onblur="pesquisacep(this.value);" class="form-control w-25"/>
                            @error('cep')
                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                            @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">Logradouro:
                            <input name="logradouro" type="text" id="logradouro" class="form-control w-75" readonly/>
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group" id="divnumero">Numero:
                            <input name="numero" type="text" id="numero" size="60" onblur="getCoordenadas()"class="form-control w-25"/>
                            @error('numero')
                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                            @enderror
                            @error('latitude')
                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                            @enderror       
                            @error('longitude')
                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                            @enderror                     
                    </div>
                </div>

                <div class="col-7">
                    <div class="form-group">Complemento:
                            <input name="complemento" type="text" id="complemento" size="100" class="form-control w-75" onblur="verificaNumero()"/>
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">Bairro:
                            <input name="bairro" type="text" id="bairro" size="40" class="form-control w-50" readonly/>
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group">Cidade:
                            <input name="cidade" type="text" id="cidade" size="40" class="form-control w-75" readonly/>
                    </div>
                </div>

                <div class="col-7">
                    <div class="form-group">Estado:
                            <input name="estado" type="text" id="estado" size="2" class="form-control w-25" readonly/>
                    </div>
                </div>          


                <input name="latitude" type="hidden" id="latitude" size="20" />
                <input name="longitude" type="hidden" id="longitude" size="20" />
            
            </div>
        </div>
        

        <button type="sumbit" method="post" class="btn-lg btn-success mt-3 ml-3 mb-5">Salvar</button>
    </form>

@endsection

@section("js-extras")
    <script type="text/javascript" src="{{ asset('js/localizacao.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/mascara.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/cadastro.js') }}"></script>
@endsection

    
