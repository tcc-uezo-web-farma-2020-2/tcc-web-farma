@extends("farmacia/layoutFarmacia")

@section("titulo", "Sucesso")

@section("conteudo")    

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    @if(isset($meus_pedidos) && $meus_pedidos == 1)
        <a href="{{ route('meus_pedidos') }}"><button class="btn-lg btn-success mt-3 ml-3">Ver meus pedidos</button></a>
    @else
        <a href="{{ route('login') }}"><button class="btn-lg btn-success mt-3 ml-3">Fazer login</button></a>
    @endif
    


@endsection

