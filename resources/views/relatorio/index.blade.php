@extends("farmacia/layoutFarmacia")

@section("titulo", "Farmácia")

@section("conteudo")    

    <h1 class="mb-5">Relatórios</h1>

    <div class="container">
        
    <div class="col-12 border rounded pt-2 pb-4 mb-5">
        <div class="row">
            <div class="col-10">                                           
                <h4 class="font-weight-bold mt-3">Relatório de estoque</h4>
            </div>

            <div class="ml-auto">
                <div class="col-2">                                            
                    <a href="{{ route('relatorio_estoque') }}"><button type="submit" class="btn btn-primary mt-3">Exportar</button></a>  
                </div>
            </div>
                        
        </div>
    </div>           

    <div class="col-12 border rounded pt-2 pb-4 mb-5">
        <div class="row">
            <div class="col-10">                                           
                <h4 class="font-weight-bold mt-3">Relatório de vendas - detalhado</h4>
            </div>

            <div class="ml-auto">
                <div class="col-2">                                            
                    <a href="{{ route('relatorio_vendas_detalhado') }}"><button type="submit" class="btn btn-primary mt-3">Exportar</button></a>  
                </div>
            </div>
                        
        </div>
    </div>

    <div class="col-12 border rounded pt-2 pb-4 mb-5">
        <div class="row">
            <div class="col-10">                                           
                <h4 class="font-weight-bold mt-3">Relatório de vendas canceladas - detalhado</h4>
            </div>

            <div class="ml-auto">
                <div class="col-2">                                            
                    <a href="{{ route('relatorio_vendas_canceladas_detalhado') }}"><button type="submit" class="btn btn-primary mt-3">Exportar</button></a>  
                </div>
            </div>
                        
        </div>
    </div>

    <div class="col-12 border rounded pt-2 pb-4 mb-5">
        <div class="row">
            <div class="col-10">                                           
                <h4 class="font-weight-bold mt-3">Relatório de vendas - por período</h4>
            </div>

            <div class="ml-auto">
                <div class="col-2">                                            
                    <a href="{{ route('relatorio_vendas_por_periodo') }}"><button type="submit" class="btn btn-primary mt-3">Exportar</button></a>  
                </div>
            </div>
                        
        </div>
    </div>

    <div class="col-12 border rounded pt-2 pb-4 mb-5">
        <div class="row">
            <div class="col-10">                                           
                <h4 class="font-weight-bold mt-3">Relatório de vendas - por mês</h4>
            </div>

            <div class="ml-auto">
                <div class="col-2">                                            
                    <a href="{{ route('relatorio_vendas_por_mes') }}"><button type="submit" class="btn btn-primary mt-3">Exportar</button></a>  
                </div>
            </div>
                        
        </div>
    </div> 

    <div class="col-12 border rounded pt-2 pb-4 mb-5">
        <div class="row">
            <div class="col-10">                                           
                <h4 class="font-weight-bold mt-3">Relatório de vendas - por região</h4>
            </div>

            <div class="ml-auto">
                <div class="col-2">                                            
                    <a href="{{ route('relatorio_vendas_por_regiao') }}"><button type="submit" class="btn btn-primary mt-3">Exportar</button></a>  
                </div>
            </div>
                        
        </div>
    </div>

    <div class="col-12 border rounded pt-2 pb-4 mb-5">
        <div class="row">
            <div class="col-10">                                           
                <h4 class="font-weight-bold mt-3">Relatório dos 100 produtos mais vendidos</h4>
            </div>

            <div class="ml-auto">
                <div class="col-2">                                            
                    <a href="{{ route('relatorio_produtos_mais_vendidos') }}"><button type="submit" class="btn btn-primary mt-3">Exportar</button></a>  
                </div>
            </div>
                        
        </div>
    </div>

    <div class="col-12 border rounded pt-2 pb-4 mb-5">
        <div class="row">
            <div class="col-10">                                           
                <h4 class="font-weight-bold mt-3">Relatório dos 100 produtos menos vendidos</h4>
            </div>

            <div class="ml-auto">
                <div class="col-2">                                            
                    <a href="{{ route('relatorio_produtos_menos_vendidos') }}"><button type="submit" class="btn btn-primary mt-3">Exportar</button></a>  
                </div>
            </div>
                        
        </div>
    </div>                
        

    <div class="mb-5">
        <a href="/"><button class="btn btn-primary">Voltar</button></a>
    </div>

@endsection