@extends("cliente/layoutCliente")

@section("titulo", "Cadastro")

@section("conteudo") 

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <h1>Cadastro</h1>

    
    <form method="post" action="{{ route('finalizar_cadastro') }}">
        <!--Cross site request forgery. Token enviado junto ao formulário, com um tempo de expiração, para garantir que a mesma pessoa que acessou o formulário é a que está enviando -->
        @csrf        
        <h3 class="mt-5 ml-3">Dados do cliente</h3>        
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <div class="form-group">Nome:
                        <input name="nome" type="text" id="nome" size="60" class="form-control w-75"/>
                        @error('nome')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">E-mail:
                        <input name="email" type="email" id="email" value="" size="60" class="form-control w-75"/>
                        @error('email')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">Senha:
                        <input name="password" type="password" id="senha" size="60" class="form-control w-50"/>     
                        @error('password')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror                   
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">CPF:
                        <input name="cpf" type="text" id="cpf" class="form-control w-25"/>
                        @error('cpf')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Telefone:
                        <input name="telefone" type="text" id="telefone" value="" class="form-control w-50"/>
                        @error('telefone')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Celular:
                        <input name="celular" type="text" id="celular" value="" class="form-control w-50"/>
                        @error('celular')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>                

                <div class="col-6">
                    <div class="form-group">Data de nascimento:
                        <input name="data_nascimento" type="date" id="data_nascimento" class="form-control w-50"/>
                        @error('data_nascimento')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Gênero:
                        <select class="form-control w-50" id="genero" name="genero">
                            <option selected>Selecionar</option>
                            <option value="Masculino">Masculino</option>
                            <option value="Feminino">Feminino</option>
                            <option value="Outros">Outros</option>
                        </select>
                        @error('genero')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                        <!--<input name="genero" type="text" id="genero" class="form-control w-50"/>-->
                    </div>
                </div>
            </div>
        </div>

        <h3 class="mt-5  ml-3">Endereço</h3>
        @include("cliente/_cadastroEndereco")
        

        <button type="sumbit" method="post" class="btn-lg btn-success mt-3 ml-3 mb-5">Salvar</button>
    </form>

@endsection

@section("js-extras")
    <script type="text/javascript" src="js/localizacao.js"></script>
    <script type="text/javascript" src="js/mascara.js"></script>
    <script type="text/javascript" src="js/cadastro.js"></script>
@endsection