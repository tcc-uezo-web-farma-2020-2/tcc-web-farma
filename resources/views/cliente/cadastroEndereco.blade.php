@extends("cliente/layoutCliente")
@section("conteudo")    

    <!-- <h1>Cadastro - cliente</h1> -->
    

    <!-- Inicio do formulario -->
      <form action="{{ route('cadastrar_endereco') }}" method="post">
        @csrf
        @if(isset($endereco))
        <h1 class="mb-5">Alterar endereço</h1>
        <div class="col-12">
              <div class="row">
                      <div class="col-8">
                              <div class="form-group">Descrição:
                                      <input name="descricao" type="text" placeholder="Ex: Casa, Trabalho, etc." class="form-control w-50" value="{{ $endereco->descricao }}"/>
                              </div>
                      </div>

                      <div class="col-8">
                              <div class="form-group" id="divcep">Cep:
                                      <input name="cep" type="text" id="cep" maxlength="9" onblur="pesquisacep(this.value);" class="form-control w-25" value="{{ $endereco->cep }}"/>
                                        @error('cep')
                                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                                        @enderror
                              </div>
                      </div>

                      <div class="col-8">
                              <div class="form-group">Logradouro:
                                      <input name="logradouro" type="text" id="logradouro" class="form-control w-75" readonly value="{{ $endereco->logradouro }}"/>
                              </div>
                      </div>

                      <div class="col-5">
                              <div class="form-group" id="divnumero">Numero:
                                      <input name="numero" type="text" id="numero" size="60" onblur="getCoordenadas()"class="form-control w-25" value="{{ $endereco->numero }}"/>
                                        @error('numero')
                                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                                        @enderror
                                        @error('latitude')
                                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                                        @enderror
                                        @error('longitude')
                                                <small class="text-danger font-weight-bold">{{ $message }}</small>
                                        @enderror
                              </div>
                      </div>

                      <div class="col-7">
                              <div class="form-group">Complemento:
                                      <input name="complemento" type="text" id="complemento" size="100" class="form-control w-75" onblur="verificaNumero()" value="{{ $endereco->complemento }}"/>
                              </div>
                      </div>

                      <div class="col-8">
                              <div class="form-group">Bairro:
                                      <input name="bairro" type="text" id="bairro" size="40" class="form-control w-50" readonly value="{{ $endereco->bairro }}"/>
                              </div>
                      </div>

                      <div class="col-5">
                              <div class="form-group">Cidade:
                                      <input name="cidade" type="text" id="cidade" size="40" class="form-control w-75" readonly value="{{ $endereco->cidade }}"/>
                              </div>
                      </div>

                      <div class="col-7">
                              <div class="form-group">Estado:
                                      <input name="estado" type="text" id="estado" size="2" class="form-control w-25" readonly value="{{ $endereco->estado }}"/>
                              </div>
                      </div>          


                      <input name="latitude" type="hidden" id="latitude" size="20" value="{{ $endereco->latitude }}"/>
                      <input name="longitude" type="hidden" id="longitude" size="20" value="{{ $endereco->longitude }}"/>
                      <input name="id_endereco" type="hidden" id="longitude" size="20" value="{{ $endereco->id }}"/>
              
              </div>
          </div>
        @else     
          <h1 class="mb-5">Novo endereço</h1>                      
          @include("cliente/_cadastroEndereco")
        @endif

        <button type="sumbit" class="btn btn-success mt-4 ml-3">Salvar</button>
      </form>

@endsection

@section("js-extras")
    <script type="text/javascript" src="{{ asset('js/localizacao.js') }}"></script>
@endsection