@extends("cliente/layoutCliente")

@section("titulo", "Página inicial")

@section("conteudo")  

    @include("cliente/_endereco", ['endereco' => $endereco])

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <!-- Incluindo view de página de produtos -->
    @include("produto/_produto", ['lista' => $lista])

@endsection