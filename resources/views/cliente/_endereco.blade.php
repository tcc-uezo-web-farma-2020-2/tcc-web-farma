<div class="container endereco-atual border rounded">
    <div class="row">
        <div class="col-10">
            <div class="col-12">
                <h5>Exibindo produtos para</h5>
                <h4 class="font-weight-bold">{{$endereco->descricao}}</h4>
            </div>

            <div class="col-12">              
                    {{$endereco->logradouro}}, {{$endereco->numero}}, {{$endereco->complemento}}
            </div>
            <div class="col-12">
                {{$endereco->bairro}}
            </div>
            <div class="col-12">
                {{$endereco->cidade}} - {{$endereco->estado}}
            </div>
        </div>

        <div class="col-2">
            @if(Auth::user())
            <a href="/minha_conta"><buton class="btn btn-success mt-5 ml-4">Alterar</buton></a>
            @else
                <a href="/endereco"><buton class="btn btn-success mt-5 ml-4">Alterar</buton></a>          
            @endif
        </div>
        
    </div>
</div>