@extends("cliente/layoutCliente")

@section("titulo", "Meus pedidos")

@section("conteudo") 

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <h1 class="mb-5">Meus pedidos</h1>

    <div class="container">
        @if(!empty($pedido[0]))
            @foreach($pedido as $p)
                <div class="col-12 border border-{{ $p->classe_estilo }} rounded pt-2 pb-4 mb-5">
                    <div class="row">
                        <div class="col-8">
                            <div class="col-12">                            
                                <h4 class="font-weight-bold mt-2">Pedido {{$p->id_pedido}}</h4>
                            </div>

                            <div class="col-12">              
                                <strong>Data do pedido: </strong>
                                {{ date("d/m/Y H:i", strtotime($p->data_pedido)) }}
                            </div>
                            <div class="col-12">
                                <strong>Valor total: </strong>
                                @if($p->status != 'Cancelado')
                                    R$ {{ number_format($p->valor_total - $p->valor_cancelado, 2, ',', '.') }}
                                @else
                                    R$ {{ number_format($p->valor_total, 2, ',', '.') }}
                                @endif
                            </div>
                            <div class="col-12">
                                <strong>Status: </strong> 
                                <span class="text-{{ $p->classe_estilo }}">{{ $p->status }}</span>
                            </div>
                            <div class="col-12">
                                <strong>Farmácia: </strong>
                                {{ $p->farmacia }}
                            </div>
                            <div class="col-12">
                                <strong>Endereço de entrega: </strong> 
                                {{ $p->descricao }} - {{ $p->logradouro }}, {{ $p->numero }} - {{ $p->bairro }}
                            </div>
                            @if($p->tempo_confirmacao_esgotado == 1)
                                <div class="col-12">
                                    @if($p->id_status == 1)
                                        <span class="text-danger">Como a farmácia não confirmou o pedido após 10 minutos, você pode cancelar o pedido</span>
                                    @else
                                        <span class="text-danger">Está demorando mais que o esperado, você pode cancelar o pedido, se não quiser esperar mais</span>
                                    @endif
                                </div>
                            @endif
                            
                        </div>
                        
                        <div class="col-2">                                            
                            @if($p->id_status == 4)
                                <a href="{{ route('concluir_pedido', ['id_pedido' => $p->id_pedido]) }}"><button type="submit" class="btn btn-success" style="margin-top: 70px">Recebi o pedido</button></a>  
                            @elseif($p->tempo_confirmacao_esgotado == 1)
                                <a href="{{ route('cancelar_pedido', ['id_pedido' => $p->id_pedido]) }}"><button type="submit" class="btn btn-danger ml-5" style="margin-top: 70px">Cancelar</button></a>  
                            @endif
                        </div>

                        <div class="col-2">                                            
                            <a href="{{ route('detalhe_pedido', ['id_pedido' => $p->id_pedido]) }}"><button type="submit" class="btn btn-info" style="margin-top: 70px">Ver detalhes</button></a>  
                        </div>
                        
                    </div>
                </div>            
                
            @endforeach

            <div class="col-12">
                <div class="navbar-nav">
                    <div class="mx-auto">
                        {{$pedido->links()}}
                    </div>
                </div>
            </div>

            <div class="mb-5 mt-4">
                <a href="/"><button class="btn btn-primary">Voltar</button></a>
            </div>
            
        @else
            <h3>Nenhum pedido encontrado</h3>
            <div class="mt-3">
                <a href="{{ route('index') }}"><button class="btn btn-primary">Continuar comprando</button></a>
            </div>
            <div class="spacer"></div>
            
        @endif
    </div>
    
@endsection