@extends("cliente/layoutCliente")

@section("titulo", "Alteração de senha")

@section("conteudo") 

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")


<form method="post" action="{{ route('confirmar_alteracao_senha') }}">
        <!--Cross site request forgery. Token enviado junto ao formulário, com um tempo de expiração, para garantir que a mesma pessoa que acessou o formulário é a que está enviando -->
        @csrf        
        <h3 class="mt-5 ml-3">Alteração de senha</h3>        
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <div class="form-group">Informe sua senha atual:
                        <input name="password" type="password" id="password" size="60" class="form-control w-50"/>
                        @error('password')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">Insira a nova senha:
                        <input name="new_password" type="password" id="new_password" size="60" class="form-control w-50"/>
                        @error('new_password')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">Confirme a nova senha:
                        <input name="new_password_confirmation" type="password" id="new_password_confirmation" value="" size="60" class="form-control w-50"/>
                    </div>
                </div>             

            </div>
        </div>        

        <button type="sumbit" method="post" class="btn-lg btn-success mt-3 ml-3 mb-5">Salvar</button>
    </form>

@endsection