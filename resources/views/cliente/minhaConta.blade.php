@extends("cliente/layoutCliente")

@section("titulo", "Minha conta")

@section("conteudo") 

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <h1>Meus dados</h1>
       
               
        <div class="mt-5 col-12 mb-3">
            <div class="row">
                <div class="col-8">
                    <strong>Nome:</strong>
                        {{ $usuario->nome }}                    
                </div>

                <div class="col-8 mt-3">
                    <strong>E-mail:</strong>
                        {{ $usuario->email }}                    
                </div>

                <div class="col-8 mt-3">
                    <strong>CPF:</strong>
                        {{ $usuario->cpf }}                    
                </div>

                <div class="col-6 mt-3">
                    <strong>Telefone:</strong>
                        {{ $usuario->telefone }}                    
                </div>

                <div class="col-6 mt-3">
                    <strong>Celular:</strong>
                        {{ $usuario->celular }}                    
                </div>                

                <div class="col-6 mt-3">
                    <strong>Data de nascimento:</strong>
                    {{ date("d/m/Y", strtotime($usuario->data_nascimento)) }} 
                </div>

                <div class="col-6 mt-3">
                    <strong>Gênero:</strong>
                        {{ $usuario->genero }} 
                </div>
            </div>

            <div class="mt-5 row">
                <div class="col-2">
                    <a href="alterar_dados"><button class="btn btn-success">Alterar dados</button></a>
                </div>

                <div class="col-2">
                    <a href="alterar_senha"><button class="btn btn-success">Alterar senha</button></a>
                </div>
            </div>
        </div>


        <h1 class="mt-5 mb-5">Endereços</h1>

        @foreach($endereco as $end)            
            <div class="col-12 border rounded pt-2 pb-4 mb-5">
                <div class="row">
                    <div class="col-8">
                        <div class="col-12">                            
                            <h4 class="font-weight-bold">{{$end->descricao}}</h4>
                        </div>

                        <div class="col-12">              
                            {{$end->logradouro}}, {{$end->numero}}, {{$end->complemento}}
                        </div>
                        <div class="col-12">
                            {{$end->bairro}}
                        </div>
                        <div class="col-12">
                            {{$end->cidade}} - {{$end->estado}}
                        </div>
                    </div>

                    <div class="col-2">
                        @if($end->em_uso == 0)
                            <form action="{{ route('alterar_endereco_em_uso') }}" method="post">
                            @csrf
                                <input type="hidden" name="id_endereco" value="{{$end->id}}">
                                <button type="submit" class="btn btn-primary mt-5 ml-5">Usar</button>
                            </form>        
                        @else                            
                            <buton class="btn btn-secondary  mt-5 ml-5">Em uso</buton> 
                        @endif
                    </div>

                    <div class="col-2">                        
                        @csrf                            
                            <a href="{{ route('dados_endereco', ['id_endereco' => $end->id]) }}"><button type="submit" class="btn btn-info mt-5">Alterar</button></a>
                        </form>      
                    </div>
                    
                </div>
            </div>
        @endforeach

        <div class="mb-5">
            <a href="endereco"><button class="btn btn-success">Cadastrar novo endereço</button></a>
        </div>

@endsection

@section("js-extras")
    <script type="text/javascript" src="js/mascara.js"></script>
    <script type="text/javascript" src="js/cadastro.js"></script>
@endsection