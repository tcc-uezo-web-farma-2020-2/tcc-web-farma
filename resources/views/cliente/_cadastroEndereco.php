<div class="col-12">
        <div class="row">
                <div class="col-8">
                        <div class="form-group">Descrição:
                                <input name="descricao" type="text" placeholder="Ex: Casa, Trabalho, etc." class="form-control w-50"/>
                        </div>
                </div>

                <div class="col-8">
                        <div class="form-group" id="divcep">Cep:
                                <input name="cep" type="text" id="cep" value="" maxlength="9" onblur="pesquisacep(this.value);" class="form-control w-25"/>                                
                        </div>
                </div>

                <div class="col-8">
                        <div class="form-group">Logradouro:
                                <input name="logradouro" type="text" id="logradouro" class="form-control w-75" readonly/>
                        </div>
                </div>

                <div class="col-5">
                        <div class="form-group" id="divnumero">Numero:
                                <input name="numero" type="text" id="numero" size="60" onblur="getCoordenadas()"class="form-control w-25"/>                                
                        </div>
                </div>

                <div class="col-7">
                        <div class="form-group">Complemento:
                                <input name="complemento" type="text" id="complemento" size="100" class="form-control w-75" onblur="verificaNumero()"/>
                        </div>
                </div>

                <div class="col-8">
                        <div class="form-group">Bairro:
                                <input name="bairro" type="text" id="bairro" size="40" class="form-control w-50" readonly/>
                        </div>
                </div>

                <div class="col-5">
                        <div class="form-group">Cidade:
                                <input name="cidade" type="text" id="cidade" size="40" class="form-control w-75" readonly/>
                        </div>
                </div>

                <div class="col-7">
                        <div class="form-group">Estado:
                                <input name="estado" type="text" id="estado" size="2" class="form-control w-25" readonly/>
                        </div>
                </div>          


                <input name="latitude" type="hidden" id="latitude" size="20" />
                <input name="longitude" type="hidden" id="longitude" size="20" />
        
        </div>
</div>