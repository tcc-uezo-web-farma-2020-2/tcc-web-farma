<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Web Farma - @yield("titulo")</title>

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">

    <link rel="stylesheet" href="{{asset('css/estilo.css')}}" crossorigin="anonymous">

	<!--<link rel="stylesheet" type="text/css" href="/css/style.css"> --><!-- Arquivo CSS está no diretório público e está sendo recuperado de lá pelo servidor HTTP -->

</head>

<body>

    <nav class="sticky-top navbar navbar-info navbar-expand-md bg-info pl-5 pr-5 mb-5 barra-navegacao">
        
            <a href= "{{ route('index') }}" class="navbar-brand text-white">Web farma</a>
        
            <div class="col-3">
                <div class="navbar-nav">
                    <a class="nav-link text-white" href="{{ route('index') }}">Home</a>
                    <a class="nav-link text-white" href="{{ route('categorias') }}">Categorias</a>
                    @if(!Auth::user())
                        <a class="nav-link text-white" href="{{ route('cadastro_cliente') }}">Cadastro</a>
                        <a class="nav-link text-white" href="{{ route('login') }}">Login</a>
                    @endif
                </div>
            </div>
        

            
            <div class="col-3">
                <form action="{{ route('pesquisa') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-10">                            
                            <input name="pesquisa" type="text" class="form-control" placeholder="O que está procurando?">                            
                        </div>
                        <div class="col-2">                            
                            <button type="submit" class="btn btn-primary botao-pesquisa"><i class="fas fa-search"></i></button>                            
                        </div>
                    </div>
                </form>
            </div> 
            
            <div class="col-1">
                
            </div>

            <div class="col-4">
                @if(Auth::user())
                    <div class="navbar-nav">
                        <span class="nav-link text-dark">Olá, {{ substr(Auth::user()->nome, 0, strpos(Auth::user()->nome, ' ')) }}!</span>
                        <a class="nav-link text-white" href="{{ route('minha_conta') }}">Minha conta</a>
                        <a class="nav-link text-white" href="{{ route('meus_pedidos') }}">Meus pedidos</a>
                        <a class="nav-link text-white" href="{{ route('sair') }}">Sair</a>
                    </div>
                @endif
            </div>

            <div class="col-1">
                <div class="ml-auto">
                    <a href="{{ route('carrinho') }}" class="btn btn-sm text-white">
                        <i class="fa fa-shopping-cart"></i>
                        <span id="qtd-carrinho" class="bg-danger rounded-circle qtd-carrinho bold"> 
                            {{ count(session('cart', [])) }}
                        </span>
                    </a>
                </div>
            </div>

        
        
    </nav>

    <!--<header class="sticky-top">
        <div class="container">
            <div class="row align-items-center">
                <div id="pesquisa" class="col-12 col-lg-5 d-none d-lg-block">
                    <div class="input-group pb-3">
                        <input name="pesquisa" type="text" class="form-control" placeholder="O que está procurando?">
                    </div>
                    <div class="input-group-append">
                        <buton class="btn btn-primary"><i class="fas fa-search"></i></buton>
                    </div>
                </div>
            </div>
        </div>
    </header> -->

    <div class="container">
        <div class="row">  
        <!-- Conteúdo a ser inserido pelas outras views -->
        @yield("conteudo")
        </div> 
    </div>

    <script type="text/javascript" src="{{ asset('js/produto.js') }}"></script>

    @yield("js-extras")

</body>

</html>