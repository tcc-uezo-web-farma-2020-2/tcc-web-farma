@extends("cliente/layoutCliente")

@section("titulo", "Erro")

@section("conteudo")    

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <a href="{{ route('index') }}"><button class="btn-lg btn-primary mt-3 ml-3">Ir para página inicial</button></a>


@endsection

