@extends("cliente/layoutCliente")

@section("titulo", "Alterar conta")

@section("conteudo") 

    <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
    @include("_mensagens")

    <h1>Alterar conta</h1>

    
    <form method="post" action="{{ route('alterar_cadastro') }}">
        <!--Cross site request forgery. Token enviado junto ao formulário, com um tempo de expiração, para garantir que a mesma pessoa que acessou o formulário é a que está enviando -->
        @csrf        
        <h3 class="mt-5 ml-3">Meus dados</h3>        
        <div class="col-12">
            <div class="row">
                <div class="col-8">
                    <div class="form-group">Nome:
                        <input name="nome" type="text" id="nome" size="60" class="form-control w-75" readonly value="{{ $usuario->nome }}"/>
                        @error('nome')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">E-mail:
                        <input name="email" type="email" id="email"size="60" class="form-control w-75" value="{{ $usuario->email }}"/>
                        @error('email')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group">CPF:
                        <input name="cpf" type="text" id="cpf" class="form-control w-25" readonly value="{{ $usuario->cpf }}"/>
                        @error('cpf')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Telefone:
                        <input name="telefone" type="text" id="telefone"class="form-control w-50" value="{{ $usuario->telefone }}"/>
                        @error('telefone')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Celular:
                        <input name="celular" type="text" id="celular"class="form-control w-50" value="{{ $usuario->celular }}"/>
                        @error('celular')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>                

                <div class="col-6">
                    <div class="form-group">Data de nascimento:
                        <input name="data_nascimento" type="date" id="data_nascimento" class="form-control w-50" readonly value="{{ $usuario->data_nascimento }}"/>
                        @error('data_nascimento')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">Gênero:
                        <select class="form-control w-50" id="genero" name="genero">
                            @if($usuario->genero == 'Masculino')
                                <option value="Masculino" selected>Masculino</option>
                            @else
                                <option value="Masculino">Masculino</option>
                            @endif
                            @if($usuario->genero == 'Feminino')
                                <option value="Feminino" selected>Feminino</option>
                            @else
                                <option value="Feminino">Feminino</option>
                            @endif
                            @if($usuario->genero == 'Outros')
                                <option value="Outros" selected>Outros</option>
                            @else
                                <option value="Outros">Outros</option>
                            @endif
                        </select>
                        @error('genero')
                            <small class="text-danger font-weight-bold">{{ $message }}</small>
                        @enderror
                        <!--<input name="genero" type="text" id="genero" class="form-control w-50"/>-->
                    </div>
                </div>
            </div>
        </div>        

        <button type="sumbit" method="post" class="btn-lg btn-success mt-3 ml-3 mb-5">Salvar</button>
    </form>

@endsection

@section("js-extras")
    <script type="text/javascript" src="js/localizacao.js"></script>
    <script type="text/javascript" src="js/mascara.js"></script>
    <script type="text/javascript" src="js/cadastro.js"></script>
@endsection