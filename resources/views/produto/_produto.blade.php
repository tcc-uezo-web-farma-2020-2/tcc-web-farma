@if(!empty($lista))  
    <div class="row">               
        @foreach($lista as $prod)
            <div class="col-3 mb-3" onmouseover="exibeQuantidade('quant{{$prod->slug}}')" onmouseout="escondeQuantidade('quant{{$prod->slug}}')">
                <div class="card card-size">                    
                    <a href="{{ route('nome_produto', ['nomeprod' => $prod->slug]) }}">
                        <img src="{{ asset($prod->imagens) }}" class="card-omg-top img-size">
                    </a>
                    <div class="card-body">
                        <a class="descricao-produto text-dark" href="{{ route('nome_produto', ['nomeprod' => $prod->slug]) }}">{{ $prod->produto }}</a>
                        <div class="pt-2">
                            <small class="text text-muted">A partir de</small>
                        </div>
                        <h5>R$ {{ number_format($prod->valor, 2, ',', '.') }}</h5>                        
                        <!-- <a href="{{ route('adicionar_carrinho', ['nomeprod' => $prod->slug]) }}" class="btn btn-sm btn-success mt-2">Comprar</a> -->
                        <form action="{{ route('adicionar_carrinho') }}" method="post">
                            @csrf
                            <!--<input type="hidden" value="">-->
                            <input id="quant{{ $prod->slug }}" class="col-5 col-form-label border rounded" name="quantidade" type="number" value="1" min="1" style="display: none">
                            <input name="id" type="hidden" value="{{ $prod->id_produto_farmacia }}">
                            <button type="submit" class="btn btn-sm btn-success mt-2">Comprar</a>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>


    <div class="col-12">
        <div class="navbar-nav">
            <div class="mx-auto">
                {{$lista->links()}}
            </div>
        </div>
    </div>
@endif
