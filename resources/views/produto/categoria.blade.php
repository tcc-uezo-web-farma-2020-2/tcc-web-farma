@extends("cliente/layoutCliente")

@section("titulo", $categoria->nomeCatGet)



@section("conteudo")   

    @include("cliente/_endereco", ['endereco' => $endereco])

    <!-- usando 2 colunas das 12 -->
    <div class="col-2">        
            <div class="list-group">            
                @foreach($categoria as $cat)
                    <a href="{{ route('nome_categoria', ['nomecat' => $cat->slug]) }}" class="list-group-item list-group-item-action @if($cat->slug == $categoria->catGet) active @endif">{{ $cat->categoria }}</a>
                @endforeach           
            </div>
            
    </div>

    <!-- usando as outras 10 colunas das 12 -->
    <div class="col-10">
        <!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
        @include("_mensagens")

        <!-- Incluindo view de página de produtos -->
        @include("produto/_produto", ['lista' => $produto])
    </div>
    

@endsection