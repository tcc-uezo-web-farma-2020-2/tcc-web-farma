@extends("cliente/layoutCliente")

@section("titulo", ucfirst(str_replace('-', ' ', $escolhido[0]->produto)))

@section("conteudo")


<!-- exibindo mensagens de erro, alerta ou sucesso, se houverem -->
@include("_mensagens")

<h1 class="mb-5">Detalhes do produto</h1>

<div class="container">
	<div class="row">
		
		<div class="col-6">
			<img class="img-produto" src="{{ asset($escolhido[0]->imagens) }}" alt=""></img>						
		</div>					
			
		<div class="col-6">
			<div class="informacoes-produto">
				<h2 class="font-weight-bold linha-titulo-produto">{{ $escolhido[0]->produto }}</h2>		
				<div class="linha-composicao-produto mt-3">		
					@if(!empty($escolhido[0]->composicao))					
							<p><strong>Composição: </strong>{{ $escolhido[0]->composicao }}</p>					
					@endif	
				</div>					
			</div>
			
			<div class="informacoes-preco">
				<h1 class="font-weight-bold">R$ {{ number_format($escolhido[0]->valor, 2, ',', '.') }}</h1>
				<div><p>Vendido e entregue por: {{ $escolhido[0]->nome_farmacia }}</p></div>
				<div class="valor-entrega-produto">
					Entrega: @if ($escolhido[0]->entrega == 0) <span class="text-success"> Grátis </span> @else R$ {{ number_format($escolhido[0]->entrega, 2, ',', '.') }} @endif
				</div>
				
				<form action="{{ route('adicionar_carrinho') }}" method="post">
					@csrf
					<input class="col-2 col-form-label border rounded mt-4" name="quantidade" type="number" value="1" min="1">				
					
					<input name="id" type="hidden" value="{{ $escolhido[0]->id_produto_farmacia }}">
					<div class="mt-4">
						<button type="submit" class="btn btn-success">Comprar</button>                   
					</div>
				</form>
				<!--<input class="col-2 col-form-label border rounded mt-4" name="quantidade" type="number" value="1" min="1">
				<div class="mt-4">
					
					<button type="submit" class="btn btn-success">Comprar</button>
				</div>	-->
			</div>					
		</div>
	</div>

		<!--<div class="container-fluid">		
			<div class="col-md-12 product-info">
					<ul id="myTab" class="nav nav-tabs nav_tabs">
						
						<li class="active"><a href="#service-one" data-toggle="tab">DESCRIPTION</a></li>
						<li><a href="#service-two" data-toggle="tab">PRODUCT INFO</a></li>
						<li><a href="#service-three" data-toggle="tab">REVIEWS</a></li>
						
					</ul>
				<div id="myTabContent" class="tab-content">
						<div class="tab-pane fade in active" id="service-one">
						 
							<section class="container product-info">
								The Corsair Gaming Series GS600 power supply is the ideal price-performance solution for building or upgrading a Gaming PC. A single +12V rail provides up to 48A of reliable, continuous power for multi-core gaming PCs with multiple graphics cards. The ultra-quiet, dual ball-bearing fan automatically adjusts its speed according to temperature, so it will never intrude on your music and games. Blue LEDs bathe the transparent fan blades in a cool glow. Not feeling blue? You can turn off the lighting with the press of a button.

								<h3>Corsair Gaming Series GS600 Features:</h3>
								<li>It supports the latest ATX12V v2.3 standard and is backward compatible with ATX12V 2.2 and ATX12V 2.01 systems</li>
								<li>An ultra-quiet 140mm double ball-bearing fan delivers great airflow at an very low noise level by varying fan speed in response to temperature</li>
								<li>80Plus certified to deliver 80% efficiency or higher at normal load conditions (20% to 100% load)</li>
								<li>0.99 Active Power Factor Correction provides clean and reliable power</li>
								<li>Universal AC input from 90~264V — no more hassle of flipping that tiny red switch to select the voltage input!</li>
								<li>Extra long fully-sleeved cables support full tower chassis</li>
								<li>A three year warranty and lifetime access to Corsair’s legendary technical support and customer service</li>
								<li>Over Current/Voltage/Power Protection, Under Voltage Protection and Short Circuit Protection provide complete component safety</li>
								<li>Dimensions: 150mm(W) x 86mm(H) x 160mm(L)</li>
								<li>MTBF: 100,000 hours</li>
								<li>Safety Approvals: UL, CUL, CE, CB, FCC Class B, TÜV, CCC, C-tick</li>
							</section>
										  
						</div>
					<div class="tab-pane fade" id="service-two">
						
						<section class="container">
								
						</section>
						
					</div>
					<div class="tab-pane fade" id="service-three">
												
					</div>
				</div>
				<hr>
			</div>
		</div>-->
		
		
		<h3 class="font-weight-bold mt-5 mb-4">Outras opções de compra</h3>
			
		@foreach ($outrasFarmacias as $farm)			
				<div class="border rounded mb-5">
					<div class="row">
						<div class="col-10">
							<div class="col-8 pt-3">
								<h4 class="font-weight-bold">R$ {{ number_format($farm->valor, 2, ',', '.') }}</h4>
							</div>								
							<div class="col-8">
								<strong>Vendido e entregue por: </strong>{{ $farm->nome_farmacia }}
							</div>								
							<div class="col-8 pb-3">
								<strong>Entrega: </strong> @if ($farm->entrega == 0) <span class="text-success"> Grátis @else R$ {{ number_format($farm->entrega, 2, ',', '.') }} @endif </span>
							</div>
						</div>

						<div class="col-2 mt-5">
							<a href="/produto/{{ $escolhido[0]->slug }}/{{ $farm->id_farmacia }}"><button class="btn btn-success">Selecionar</button></a>
						</div>
					</div>
				</div>			
			
		@endforeach
			






@endsection


@section('js-extras')

    <script type="text/javascript" src="{{ asset('js/detalheProduto.js') }}"></script>
@endsection