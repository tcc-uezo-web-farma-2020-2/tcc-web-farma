@if(session()->has('msg_erro'))
    <div class="col-12">
        <div class="alert alert-danger mb-4"> {{ session()->get('msg_erro') }} </div>
        <!-- apagando a mensagem, para não ser mais exibida quando a página for recarregada -->
        {{ session()->forget('msg_erro') }}
    </div>
@endif
@if(session()->has('msg_atencao'))
    <div class="col-12">
        <div class="alert alert-warning mb-4"> {{ session()->get('msg_atencao') }} </div>
        <!-- apagando a mensagem, para não ser mais exibida quando a página for recarregada -->
        {{ session()->forget('msg_atencao') }}
    </div>
@endif
@if(session()->has('msg_sucesso'))
    <div class="col-12">
        <div class="alert alert-success mb-4"> {{ session()->get('msg_sucesso') }} </div>
        {{ session()->forget('msg_sucesso') }}
    </div>
@endif